# inspired by https://gitlab.cern.ch/batch-team/singularity-containers/-/blob/master/CentOS_recipe_mpi4py_openmpi 
# uses software repositories available at CERN
# IMPORTANT: use the same OpenMPI version as on computed nodes and same PMI version 

FROM gitlab-registry.cern.ch/linuxsupport/alma9-base 

# environment variables for python version 
# coming from the CI/CD pipeline as a build argument to Docker
ARG PYTHON_MAJOR
ARG PYTHON_MINOR
ARG PYTHON_VERSION

# set package repositories to CERN ones
RUN yum -y install yum-utils
RUN yum-config-manager --add-repo https://linuxsoft.cern.ch/internal/repos/batch9al-stable/x86_64/os/
RUN yum-config-manager --add-repo https://linuxsoft.cern.ch/epel/9/Everything/x86_64/
RUN rpm --import http://linuxsoft.cern.ch/epel/RPM-GPG-KEY-EPEL-9
RUN yum-config-manager --add-repo https://linuxsoft.cern.ch//cern/alma/9/BaseOS/x86_64/os/

# install convenience packages for status checking on HTCondor
RUN yum -y install htop
# set TERM to xterm to avoid error messages in top
ENV TERM=xterm

# install packages needed for MPI, SLURM etc. 
RUN yum -y install openssh
RUN yum -y install gcc
RUN yum -y install gcc-c++
RUN yum -y install gcc-gfortran
RUN yum -y install make
RUN yum -y install wget
RUN yum -y install which
RUN yum -y install environment-modules
#running with pre-compiled openmpi package did cause communication problems previously 
#openmpi will be compiled later
#RUN yum -y install openmpi411
#running with pre-compiled mvapich2 cause problems compiling MPI-ready petsc
#RUN yum -y install mvapich2-devel
RUN yum -y install libibverbs 
RUN yum -y install mesa-libGLU
RUN yum -y install gsl-devel

# compile python 3.$PYTHON_MAJOR.$PYTHON_MINOR
RUN yum -y install openssl-devel bzip2-devel libffi-devel zlib-devel wget yum-utils make gcc tar sqlite-devel
RUN yum -y update 

RUN curl -O https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
RUN tar -xvzf Python-${PYTHON_VERSION}.tgz
WORKDIR Python-${PYTHON_VERSION}
RUN  ./configure --enable-loadable-sqlite-extensions --enable-optimizations

RUN make -j 4
RUN make install

RUN python3 -m ensurepip --upgrade
RUN pip3 --version

# switch back to root directory
WORKDIR /

# install prerequisites for getdp
RUN yum groupinstall -y 'Development Tools'
RUN yum install -y cmake
RUN yum install -y git 
RUN yum install -y curl

RUN yum install -y openblas-devel

# slurm is needed for the correct pmi version
RUN yum -y install slurm-libpmi
RUN yum -y install slurm-devel
RUN curl -L -O https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.1.tar.gz 
RUN tar -xzf openmpi-4.1.1.tar.gz 
WORKDIR openmpi-4.1.1
# important to configure with slurm and with PMI to grab correct pmi
RUN ./configure --prefix=/usr/local --with-slurm --enable-orterun-prefix-by-default --with-pmi=/usr
RUN make all install
WORKDIR .. 

# create python venv
RUN python3 -m venv /cerngetdp_venv

# install python requirements
RUN /cerngetdp_venv/bin/pip3 install --upgrade pip
COPY requirements.txt .
RUN /cerngetdp_venv/bin/pip3 install -r requirements.txt 

# compile petsc 
RUN curl -L -O http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.19.4.tar.gz
RUN tar -xzf petsc-3.19.4.tar.gz
WORKDIR petsc-3.19.4
ENV PETSC_DIR=/petsc-3.19.4

ENV PETSC_ARCH=real_mumps_seq
# link against 32-bit pthread openblas 
# libopenblaso64 would be 64-bit OpenMP version, MUMPS does not support 64-bit BLAS/LAPACK
RUN ./configure --with-blaslapack-lib=/usr/lib64/libopenblasp.so --with-64-bit-indices --with-clanguage=cxx --with-debugging=0 --with-mpi=0 --with-mpiuni-fortran-binding=0 --download-mumps=yes --with-mumps-serial --with-shared-libraries=0 --with-x=0 --with-ssl=0 --with-scalar-type=real --download-suitesparse
RUN make -j 8 PETSC_DIR=/petsc-3.19.4 PETSC_ARCH=real_mumps_seq all
WORKDIR ..

# compile small static gmsh for getdp for field interpolation (not the real mighty one, that one is installed via python)
RUN git clone https://gitlab.onelab.info/gmsh/gmsh.git
WORKDIR gmsh
RUN mkdir lib
WORKDIR lib
RUN cmake -DDEFAULT=0 -DENABLE_PARSER=1 -DENABLE_POST=1 -DENABLE_ANN=1 -DENABLE_BLAS_LAPACK=1 -DENABLE_BUILD_LIB=1 -DENABLE_PRIVATE_API=1 ..
RUN make -j 8 lib
RUN make install/fast
WORKDIR ../..

# compile CERNGetDP
RUN git clone --recurse-submodules https://gitlab.cern.ch/steam/cerngetdp.git
WORKDIR cerngetdp
# reset git to current commit
RUN git reset --hard $CI_COMMIT_SHA
# add CERN material functions to getdp
RUN /cerngetdp_venv/bin/python3 steam_material_adder/function_adder.py
# add TSA 1D integrals to getdp
RUN /cerngetdp_venv/bin/python3 steam_material_adder/tsa_function_adder.py
RUN mkdir bin
WORKDIR bin
RUN cmake -DGETDP_HOST=CERN -DENABLE_MPI=0 -DENABLE_SYSTEM_CONTRIB=1 -DENABLE_BLAS_LAPACK=0 -DENABLE_FORTRAN=1 -DENABLE_ARPACK=0 -DENABLE_SPARSKIT=0 -DENABLE_GSL=1 .. 
RUN make -j 8
RUN make install

RUN getdp --info

WORKDIR /
