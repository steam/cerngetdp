// These pro defines will be added to ProDefines.h. On the runner, the files will be found as artifacts of the build.

{"TSA_constantMaterial_rhs", (CAST)F_TSA_constantMaterial_rhs,  1,   1},
{"TSA_constantMaterial_constantThickness_rhs", (CAST)F_TSA_constantMaterial_constantThickness_rhs,  2,   0},
{"TSA_spatialMaterial_directOutput", (CAST)F_TSA_spatialMaterial_directOutput,  -1,   1},
{"TSA_spatialMaterial_constantThickness_rhs", (CAST)F_TSA_spatialMaterial_constantThickness_rhs,  -1,   1},

{"TSA_constantMaterial_mass", (CAST)F_TSA_constantMaterial_mass,  3,   1},
{"TSA_constantMaterial_constantThickness_mass", (CAST)F_TSA_constantMaterial_constantThickness_mass,  4,   0},
{"TSA_constantMaterial_stiffness", (CAST)F_TSA_constantMaterial_stiffness,  3,   1},
{"TSA_constantMaterial_constantThickness_stiffness", (CAST)F_TSA_constantMaterial_constantThickness_stiffness,  4,   0},
{"TSA_constantMaterial_triple", (CAST)F_TSA_constantMaterial_triple,  4,   1},
{"TSA_constantMaterial_constantThickness_triple", (CAST)F_TSA_constantMaterial_constantThickness_triple,  5,   0},
{"TSA_constantMaterial_fct_only", (CAST)F_TSA_constantMaterial_fct_only,  1,   1},
{"TSA_constantMaterial_constantThickness_fct_only", (CAST)F_TSA_constantMaterial_constantThickness_fct_only,  2,   0},
{"TSA_spatialMaterial_constantThickness_fct_only", (CAST)F_TSA_spatialMaterial_constantThickness_fct_only,  -1,   1},
{"TSA_spatialMaterial_constantThickness_mass", (CAST)F_TSA_spatialMaterial_constantThickness_mass,  -1,   1},
{"TSA_spatialMaterial_constantThickness_stiffness", (CAST)F_TSA_spatialMaterial_constantThickness_stiffness,  -1,   1},
{"TSA_spatialMaterial_constantThickness_triple", (CAST)F_TSA_spatialMaterial_constantThickness_triple,  -1,   1},

{"TSA_CFUN_QHCircuit_t_T_rhs", (CAST)F_TSA_CFUN_QHCircuit_t_T_rhs, 10, 4},
{"TSA_CFUN_kCu_T_stiffness", (CAST)F_TSA_CFUN_kCu_T_stiffness, 5, 3},
{"TSA_CFUN_kCu_T_mass", (CAST)F_TSA_CFUN_kCu_T_mass, 5, 3},
{"TSA_CFUN_rhoCu_T_stiffness", (CAST)F_TSA_CFUN_rhoCu_T_stiffness, 5, 3},
{"TSA_CFUN_rhoCu_T_mass", (CAST)F_TSA_CFUN_rhoCu_T_mass, 5, 3},
{"TSA_CFUN_kCu_T_constantThickness_stiffness", (CAST)F_TSA_CFUN_kCu_T_constantThickness_stiffness, 6, 2},
{"TSA_CFUN_kCu_T_constantThickness_mass", (CAST)F_TSA_CFUN_kCu_T_constantThickness_mass, 6, 2},
{"TSA_CFUN_rhoCu_T_constantThickness_stiffness", (CAST)F_TSA_CFUN_rhoCu_T_constantThickness_stiffness, 6, 2},
{"TSA_CFUN_rhoCu_T_constantThickness_mass", (CAST)F_TSA_CFUN_rhoCu_T_constantThickness_mass, 6, 2},


{% for (getdp_name, num_const_params, num_var_params, rhs, mass, stiffness, triple, fct_only) in zipped_inputs%}
{% if rhs %}
{"TSA_<<getdp_name>>_rhs", (CAST)F_TSA_<<getdp_name>>_rhs, <<num_const_params + 2>>, <<num_var_params>>},
{"TSA_<<getdp_name>>_constantThickness_rhs", (CAST)F_TSA_<<getdp_name>>_constantThickness_rhs, <<num_const_params + 3>>, <<num_var_params - 1>>},
{% endif %}
{% if stiffness %}
{"TSA_<<getdp_name>>_stiffness", (CAST)F_TSA_<<getdp_name>>_stiffness, <<num_const_params + 3>>, <<num_var_params>>},
{"TSA_<<getdp_name>>_constantThickness_stiffness", (CAST)F_TSA_<<getdp_name>>_constantThickness_stiffness, <<num_const_params + 4>>, <<num_var_params - 1>>},
{% endif %}
{% if mass %}
{"TSA_<<getdp_name>>_mass", (CAST)F_TSA_<<getdp_name>>_mass, <<num_const_params + 3>>, <<num_var_params>>},
{"TSA_<<getdp_name>>_constantThickness_mass", (CAST)F_TSA_<<getdp_name>>_constantThickness_mass, <<num_const_params + 4>>, <<num_var_params - 1>>},
{% endif %}
{% if triple %}
{"TSA_<<getdp_name>>_triple", (CAST)F_TSA_<<getdp_name>>_triple, <<num_const_params + 4>>, <<num_var_params>>},
{"TSA_<<getdp_name>>_constantThickness_triple", (CAST)F_TSA_<<getdp_name>>_constantThickness_triple, <<num_const_params + 5>>, <<num_var_params - 1>>},
{% endif %}
{% if fct_only %}
{"TSA_<<getdp_name>>_fct_only", (CAST)F_TSA_<<getdp_name>>_fct_only, <<num_const_params + 1>>, <<num_var_params>>},
{"TSA_<<getdp_name>>_constantThickness_fct_only", (CAST)F_TSA_<<getdp_name>>_constantThickness_fct_only, <<num_const_params + 2>>, <<num_var_params - 1>>},
{% endif %}
{% endfor %}

