#include "FiQuS_Pancake3D_2.h"

bool isInsideTheTriangle(double x, double y, double x1, double y1, double x2,
    double y2, double x3, double y3)
{
    double A = 0.5 * (-y2 * x3 + y1 * (-x2 + x3) + x1 * (y2 - y3) + x2 * y3);
    double sign = A < 0 ? -1 : 1;
    double s = (y1 * x3 - x1 * y3 + (y3 - y1) * x + (x1 - x3) * y) * sign;
    double t = (x1 * y2 - y1 * x2 + (y1 - y2) * x + (x2 - x1) * y) * sign;

    return s >= 0.0 && t >= 0.0 && (s + t) <= 2.0 * A * sign;
}

int isInsideTheMeshPolygon(double x, double y, double ane, double r_i,
    double theta_i, double gap, bool clockwise)
{
    double theta = theta_i;
    double theta_step;
    if (clockwise) {
        theta_step = -2.0 * PI / ane;
    } else {
        theta_step = 2.0 * PI / ane;
    }
    double r = r_i;
    double r_step = gap / ane;

    double x1, y1, x2, y2, x3, y3;
    for (int i = 0; i < ane; i++) {
        x1 = r * cos(theta);
        y1 = r * sin(theta);
        x2 = (r + r_step) * cos(theta + theta_step);
        y2 = (r + r_step) * sin(theta + theta_step);
        x3 = 0.0;
        y3 = 0.0;
        if (isInsideTheTriangle(x, y, x1, y1, x2, y2, x3, y3)) {
            return i;
        }
        theta += theta_step;
        r += r_step;
    }
    return -1;
}

double spiralsSpeedFunction(double t, double a, double b)
{
    return sqrt(a * a + (a * t + b) * (a * t + b));
}

double integrateSpiralsSpeedFunction(double r_2, double theta_2, double r_1,
    double theta_1, double dt)
{
    theta_2 = theta_2 - theta_1;
    theta_1 = 0.0;

    if (theta_2 <= PI / 36000.0) {
        return 0.0;
    }

    double b = r_1;
    double a = (r_2 - b) / theta_2;

    // integrate spiralsSpeedFunction with trapezoidal rule
    double arcLength = spiralsSpeedFunction(0.0, a, b);
    for (double t = dt; t < theta_2; t = t + dt) {
        arcLength = arcLength + 2.0 * spiralsSpeedFunction(t, a, b);
    }
    arcLength = arcLength + spiralsSpeedFunction(theta_2, a, b);

    arcLength = arcLength * dt / 2.0;

    return arcLength;
}

// Vec 3 --------------------------------------------------------------------------
Vec3::Vec3(double x, double y, double z)
        : m_x(x)
        , m_y(y)
        , m_z(z)
    {
    }

Vec3::Vec3() : m_x(0.0), m_y(0.0), m_z(0.0) {}

void Vec3::normalize()
{
	double length = sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
	m_x = m_x / length;
	m_y = m_y / length;
	m_z = m_z / length;
}

void Vec3::rotateWithRespectToXYPlane(double radians)
{
	double x = m_x;
	double y = m_y;
	double z = m_z;
	m_x = x * cos(radians) - y * sin(radians);
	m_y = x * sin(radians) + y * cos(radians);
	m_z = z;
}

double Vec3::XYplanarMagnitude() const { return sqrt(m_x * m_x + m_y * m_y); }

double Vec3::x() const { return m_x; }
double Vec3::y() const { return m_y; }
double Vec3::z() const { return m_z; }

double Vec3::magnitude() const { return sqrt(m_x * m_x + m_y * m_y + m_z * m_z); }

// operator overloading, scalar multiplication:
Vec3 Vec3::operator*(double scalar) const
{
	return Vec3(m_x * scalar, m_y * scalar, m_z * scalar);
}

// operator overloading, division by scalar:
Vec3 Vec3::operator/(double scalar) const
{
	return Vec3(m_x/scalar, m_y/scalar, m_z/scalar);
}


// Equality operator for Vec3
bool Vec3::operator==(const Vec3& other) const {
	return m_x == other.m_x && m_y == other.m_y && m_z == other.m_z;
}


Vec3 Vec3::transformToWindingCoordinates(double x, double y, double z) const
{
    Tensor transformationTensor = Winding::getInstance().getTransformationTensor(Vec3(x, y, z)).transpose();

    return transformationTensor * (*this);
}

Vec3 Vec3::transformToXYZCoordinates(double x, double y, double z) const
{
    Tensor transformationTensor = Winding::getInstance().getTransformationTensor(Vec3(x, y, z));

    return transformationTensor * (*this);
}


// Vec3Hash --------------------------------------------------------------------------

std::size_t Vec3Hash::operator()(const Vec3& v) const {
	std::size_t h1 = std::hash<double>()(v.x());
	std::size_t h2 = std::hash<double>()(v.y());
	std::size_t h3 = std::hash<double>()(v.z());
	return h1 ^ (h2 << 1) ^ (h3 << 2); // Combine hashes
}

// Tensor --------------------------------------------------------------------------

Tensor::Tensor(double xx, double xy, double xz, double yx, double yy, double yz,
	double zx, double zy, double zz)
	: m_xx(xx)
	, m_xy(xy)
	, m_xz(xz)
	, m_yx(yx)
	, m_yy(yy)
	, m_yz(yz)
	, m_zx(zx)
	, m_zy(zy)
	, m_zz(zz)
{
}

Tensor::Tensor()
	: m_xx(0.0)
	, m_xy(0.0)
	, m_xz(0.0)
	, m_yx(0.0)
	, m_yy(0.0)
	, m_yz(0.0)
	, m_zx(0.0)
	, m_zy(0.0)
	, m_zz(0.0)
{
}

double Tensor::xx() const { return m_xx; }
double Tensor::xy() const { return m_xy; }
double Tensor::xz() const { return m_xz; }
double Tensor::yx() const { return m_yx; }
double Tensor::yy() const { return m_yy; }
double Tensor::yz() const { return m_yz; }
double Tensor::zx() const { return m_zx; }
double Tensor::zy() const { return m_zy; }
double Tensor::zz() const { return m_zz; }

Tensor Tensor::transpose() const
{
	return Tensor(m_xx, m_yx, m_zx, m_xy, m_yy, m_zy, m_xz, m_yz, m_zz);
}
// operator overleading, tensor multiplication:
Tensor Tensor::operator*(const Tensor& tensor) const
{
	double xx = m_xx * tensor.xx() + m_xy * tensor.yx() + m_xz * tensor.zx();
	double xy = m_xx * tensor.xy() + m_xy * tensor.yy() + m_xz * tensor.zy();
	double xz = m_xx * tensor.xz() + m_xy * tensor.yz() + m_xz * tensor.zz();
	double yx = m_yx * tensor.xx() + m_yy * tensor.yx() + m_yz * tensor.zx();
	double yy = m_yx * tensor.xy() + m_yy * tensor.yy() + m_yz * tensor.zy();
	double yz = m_yx * tensor.xz() + m_yy * tensor.yz() + m_yz * tensor.zz();
	double zx = m_zx * tensor.xx() + m_zy * tensor.yx() + m_zz * tensor.zx();
	double zy = m_zx * tensor.xy() + m_zy * tensor.yy() + m_zz * tensor.zy();
	double zz = m_zx * tensor.xz() + m_zy * tensor.yz() + m_zz * tensor.zz();

	return Tensor(xx, xy, xz, yx, yy, yz, zx, zy, zz);
}

Tensor Tensor::transformToWindingCoordinates(double x, double y, double z) const
{
    Tensor transformationTensor = Winding::getInstance().getTransformationTensor(Vec3(x, y, z));

    Tensor transformedTensor = transformationTensor.transpose() * (*this) * transformationTensor;

    return transformedTensor;
}

Tensor Tensor::transformToXYZCoordinates(double x, double y, double z) const
{
    Tensor transformationTensor = Winding::getInstance().getTransformationTensor(Vec3(x, y, z));

    Tensor transformedTensor = transformationTensor * (*this) * transformationTensor.transpose();

    return transformedTensor;
}

// operator overloading, tensor multiplied with a vector:
Vec3 Tensor::operator*(const Vec3& vector) const
{
    double x = m_xx * vector.x() + m_xy * vector.y() + m_xz * vector.z();
    double y = m_yx * vector.x() + m_yy * vector.y() + m_yz * vector.z();
    double z = m_zx * vector.x() + m_zy * vector.y() + m_zz * vector.z();

    return Vec3(x, y, z);
}

Winding::Winding()
    : m_innerRadius(0.0), m_thickness(0.0), m_gapThickness(0.0), m_startAngle(0.0),
      m_azimuthalNumberOfElements(0), m_numberOfPancakeCoils(0), m_tapeHeight(0.0),
      m_gapBetweenPancakeCoils(0.0), m_initialized(false) {}

Winding& Winding::getInstance()
{
	static Winding instance;
	return instance;
}

void Winding::init(double innerRadius = 0.0, double thickness = 0.0,
	double gapThickness = 0.0, double startAngle = 0.0,
	int azimuthalNumberOfElements = 0.0, double tapeHeight = 0.0,
	int numberOfPancakeCoils = 0.0, double gapBetweenPancakeCoils = 0.0)
{
	m_innerRadius = innerRadius;
	m_thickness = thickness;
	m_gapThickness = gapThickness;
	m_startAngle = startAngle;
	m_azimuthalNumberOfElements = azimuthalNumberOfElements;
	m_tapeHeight = tapeHeight;
	m_numberOfPancakeCoils = numberOfPancakeCoils;
	m_gapBetweenPancakeCoils = gapBetweenPancakeCoils;

	m_initialized = true;
}

bool Winding::isInitialized() const { return m_initialized; }

double Winding::getGlobalStartingZ() const
{
	// Find the z-coordinate of the bottom of the winding:
	double startZ = -((double)m_numberOfPancakeCoils * m_tapeHeight + ((double)m_numberOfPancakeCoils - 1.0) * m_gapBetweenPancakeCoils) / 2.0;

	return startZ;
}

double Winding::getStartingZ(int whichPancakeCoil) const
{
	double startZ = this->getGlobalStartingZ();
	return startZ + ((double)whichPancakeCoil - 1.0) * (m_tapeHeight + m_gapBetweenPancakeCoils);
}

double Winding::getEndingZ(int whichPancakeCoil) const
{
	double startZ = this->getStartingZ(whichPancakeCoil);
	return startZ + m_tapeHeight;
}
int Winding::findTurnNumberFromElementIndex(int elementIndex) const
{
	return (double)elementIndex / (double)m_azimuthalNumberOfElements;
}


int Winding::getRotationMultiplier(const Vec3& point) const
{
    double startZ = this->getGlobalStartingZ();
    double z = point.z();

    // Determine which pancake coil the point is in and set the winding's
    // rotation direction accordingly:
    int rotationMultiplier = 1;
    for (int i = 0; i < m_numberOfPancakeCoils; i++) {
        if (z >= startZ + i * (m_tapeHeight + m_gapBetweenPancakeCoils / 2.0) && z <= startZ + (i + 1) * (m_tapeHeight + m_gapBetweenPancakeCoils / 2.0)) {
            if (i % 2 == 1) {
                rotationMultiplier = -1;
            }
        }
    }

    return rotationMultiplier;
}

bool Winding::isClockwise(const Vec3& point) const
{
    // 1st pancake coil: counter-clockwise
    // 2nd pancake coil: clockwise
    // and so on...
    double startZ = this->getGlobalStartingZ();
    double z = point.z();

    bool clockwise = false;
    for (int i = 0; i < m_numberOfPancakeCoils; i++) {
        if (z >= startZ + i * (m_tapeHeight + m_gapBetweenPancakeCoils / 2.0) && z <= startZ + (i + 1) * (m_tapeHeight + m_gapBetweenPancakeCoils / 2.0)) {
            if (i % 2 == 1) {
                clockwise = true;
            }
        }
    }

    return clockwise;
}

int Winding::getElementIndex(const Vec3& point) const
{
    // Determine which pancake coil the point is in and set the winding's
    // rotation direction accordingly:
    bool clockwise = this->isClockwise(point);

    // Total gap for the spiral:
    double totalGap = m_thickness + m_gapThickness;

    // Find the magnitude of the point's position vector:
    double r = point.XYplanarMagnitude();

    // Find the approximate turn number of the point. The point must be
    // between startTurn and finalTurn:
    int startTurn = (r - m_innerRadius) / totalGap - 1.5;
    if (startTurn < 0) {
        startTurn = 0;
    }

    int finalTurn = (r / cos(2.0 * PI / m_azimuthalNumberOfElements / 2.0) - m_innerRadius) / totalGap + 0.5;
    if (finalTurn == 0) {
        finalTurn = 1;
    }

    // Check each turn to find exact element index that containts the point:
    // 0th turn: first turn
    // 0th elementIndex: first element
    int elementIndex = -1;
    for (int turn = startTurn; turn <= finalTurn; turn++) {
        // Outer radius of the first turn's first element:
        double outerPolygonRadius = m_innerRadius + turn * totalGap + m_thickness;

        int relativeElementIndex = isInsideTheMeshPolygon(
            point.x(), point.y(), m_azimuthalNumberOfElements, outerPolygonRadius,
            m_startAngle, totalGap, clockwise);

        if (relativeElementIndex != -1) {
            elementIndex = turn * m_azimuthalNumberOfElements + relativeElementIndex;
            break;
        }
    }
    return elementIndex;
}

double Winding::getArcLength(const Vec3& point)
{
    if (m_arc_length_map.find(point) != m_arc_length_map.end()) {
        return m_arc_length_map[point];
    } else {
        //Message::Debug("Pancake3D: getArcLength did not find point.");

        int elementIndex = this->getElementIndex(point);
        int turn = this->findTurnNumberFromElementIndex(elementIndex);
        int rotationMultiplier = this->getRotationMultiplier(point);

        if (elementIndex == -1) {
            return -1;
        } else {
            double r_1 = m_innerRadius;
            double theta_1 = m_startAngle;

            double totalGap = m_thickness + m_gapThickness;
            double innerPolygonRadius = m_innerRadius + turn * totalGap;
            double outerPolygonRadius = m_innerRadius + turn * totalGap + m_thickness;

            double thetaRelativeToCurrentTurn = atan2(point.y(), point.x()) * rotationMultiplier;

            if (thetaRelativeToCurrentTurn < 0.0) {
                thetaRelativeToCurrentTurn += 2.0 * PI;
            }

            double theta_2 = m_startAngle + turn * 2.0 * PI + thetaRelativeToCurrentTurn;
            double r_2 = (innerPolygonRadius + outerPolygonRadius) / 2.0 + thetaRelativeToCurrentTurn * totalGap / (2.0 * PI);

            double dt = 2.0 * PI / m_azimuthalNumberOfElements / 8.0;
            if (dt > theta_2 - theta_1) {
                dt = (theta_2 - theta_1) / 2.0;
            }

            double arcLength = integrateSpiralsSpeedFunction(r_2, theta_2, r_1, theta_1, dt);

            m_arc_length_map[point] = arcLength;

            return arcLength;
        }
    }
}

double Winding::getArcLengthContactLayer(const Vec3& point)
{
    if (m_arc_length_contact_layer_map.find(point) != m_arc_length_contact_layer_map.end()) {
        return m_arc_length_contact_layer_map[point];
    } else {
        //Message::Debug("Pancake3D: getArcLength did not find point.");
        double r_1 = m_innerRadius;
        double theta_1 = m_startAngle;

        double total_thickness = m_thickness + m_gapThickness;

        double r_2 = point.XYplanarMagnitude();

        double turn = (r_2 - m_innerRadius) / total_thickness;

        double theta_2 = m_startAngle + turn * 2.0 * PI ;

        double dt = 2.0 * PI / m_azimuthalNumberOfElements / 8.0;
        if (dt > theta_2 - theta_1) {
            dt = (theta_2 - theta_1) / 2.0;
        }

        double arcLength = integrateSpiralsSpeedFunction(r_2, theta_2, r_1, theta_1, dt);

        m_arc_length_contact_layer_map[point] = arcLength;

        return arcLength;
    }
}

double Winding::getTurnNumberContactLayer(const Vec3& point)
{
    if (m_turn_number_contact_layer_map.find(point) != m_turn_number_contact_layer_map.end()) {
        return m_turn_number_contact_layer_map[point];
    } else {
        //Message::Debug("Pancake3D: getArcLength did not find point.");
        double r_1 = m_innerRadius;

        double theta_1 = m_startAngle;

        double total_thickness = m_thickness + m_gapThickness;

        double r_2 = point.XYplanarMagnitude();

        double turn = (r_2 - m_innerRadius) / total_thickness;

        double theta_2 = m_startAngle + turn * 2.0 * PI ;

        double turnNumber = (theta_2 - theta_1) / (2.0 * PI);

        m_turn_number_contact_layer_map[point] = turnNumber;

        return turnNumber;
    }
}

double Winding::getTurnNumber(const Vec3& point)
{

    if (m_turn_number_map.find(point) != m_turn_number_map.end()) {
        return m_turn_number_map[point];
    } else {
        //Message::Debug("Pancake3D: getTurnNumber did not find point.");
    
        int elementIndex = this->getElementIndex(point);
        int turn = this->findTurnNumberFromElementIndex(elementIndex);
        int rotationMultiplier = this->getRotationMultiplier(point);

        if (elementIndex == -1) {
            return -1;
        } else {
            double theta_1 = m_startAngle;

            double thetaRelativeToCurrentTurn = atan2(point.y(), point.x()) * rotationMultiplier;

            if (thetaRelativeToCurrentTurn < 0.0) {
                thetaRelativeToCurrentTurn += 2.0 * PI;
            }

            double theta_2 = m_startAngle + turn * 2.0 * PI + thetaRelativeToCurrentTurn;

            double turnNumber = (theta_2 - theta_1) / (2.0 * PI);

            m_turn_number_map[point] = turnNumber;

            return turnNumber;
        }
    }
}

Vec3 Winding::getNormal(const Vec3& point)
{

    if (m_normal_map.find(point) != m_normal_map.end()) {
        return m_normal_map[point];
    } else {
        //Message::Debug("Pancake3D: getNormal did not find point.");

        int elementIndex = this->getElementIndex(point);
        int turn = this->findTurnNumberFromElementIndex(elementIndex);
        int relativeElementIndex = elementIndex % m_azimuthalNumberOfElements;
        int rotationMultiplier = this->getRotationMultiplier(point);

        if (elementIndex == -1) {
            return Vec3(0.0, 0.0, 0.0);
        } else {
            double theta_step = 2.0 * PI / m_azimuthalNumberOfElements * rotationMultiplier;

            double totalGap = m_thickness + m_gapThickness;
            double r_step = totalGap / m_azimuthalNumberOfElements;

            double theta = m_startAngle + relativeElementIndex * theta_step;

            double outerPolygonRadius = m_innerRadius + turn * totalGap + m_thickness;
            double r = outerPolygonRadius + relativeElementIndex * r_step;

            double x1 = r * cos(theta);
            double y1 = r * sin(theta);
            double x2 = (r + r_step) * cos(theta + theta_step);
            double y2 = (r + r_step) * sin(theta + theta_step);

            Vec3 normalVector = Vec3(x2 - x1, y2 - y1, 0.0);
            normalVector.rotateWithRespectToXYPlane(-PI / 2.0);
            normalVector.normalize();

            m_normal_map[point] = normalVector;

            return normalVector;
        }
    }
}

Tensor Winding::getTransformationTensor(const Vec3& point)
{
    // Transformation tensor from winding coordinate system to XYZ coordinate
    // system: see: https://www.continuummechanics.org/coordxforms.html X, Y, Z
    // are the global coordinate system, x, y, z are the winding coordinate
    // system. x: normal to the winding, positive direction is from the inner
    // radius to the outer radius y: parallel to the windng, positive direction is
    // in counterclockwise direction (i.e., right-hand rule applies for the
    // winding coordinate system, assuming z and Z are the same) z: the same with
    // Z

    // n1: Unit vector of X in the winding coordinate system
    // n2: Unit vector of Y in the winding coordinate system
    // n3: Unit vector of Z in the winding coordinate system


    if (m_transformation_tensor_map.find(point) != m_transformation_tensor_map.end()) {
        return m_transformation_tensor_map[point];
    } else {

        Vec3 normal = this->getNormal(point);

        double n1x = normal.x();
        double n1y = -normal.y();
        double n1z = 0.0;
        double n2x = normal.y();
        double n2y = normal.x();
        double n2z = 0.0;
        double n3x = 0.0;
        double n3y = 0.0;
        double n3z = 1.0;

        Tensor return_tensor = Tensor(n1x, n1y, n1z, n2x, n2y, n2z, n3x, n3y, n3z);
        m_transformation_tensor_map[point] = return_tensor;
        return return_tensor;
    }
}

void Winding::setRhoDerivative(const Tensor& rho_derivative)
{
    m_rho_derivative = rho_derivative;
}

Tensor Winding::getRhoDerivative() const { return m_rho_derivative; }

Winding& Winding::get_winding_object(double innerRadius, double thickness,
    double gapThickness, double startAngle,
    int azimuthalNumberOfElements,
    double tapeHeight,
    int numberOfPancakeCoils,
    double gapBetweenPancakeCoils)
{
    Winding& winding_object = Winding::getInstance();

    if (!winding_object.isInitialized()) {
        //Message::Debug("not initialized");
        winding_object.init(innerRadius, thickness, gapThickness, startAngle,
            azimuthalNumberOfElements, tapeHeight,
            numberOfPancakeCoils, gapBetweenPancakeCoils);
    }

    return winding_object;
}

enum MaterialName {
    Copper = 0,
    Hastelloy = 1,
    Silver = 2,
    Indium = 3,
    StainlessSteel = 4,
    HTSSuperPower = 5,
    HTSFujikura = 6,
    HTSSucci = 7,
    HTSBenchmark = 8
};

double rho(double rrr, double rrr_ref_T, MaterialName material_name,
    double temperature, double magnetic_field_magnitude)
{
    static Function Func;
    static Value V;
    if (material_name == Copper) {
        Value A[2];

        double parameters[] = { rrr };
        Func.Para = &parameters[0];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;
        A[1].Type = SCALAR;
        A[1].Val[0] = magnetic_field_magnitude;

        F_CFUN_rhoCu_T_B(&Func, &A[0], &V);
    } else if (material_name == Hastelloy) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_rhoHast_T(&Func, &A[0], &V);
    } else if (material_name == Silver) {
        Value A[2];

        double parameters[] = { rrr, rrr_ref_T };
        Func.Para = &parameters[0];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;
        A[1].Type = SCALAR;
        A[1].Val[0] = magnetic_field_magnitude;

        F_CFUN_rhoAg_T_B(&Func, &A[0], &V);
    } else if (material_name == Indium) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_rhoIn_T(&Func, &A[0], &V);
    } else if (material_name == StainlessSteel) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_rhoSS_T(&Func, &A[0], &V);
    } else {
        Message::Error("Pancake3D material name is not found!");
        return 0;
    }

    return V.Val[0];
}

double kappa(double rrr, double rrr_ref_T, MaterialName material_name,
    double temperature, double magnetic_field_magnitude)
{
    static Function Func;
    static Value V;
    if (material_name == Copper) {
        Value A[3];
        double parameters[] = {rrr};
        Func.Para = &parameters[0];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;
        A[1].Type = SCALAR;
        A[1].Val[0] = rho(rrr, rrr_ref_T, Copper, temperature, 0.0);
        A[2].Type = SCALAR;
        A[2].Val[0] = rho(rrr, rrr_ref_T, Copper, temperature, magnetic_field_magnitude);

        F_CFUN_kCu_T_rho0_rho(&Func, &A[0], &V);
    } else if (material_name == Hastelloy) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_kHast_T(&Func, &A[0], &V);
    } else if (material_name == Silver) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_kAg_T(&Func, &A[0], &V);
    } else if (material_name == Indium) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_kIn_T(&Func, &A[0], &V);
    } else if (material_name == StainlessSteel) {
        Value A[1];

        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;

        F_CFUN_kSteel_T(&Func, &A[0], &V);
    } else {
        Message::Error("Pancake3D material name is not found!");
        return 0;
    }

    return V.Val[0];
}

double parallel_connection_of_rhos(const std::vector<double>& rhos,
    const std::vector<double>& relative_thicknesses)
{
    double result_inversed = 0.0;

    for (long unsigned int i = 0; i < rhos.size(); i++) {
        double ratio_i = relative_thicknesses[i];
        double material_i = rhos[i];
        result_inversed += ratio_i / material_i;
    }

    return 1.0 / result_inversed;
}

// derivative of the total effective resistivity with respect to the resistivity
// of the i-th material times that materials derivative with respect to j
Vec3 parallel_connection_of_rhos_derivative(double rho_effective, double rho_i,
    double relative_thickness_i,
    double rho_iDotx, double rho_iDoty,
    double rho_iDotz)
{
    double derivativex, derivativey, derivativez;
    if (rho_i <= 1e-20) {
        derivativex = 0.0;
        derivativey = 0.0;
        derivativez = 0.0;
    } else {
        double common = pow(rho_effective/rho_i, 2) * relative_thickness_i;
        derivativex = common * rho_iDotx;
        derivativey = common * rho_iDoty;
        derivativez = common * rho_iDotz;
    }

    Vec3 derivative = Vec3(derivativex, derivativey, derivativez);
    return derivative;
}

double series_connection_of_rhos(const std::vector<double>& rhos,
    const std::vector<double>& relative_thicknesses)
{
    double result = 0.0;

    // mixture: sum_i ratio_i * material_i
    for (long unsigned int i = 0; i < rhos.size(); i++) {
        double ratio_i = relative_thicknesses[i];
        double material_i = rhos[i];
        result += ratio_i * material_i;
    }

    return result;
}

// // derivative of the total effective resistivity with respect to the
// resistivity of the i-th material
// // times that materials derivative with respect to j
// Vec3 series_connection_of_rhos_derivative(double rho_effective, double rho_i,
// double relative_thickness_i, double rho_iDotx, double rho_iDoty, double
// rho_iDotz)
// {
//     return Vec3(rho_iDotx, rho_iDoty, rho_iDotz) * relative_thickness_i;
// }

double parallel_connection_of_kappas(const std::vector<double>& kappas,
    const std::vector<double>& relative_thicknesses)
{
    return series_connection_of_rhos(kappas, relative_thicknesses);
}

double series_connection_of_kappas(const std::vector<double>& kappas,
    const std::vector<double>& relative_thicknesses)
{
    return parallel_connection_of_rhos(kappas, relative_thicknesses);
}

double jcritical_hts(MaterialName material_name, double temperature, double Bx, double By,
    double Bz, double x, double y, double z)
{
    static Winding& winding = Winding::get_winding_object();
    Vec3 normal = winding.getNormal(Vec3(x, y, z));

    // compute angle between current density and normal vector:
    double Bnorm = sqrt(Bx * Bx + By * By + Bz * Bz);
    double angle = acos((Bx * normal.x() + By * normal.y() + Bz * normal.z()) / Bnorm);

    // compute critical current density:
    static Function Func;
    static Value V;
    static Value A[3];

    if (material_name != HTSBenchmark) {
        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;
    
        if (Bnorm < 1e-12) {
            A[1].Type = SCALAR;
            A[1].Val[0] = 0.0;
            A[2].Type = SCALAR;
            A[2].Val[0] = 0.0;
        } else {
            A[1].Type = SCALAR;
            A[1].Val[0] = Bnorm;
            if (material_name != HTSSucci) {
                A[2].Type = SCALAR;
                A[2].Val[0] = angle;
            }
        }
    } else {
        A[0].Type = SCALAR;
        A[0].Val[0] = Bnorm;
    }

    if(material_name == HTSSuperPower)
        F_CFUN_HTS_JcFit_SUPERPOWER_T_B_theta(&Func, &A[0], &V);
    else if(material_name == HTSFujikura)
        F_CFUN_HTS_JcFit_Fujikura_T_B_theta(&Func, &A[0], &V);
    else if(material_name == HTSSucci)
        F_CFUN_HTS_JcFit_Succi_T_B(&Func, &A[0], &V);
    else if(material_name == HTSBenchmark)
        F_CFUN_HTS_JcFit_Benchmark_B(&Func, &A[0], &V);
    else
        Message::Error("Pancake3D superconductor material name is not found!");

    return V.Val[0];
}

double jcritical_hts_reference(MaterialName material_name, double temperature, double reference_Bmagnitude,
    double reference_Bangle)
{
    // compute critical current density:
    static Function Func;
    static Value V;
    static Value A[3];

    if (material_name != HTSBenchmark) {
        A[0].Type = SCALAR;
        A[0].Val[0] = temperature;
    
        A[1].Type = SCALAR;
        A[1].Val[0] = reference_Bmagnitude;

        if (material_name != HTSSucci) {
            A[2].Type = SCALAR;
            A[2].Val[0] = reference_Bangle;
        }
        
    } else {
        A[0].Type = SCALAR;
        A[0].Val[0] = reference_Bmagnitude;
    }

    if(material_name == HTSSuperPower)
        F_CFUN_HTS_JcFit_SUPERPOWER_T_B_theta(&Func, &A[0], &V);
    else if(material_name == HTSFujikura)
        F_CFUN_HTS_JcFit_Fujikura_T_B_theta(&Func, &A[0], &V);
    else if(material_name == HTSSucci)
        F_CFUN_HTS_JcFit_Succi_T_B(&Func, &A[0], &V);
    else if(material_name == HTSBenchmark)
        F_CFUN_HTS_JcFit_Benchmark_B(&Func, &A[0], &V);
    else
        Message::Error("Pancake3D superconductor material name is not found!");

    return V.Val[0];
}

double linear_interpolation(const std::vector<double>& x_values,
    const std::vector<double>& y_values, double x)
{
    // if the length of x_values and y_values are 1, return the only value in
    // y_values
    if (x_values.size() == 1) {
        return y_values[0];
    }

    // Find the index of the first element that is greater than x:
    std::vector<double>::size_type index = 0;
    bool found = false;
    for (std::vector<double>::size_type i = 0; i < x_values.size(); i++) {
        if (x_values[i] > x) {
            index = i;
            found = true;
            break;
        }
    }

    if (index == 0) {
        if (found) {
            Message::Error("Pancake3D: provided Ic over length csv data should start with an Ic value for length 0m. Point with length %g could not be interpolated.", x);
        } else {
            Message::Error("Pancake3D: argument of linear interpolation is outside the provided data range. This typically occurs if the winding modeled with Pancake3D is longer than the provided Ic over length csv data. Point with length %g could not be interpolated.", x);
        }
    }

    // Compute the slope:
    double slope = (y_values[index] - y_values[index - 1]) / (x_values[index] - x_values[index - 1]);

    // Compute the y value:
    double y = y_values[index - 1] + slope * (x - x_values[index - 1]);

    return y;
}

namespace Pancake3DNameSpace {
    // Declare global map within namespace
    std::unordered_map<Vec3, double, Vec3Hash> jcritical_multiplier_map;
}   
 
double compute_jcritical_multiplier(Winding& winding, double x, double y, double z, MaterialName material_name, double reference_temperature, double reference_Bmagnitude, double reference_Bangle, const std::vector<double>& length_values, const std::vector<double>& corresponding_Ic_values, double hts_surface_area, double scaling_factor_due_to_TSA, int Ic_in_arc_length) {

    Vec3 point = Vec3(x, y, z);
    if (Pancake3DNameSpace::jcritical_multiplier_map.find(point) != Pancake3DNameSpace::jcritical_multiplier_map.end()) {
        return Pancake3DNameSpace::jcritical_multiplier_map[point];
    } else {
        double reference_critical_current_density = jcritical_hts_reference(material_name, reference_temperature, reference_Bmagnitude, reference_Bangle);
        double length = 0;

        if (Ic_in_arc_length == 1)
            length = winding.getArcLength(point);
        else
            length = winding.getTurnNumber(point);

        double initial_current_density = linear_interpolation(length_values, corresponding_Ic_values, length) / hts_surface_area * scaling_factor_due_to_TSA;
        double jcritical_multiplier = initial_current_density / reference_critical_current_density;

        Pancake3DNameSpace::jcritical_multiplier_map[point] = jcritical_multiplier;

        return jcritical_multiplier;
    }
}

double jcritical(MaterialName material_name, double time, double temperature, double x, double y, double z,
    double Bx, double By, double Bz,
    double reference_temperature,
    double reference_Bmagnitude, 
    double reference_Bangle,
    const std::vector<double>& length_values,
    const std::vector<double>& corresponding_Ic_values,
    double local_defect_start_turn,
    double local_defect_end_turn,
    double local_defect_which_pancake,
    double local_defect_start_time, double local_defect_value,
    double hts_surface_area, // thickness of HTS * height of HTS
    double scaling_factor_due_to_TSA,
    int Ic_in_arc_length)
{
    static Winding& winding = Winding::get_winding_object();
    // Check if there is a local defect:
    if (time >= local_defect_start_time && z >= winding.getStartingZ(local_defect_which_pancake) && z <= winding.getEndingZ(local_defect_which_pancake)) {
        double turn_number = winding.getTurnNumber(Vec3(x, y, z));
        if (turn_number >= local_defect_start_turn && turn_number <= local_defect_end_turn) {
            return local_defect_value;
        }
    }

    double original_critical_current_density = jcritical_hts(material_name, temperature, Bx, By, Bz, x, y, z);

    // Compute Jcritical multiplier:     
    double jcritical_multiplier = compute_jcritical_multiplier(winding, x, y, z, material_name, reference_temperature, reference_Bmagnitude, reference_Bangle, length_values, corresponding_Ic_values, hts_surface_area, scaling_factor_due_to_TSA, Ic_in_arc_length) ; 

    // Compute Jcritical:
    double jcritical = original_critical_current_density * jcritical_multiplier;

    return jcritical;
}

double rhoNormalConductorParallelToWinding(
    const std::vector<double>& rrr, const std::vector<double>& rrr_ref_T, double temperature,
    double magnetic_field_magnitude, const std::vector<MaterialName>& material_names,
    const std::vector<double>& fractions, double rhoShunt)
{
    static std::vector<double> rhos(material_names.size() + 1);
    for (long unsigned int i = 0; i < material_names.size(); i++) {
        rhos[i] = rho(rrr[i], rrr_ref_T[i], material_names[i],
            temperature, magnetic_field_magnitude);
    }

    rhos[material_names.size()] = rhoShunt;
    return parallel_connection_of_rhos(rhos, fractions);
}

double rhoNormalConductorNormalToWinding(
    const std::vector<double>& rrr, const std::vector<double>& rrr_ref_T, double temperature,
    double magnetic_field_magnitude, const std::vector<MaterialName>& material_names,
    const std::vector<double>& relative_thicknesses)
{
    static std::vector<double> rhos(material_names.size());
    for (long unsigned int i = 0; i < material_names.size(); i++) {
        rhos[i] = rho(rrr[i], rrr_ref_T[i], material_names[i],
            temperature, magnetic_field_magnitude);
    }

    return series_connection_of_rhos(rhos, relative_thicknesses);
}

double kappaAzimuthalDirection(const std::vector<double>& rrr,
    const std::vector<double>& rrr_ref_T, double temperature,
    double magnetic_field_magnitude,
    const std::vector<MaterialName>& material_names,
    const std::vector<double>& fractions, double kappaShunt)
{
    static std::vector<double> kappas(material_names.size() + 1);
    for (long unsigned int i = 0; i < material_names.size(); i++) {
        kappas[i] = kappa(rrr[i], rrr_ref_T[i], material_names[i],
            temperature, magnetic_field_magnitude);
    }

    kappas[material_names.size()] = kappaShunt;

    return parallel_connection_of_kappas(kappas, fractions);
}

double kappaNormalToWindingWithoutShunt(const std::vector<double>& rrr,
    const std::vector<double>& rrr_ref_T, double temperature,
    double magnetic_field_magnitude,
    const std::vector<MaterialName>& material_names,
    const std::vector<double>& fractions)
{
    static std::vector<double> kappas(material_names.size());
    for (long unsigned int i = 0; i < material_names.size(); i++) {
        kappas[i] = kappa(rrr[i], rrr_ref_T[i], material_names[i],
            temperature, magnetic_field_magnitude);
    }

    return series_connection_of_kappas(kappas, fractions);
}

double kappaAxialDirectionWithoutShunt(const std::vector<double>& rrr,
    const std::vector<double>& rrr_ref_T, double temperature,
    double magnetic_field_magnitude,
    const std::vector<MaterialName>& material_names,
    const std::vector<double>& fractions)
{
    static std::vector<double> kappas(material_names.size());
    for (long unsigned int i = 0; i < material_names.size(); i++) {
        kappas[i] = kappa(rrr[i], rrr_ref_T[i], material_names[i],
            temperature, magnetic_field_magnitude);
    }

    return parallel_connection_of_kappas(kappas, fractions);
}


double lambda(double jr, double jtheta, double jz, double x, double y, double z,
    double jCritical, double rhoNormalConductor,
    double electricFieldCriterion, double nValue,
    double superConductorFraction,
    double normalConductorFraction, double windingThickness, int isotropic_materials)
{
    double J_hom_cc_r = jr;
    double J_hom_cc_phi = jtheta;
    double J_hom_cc_z = jz;
    double Jc = jCritical;
    double rhoNc = rhoNormalConductor;

    double Ec = electricFieldCriterion;
    double n = nValue;
    double NO_OF_MAX_ITER = 20;
    double ABS_TOL = 1e-5;

    // Global index
    // int i;
    int iter_i;

    // Binary search algorithm
    double a_coefficient;
    double lambda;
    double lambda_MIN;
    double lambda_MAX;
    double fun;
    double err;

    // Quench
    if (Jc < 1e-4) {
        lambda = 0.0;
    }
    // Current Sharing
    else {
        // Binary search algorithm
        lambda_MIN = 0.0;
        lambda_MAX = 1.0;
        if (isotropic_materials == 0)
            a_coefficient = normalConductorFraction/superConductorFraction * 1.0 / rhoNc * Ec/Jc * pow(sqrt(pow(J_hom_cc_phi, 2) + pow(J_hom_cc_z, 2)) / (Jc * superConductorFraction), n - 1.0); 
        else 
            a_coefficient = normalConductorFraction/superConductorFraction * 1.0 / rhoNc * Ec/Jc * pow(sqrt(pow(J_hom_cc_r, 2) + pow(J_hom_cc_phi, 2) + pow(J_hom_cc_z, 2)) / (Jc * superConductorFraction), n - 1.0); 

        // Inner iterations
        for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
            lambda = (lambda_MIN + lambda_MAX) * 0.5;
            fun = a_coefficient * pow(lambda, n) + lambda - 1.0;

            err = lambda_MAX - lambda_MIN;

            if (err > ABS_TOL) { // Update boundaries
                if (fun >= 0.0) { // too much current in HTS
                    lambda_MAX = lambda;
                } else if (fun < 0.0) { // not enough current in HTS
                    lambda_MIN = lambda;
                }
            } else { // Converged
                break;
            }
        }

        // Consistency check: number of iterations
        if (iter_i == NO_OF_MAX_ITER) {
            Message::Debug("F_CurrentSharing_anisotropic: Maximum number of "
                           "bisection iterations reached");
        }
    }

    // Consistency check: output
    if (lambda != lambda) {
        Message::Debug("F_CurrentSharing_anisotropic: Output is nan");
    }

    if (fabs(lambda) > std::numeric_limits<double>::max()) {
        Message::Debug("F_CurrentSharing_anisotropic: Output is inf");
    }

    return lambda;
}

Vec3 lambdaDerivative(double lambda, double jr, double jtheta, double jz,
    double x, double y, double z, double jCritical,
    double rhoNormalConductor, double electricFieldCriterion,
    double nValue, double superConductorFraction,
    double normalConductorFraction,
    double windingThickness, int isotropic_materials)
{
    double J_hom_cc_r = jr;
    double J_hom_cc_phi = jtheta;
    double J_hom_cc_z = jz;
    double Jc = jCritical;
    double rhoNc = rhoNormalConductor;

    double Ec = electricFieldCriterion;
    double n = nValue;

    // Consistency check: arguments
    // if(Jc    <  0){error = "Jc    is <  0!"; return 0; }
    // if(Ec    <= 0){error = "Ec    is <= 0!"; return 0; }
    // if(n     <  0){error = "n     is <  0!"; return 0; }
    // if(hSc   <= 0){error = "hSc   is <= 0!"; return 0; }
    // if(hNc   <= 0){error = "hNc   is <= 0!"; return 0; }
    // if(rhoNc <= 0){error = "rhoNc is <= 0!"; return 0; }

    double lambdaDot2, lambdaDot3;
    double jNorm;
    double a_coefficient; 
    double j_r_phi_norm;

    // Quench
    if (Jc < 1e-4) {
        lambdaDot2 = 0.0;
        lambdaDot3 = 0.0;
    }
    // Current Sharing
    else {
        if (isotropic_materials == 0)
            j_r_phi_norm = sqrt(pow(J_hom_cc_phi, 2) + pow(J_hom_cc_z, 2));
        else
            j_r_phi_norm = sqrt(pow(J_hom_cc_r, 2) + pow(J_hom_cc_phi, 2) + pow(J_hom_cc_z, 2));
            
        jNorm = lambda/superConductorFraction * j_r_phi_norm;

        if (fabs(jNorm) < 1e-10) {
            lambdaDot2 = 0.0;
            lambdaDot3 = 0.0;
        } else {
            a_coefficient = normalConductorFraction/superConductorFraction * 1.0 / rhoNc * Ec/Jc * pow(j_r_phi_norm / (Jc * superConductorFraction), n - 1.0);

            double a_coefficient_der_without_j = a_coefficient/pow(j_r_phi_norm, 2) * (n - 1.0); 

            double pre_factor = -pow(lambda, n) *  a_coefficient_der_without_j / (n * a_coefficient * pow(lambda, n - 1.0) + 1);

            lambdaDot2 =  pre_factor * J_hom_cc_phi;
            lambdaDot3 =  pre_factor * J_hom_cc_z;
        }
    }

    // Consistency check: output
    if (lambdaDot2 != lambdaDot2 || lambdaDot3 != lambdaDot3) {
        Message::Debug("F_CurrentSharingDerivative_anisotropic: Output is nan");
    }

    if (fabs(lambdaDot2) > std::numeric_limits<double>::max() || fabs(lambdaDot3) > std::numeric_limits<double>::max()) {
        Message::Debug("F_CurrentSharingDerivative_anisotropic: Output is inf");
    }

    return Vec3(0.0, lambdaDot2, lambdaDot3);
}

// power law
// rho = Ecrit/Jc * (J/Jc)^(n - 1)
double rhoSuperConductor(double jMagnitude, double jCritical,
    double electricFieldCriterion, double nValue)
{
    // current density norm in the HTS layer
    double Jnorm = jMagnitude;
    double Jcrit = jCritical;

    double Ecrit = electricFieldCriterion;
    double n = nValue;

    double rho;
    if (Jcrit > 0.0) {
        rho = Ecrit / Jcrit * pow(Jnorm / Jcrit, n - 1.0);
    } else {
        rho = std::numeric_limits<double>::max();
    }

    return rho;
}

// derivative of the power law with respect to the current density
// because current density is a vector, the derivative will be a vector
Vec3 rhoSuperConductorDerivativeWRTj(double Jx, double Jy, double Jz,
    double jCritical,
    double electricfieldCriterion,
    double nValue)
{
    double Jcrit = jCritical;
    double Ecrit = electricfieldCriterion;
    double n = nValue;

    double rhoDotx, rhoDoty, rhoDotz;
    if (Jcrit > 0.0) {
        double jNorm = sqrt(Jx * Jx + Jy * Jy + Jz * Jz);
        if (jNorm > 1e-10) {
            double rhoDotCommon = Ecrit / pow(Jcrit, 3) * (n - 1.0) * pow(jNorm / Jcrit, n - 3.0);
            rhoDotx = rhoDotCommon * Jx;
            rhoDoty = rhoDotCommon * Jy;
            rhoDotz = rhoDotCommon * Jz;
        } else {
            rhoDotx = 0.0;
            rhoDoty = 0.0;
            rhoDotz = 0.0;
        }
    } else {
        rhoDotx = std::numeric_limits<double>::max();
        rhoDoty = std::numeric_limits<double>::max();
        rhoDotz = std::numeric_limits<double>::max();
    }

    return Vec3(rhoDotx, rhoDoty, rhoDotz);
}

// GETDP SIDE
// ==========================================================================
// =====================================================================================
// =====================================================================================
// =====================================================================================
// =====================================================================================
// =====================================================================================
void F_FiQuS_Pancake3DCriticalCurrentDensity(F_ARG)
{
    double time = (double)A->Val[0];

    double x = (double)(A + 1)->Val[0];
    double y = (double)(A + 1)->Val[1];
    double z = (double)(A + 1)->Val[2];

    double Bx = (double)(A + 2)->Val[0];
    double By = (double)(A + 2)->Val[1];
    double Bz = (double)(A + 2)->Val[2];

    double temperature = (double)(A + 3)->Val[0];

    static double reference_temperature = (double)Fct->Para[0];
    static double reference_Bmagnitude = (double)Fct->Para[1];
    static double reference_Bangle = (double)Fct->Para[2];
    static int Ic_in_arc_length = (int)Fct->Para[3];

    int index = 4;
    static int NofIcValues = (int)Fct->Para[index];
    index++;
    static std::vector<double> lengthValues(NofIcValues);
    static std::vector<double> IcValues(NofIcValues);
    for (int i = 0; i < NofIcValues; i++) {
        lengthValues[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofIcValues; i++) {
        IcValues[i] = (double)Fct->Para[index];
        index++;
    }

    static int NofMaterials = (int)Fct->Para[index];
    index++;
    static std::vector<MaterialName> materialNames(NofMaterials);
    static std::vector<double> relativeThicknesses(NofMaterials);
    static std::vector<double> RRRs(NofMaterials);
    static std::vector<double> RRRRefTs(NofMaterials);
    for (int i = 0; i < NofMaterials; i++) {
        materialNames[i] = (MaterialName)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        relativeThicknesses[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        RRRs[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        RRRRefTs[i] = (double)Fct->Para[index];
        index++;
    }

    static MaterialName superConductorMaterialInteger = (MaterialName)Fct->Para[index];
    index++;
    static double superConductorRelativeThickness = (double)Fct->Para[index];
    index++;

    static double localDefectStartTurn = (double)Fct->Para[index];
    index++;
    static double localDefectEndTurn = (double)Fct->Para[index];
    index++;
    static int whichPancakeCoil = (int)Fct->Para[index];
    index++;
    static double localDefectValue = (double)Fct->Para[index];
    index++;
    static double localDefectStartTime = (double)Fct->Para[index];
    index++;

    static double windingCADThickness = (double)Fct->Para[index];
    index++;
    static double windingTapeHeight = (double)Fct->Para[index];
    index++;

    static double scalingFactorDueToTSA = (double)Fct->Para[index];
    index++;

    index++;
    static double axialShuntRelativeHeight = (double)Fct->Para[index];
    index++;

    double windingThickness = windingCADThickness * scalingFactorDueToTSA;
    double hts_surface_area = windingTapeHeight * windingThickness * superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);

    double jCritical = jcritical(
        superConductorMaterialInteger,
        time, temperature, x, y, z, Bx, By, Bz,
        reference_temperature, reference_Bmagnitude, reference_Bangle, 
        lengthValues, IcValues,
        localDefectStartTurn,
        localDefectEndTurn, whichPancakeCoil, localDefectStartTime,
        localDefectValue, hts_surface_area, scalingFactorDueToTSA, Ic_in_arc_length);

    V->Val[0] = jCritical;
    V->Type = SCALAR;
}

// same input as F_FiQuS_Pancake3DCriticalCurrentDensity, simply call it and multiply by the HTS surface area
void F_FiQuS_Pancake3DCriticalCurrent(F_ARG)
{

    static int NofIcValues = (int)Fct->Para[4];
    //Message::Debug("NofIcValues: %i", NofIcValues);
    static int NofMaterials = (int)Fct->Para[5 + 2 * NofIcValues];
    //Message::Debug("NofMaterials: %i", NofMaterials);

    static MaterialName superConductorMaterialInteger = (MaterialName)Fct->Para[6 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("superConductorMaterialInteger: %i", superConductorMaterialInteger);

    static double superConductorRelativeThickness = (double)Fct->Para[7 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("superConductorRelativeThickness: %g", superConductorRelativeThickness);

    static double windingCADThickness = (double)Fct->Para[13 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("windingCADThickness: %g", windingCADThickness);
    static double windingTapeHeight = (double)Fct->Para[14 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("windingTapeHeight: %g", windingTapeHeight);
    static double scalingFactorDueToTSA = (double)Fct->Para[15 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("scalingFactorDueToTSA: %g", scalingFactorDueToTSA);

    static double axialShuntRelativeHeight = (double)Fct->Para[17 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("axialShuntRelativeHeight: %g", axialShuntRelativeHeight);

    static double windingThickness = windingCADThickness * scalingFactorDueToTSA;
    static double hts_surface_area = windingTapeHeight * windingThickness * superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);

    Value V_jc;
    F_FiQuS_Pancake3DCriticalCurrentDensity(Fct, &A[0], &V_jc);

    V->Val[0] = V_jc.Val[0] * hts_surface_area/scalingFactorDueToTSA;
    V->Type = SCALAR;
}

// instead of calling jCritical in here, one could call F_FiQuS_Pancake3DCriticalCurrentDensity
// not done as it would require to change the function signature of F_FiQuS_Pancake3DCriticalCurrentDensity
// or copy many values for its input arguments
void F_FiQuS_Pancake3DHTSCurrentSharingIndex(F_ARG)
{
    double time = (double)A->Val[0];

    double x = (double)(A + 1)->Val[0];
    double y = (double)(A + 1)->Val[1];
    double z = (double)(A + 1)->Val[2];

    double jx = (double)(A + 2)->Val[0];
    double jy = (double)(A + 2)->Val[1];
    double jz = (double)(A + 2)->Val[2];

    double Bx = (double)(A + 3)->Val[0];
    double By = (double)(A + 3)->Val[1];
    double Bz = (double)(A + 3)->Val[2];

    double temperature = (double)(A + 4)->Val[0];

    static double reference_temperature = (double)Fct->Para[0];
    static double reference_Bmagnitude = (double)Fct->Para[1];
    static double reference_Bangle = (double)Fct->Para[2];
    static int Ic_in_arc_length = (int)Fct->Para[3];
    static int isotropic_materials = (int)Fct->Para[4];

    int index = 5;
    static int NofIcValues = (int)Fct->Para[index];
    index++;
    static std::vector<double> lengthValues(NofIcValues);
    static std::vector<double> IcValues(NofIcValues);
    for (int i = 0; i < NofIcValues; i++) {
        lengthValues[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofIcValues; i++) {
        IcValues[i] = (double)Fct->Para[index];
        index++;
    }

    static int NofMaterials = (int)Fct->Para[index];
    index++;
    static std::vector<MaterialName> materialNames(NofMaterials);
    static std::vector<double> relativeThicknesses(NofMaterials);
    static std::vector<double> RRRs(NofMaterials);
    static std::vector<double> RRRRefTs(NofMaterials);
    for (int i = 0; i < NofMaterials; i++) {
        materialNames[i] = (MaterialName)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        relativeThicknesses[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        RRRs[i] = (double)Fct->Para[index];
        index++;
    }
    for (int i = 0; i < NofMaterials; i++) {
        RRRRefTs[i] = (double)Fct->Para[index];
        index++;
    }

    static MaterialName superConductorMaterialInteger = (MaterialName)Fct->Para[index];
    index++;
    static double superConductorRelativeThickness = (double)Fct->Para[index];
    index++;
    static double superConductorElectricFieldCriterion = (double)Fct->Para[index];
    index++;
    static double superConductorNValue = (double)Fct->Para[index];
    index++;

    static double localDefectStartTurn = (double)Fct->Para[index];
    index++;
    static double localDefectEndTurn = (double)Fct->Para[index];
    index++;
    static int whichPancakeCoil = (int)Fct->Para[index];
    index++;
    static double localDefectValue = (double)Fct->Para[index];
    index++;
    static  double localDefectStartTime = (double)Fct->Para[index];
    index++;

    static double windingCADThickness = (double)Fct->Para[index];
    index++;
    static double windingTapeHeight = (double)Fct->Para[index];
    index++;

    static double scalingFactorDueToTSA = (double)Fct->Para[index];
    index++;

    static MaterialName platingMaterialInteger = (MaterialName)Fct->Para[index];
    index++;
    static double axialShuntRelativeHeight = (double)Fct->Para[index];
    index++;
    static double platingRRR = (double)Fct->Para[index];
    index++; 
    static double platingRRRRefT = (double)Fct->Para[index];
    index++;

    double superConductorFraction = superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);

    Vec3 jInXYZCoordinates = Vec3(jx, jy, jz);
    Vec3 jInWindingCoordinates = jInXYZCoordinates.transformToWindingCoordinates(x, y, z);

    double windingThickness = windingCADThickness * scalingFactorDueToTSA;
    double hts_surface_area = windingTapeHeight * windingThickness * superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);
    double normalConductorFraction = 1.0 - superConductorFraction;

    double magnetic_field_magnitude = sqrt(Bx * Bx + By * By + Bz * Bz);

    double rhoShunt = rho(platingRRR, platingRRRRefT, platingMaterialInteger, temperature, magnetic_field_magnitude);

    static std::vector<double> fractions(NofMaterials);
    for (int i = 0; i < NofMaterials; i++) {
        fractions[i] = relativeThicknesses[i] * (1.0 - axialShuntRelativeHeight);
    }

    static std::vector<double> fractions_times_normalConductorFraction(NofMaterials + 1);
    for (int i = 0; i < NofMaterials; i++) {
        fractions_times_normalConductorFraction[i] = fractions[i] * normalConductorFraction;
    }
    fractions_times_normalConductorFraction[NofMaterials] = axialShuntRelativeHeight * normalConductorFraction;

    double rhoNormalMaterialsParallelToWinding = rhoNormalConductorParallelToWinding(RRRs, RRRRefTs, temperature,
        magnetic_field_magnitude, materialNames, fractions_times_normalConductorFraction, rhoShunt);

    double jCritical = jcritical(
        superConductorMaterialInteger,
        time, temperature, x, y, z, Bx, By, Bz,
        reference_temperature, reference_Bmagnitude, reference_Bangle, 
        lengthValues, IcValues,
        localDefectStartTurn,
        localDefectEndTurn, whichPancakeCoil, localDefectStartTime,
        localDefectValue, hts_surface_area, scalingFactorDueToTSA, Ic_in_arc_length);

    double lambdaValue = lambda(
        jInWindingCoordinates.x(), jInWindingCoordinates.y(),
        jInWindingCoordinates.z(), x, y, z, jCritical,
        rhoNormalMaterialsParallelToWinding, superConductorElectricFieldCriterion,
        superConductorNValue, superConductorFraction,
        normalConductorFraction, windingThickness, isotropic_materials); 

    V->Val[0] = lambdaValue;
    V->Type = SCALAR;
}

// same input arguments as F_FiQuS_Pancake3DHTSCurrentSharingIndex since it is the main complicated function that needs to 
// be called
void F_FiQuS_Pancake3DHTSCurrentDensity(F_ARG)
{
    double x = (double)(A + 1)->Val[0];
    double y = (double)(A + 1)->Val[1];
    double z = (double)(A + 1)->Val[2];

    double jx = (double)(A + 2)->Val[0];
    double jy = (double)(A + 2)->Val[1];
    double jz = (double)(A + 2)->Val[2];

    Vec3 jInXYZCoordinates = Vec3(jx, jy, jz);
    Vec3 jInWindingCoordinates = jInXYZCoordinates.transformToWindingCoordinates(x, y, z);

    static int isotropic_materials = (int)Fct->Para[4];

    static int NofIcValues = (int)Fct->Para[5];
    //Message::Debug("NofIcValues: %i", NofIcValues);
    static int NofMaterials = (int)Fct->Para[6 + 2 * NofIcValues];
    //Message::Debug("NofMaterials: %i", NofMaterials);

    static double superConductorRelativeThickness = (double)Fct->Para[8 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("superConductorRelativeThickness: %g", superConductorRelativeThickness);

    static double axialShuntRelativeHeight = (double)Fct->Para[20 + 2 * NofIcValues + 4 * NofMaterials];
    //Message::Debug("axialShuntRelativeHeight: %g", axialShuntRelativeHeight);
    static double superConductorFraction = superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);

    Value V_lambda;
    F_FiQuS_Pancake3DHTSCurrentSharingIndex(Fct, &A[0], &V_lambda);
    double lambdaValue = V_lambda.Val[0];

    Vec3 jHTSInWindingCoordinates;

    if (isotropic_materials == 0) {
            jHTSInWindingCoordinates = Vec3(
                0,
                jInWindingCoordinates.y() * lambdaValue / superConductorFraction,
                jInWindingCoordinates.z() * lambdaValue / superConductorFraction);
    } else {
            jHTSInWindingCoordinates = jInWindingCoordinates * lambdaValue / superConductorFraction;

    }

    Vec3 jHTSInXYZCoordinates =  jHTSInWindingCoordinates.transformToXYZCoordinates(x, y, z);

    V->Val[0] = jHTSInXYZCoordinates.x();
    V->Val[1] = jHTSInXYZCoordinates.y();
    V->Val[2] = jHTSInXYZCoordinates.z();

    V->Type = VECTOR;
}

void F_FiQuS_Pancake3DPowerDensity(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    double time = (double) (A+1)->Val[0];

    static double start_time = (double)Fct->Para[0];
    static double end_time = (double)Fct->Para[1];
    static double start_length = (double)Fct->Para[2];
    static double end_length = (double)Fct->Para[3];
    // power in Watt
    static double power_input = (double)Fct->Para[4];

    static double r_i = (double)Fct->Para[5];
    static double w_t = (double)Fct->Para[6];
    static double gap_t = (double)Fct->Para[7];
    static double theta_i = (double)Fct->Para[8];
    static double ane = (double)Fct->Para[9];
    static double NofPancakeCoils = (double)Fct->Para[10];
    static double h = (double)Fct->Para[11];
    static double gapBetweenPancakeCoils = (double)Fct->Para[12];

    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    double turn_number = winding.getArcLength(Vec3(x, y, z));

    double powerDensity; 

    if (turn_number >= start_length && turn_number <= end_length && time >= start_time && time <= end_time) {
        double volume = (end_length - start_length) * h * w_t;
        powerDensity = power_input/volume;
    } else {
        powerDensity = 0;
    }

    V->Val[0] = powerDensity;
    V->Type = SCALAR;
}

void F_FiQuS_Pancake3D_getNormal(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    static double r_i = (double)Fct->Para[0];
    static double w_t = (double)Fct->Para[1];
    static double gap_t = (double)Fct->Para[2];
    static double theta_i = (double)Fct->Para[3];
    static double ane = (double)Fct->Para[4];
    static double NofPancakeCoils = (double)Fct->Para[5];
    static double h = (double)Fct->Para[6];
    static double gapBetweenPancakeCoils = (double)Fct->Para[7];

    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    Vec3 normalVector = winding.getNormal(Vec3(x, y, z));

    V->Val[0] = normalVector.x();
    V->Val[1] = normalVector.y();
    V->Val[2] = normalVector.z();
    V->Type = VECTOR;
}

void F_FiQuS_Pancake3D_getContinuousArcLength(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    static double r_i = (double)Fct->Para[0];
    static double w_t = (double)Fct->Para[1];
    static double gap_t = (double)Fct->Para[2];
    static double theta_i = (double)Fct->Para[3];
    static double ane = (double)Fct->Para[4];
    static double NofPancakeCoils = (double)Fct->Para[5];
    static double h = (double)Fct->Para[6];
    static double gapBetweenPancakeCoils = (double)Fct->Para[7];

    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    double arcLength = winding.getArcLength(Vec3(x, y, z));

    V->Val[0] = arcLength;
    V->Type = SCALAR;
}

void F_FiQuS_Pancake3D_getContinuousArcLength_ContactLayer(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    static double r_i = (double)Fct->Para[0];
    static double w_t = (double)Fct->Para[1];
    static double gap_t = (double)Fct->Para[2];
    static double theta_i = (double)Fct->Para[3];
    static double ane = (double)Fct->Para[4];
    static double NofPancakeCoils = (double)Fct->Para[5];
    static double h = (double)Fct->Para[6];
    static double gapBetweenPancakeCoils = (double)Fct->Para[7];


    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    double arcLength_contactLayer = winding.getArcLengthContactLayer(Vec3(x, y, z));

    V->Val[0] = arcLength_contactLayer;
    V->Type = SCALAR;
}

void F_FiQuS_Pancake3D_getContinuousTurnNumber(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    static double r_i = (double)Fct->Para[0];
    static double w_t = (double)Fct->Para[1];
    static double gap_t = (double)Fct->Para[2];
    static double theta_i = (double)Fct->Para[3];
    static double ane = (double)Fct->Para[4];
    static double NofPancakeCoils = (double)Fct->Para[5];
    static double h = (double)Fct->Para[6];
    static double gapBetweenPancakeCoils = (double)Fct->Para[7];

    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    double turnNumber = winding.getTurnNumber(Vec3(x, y, z));

    V->Val[0] = turnNumber;
    V->Type = SCALAR;
}

void F_FiQuS_Pancake3D_getContinuousTurnNumber_ContactLayer(F_ARG)
{
    double x = (double)A->Val[0];
    double y = (double)A->Val[1];
    double z = (double)A->Val[2];

    static double r_i = (double)Fct->Para[0];
    static double w_t = (double)Fct->Para[1];
    static double gap_t = (double)Fct->Para[2];
    static double theta_i = (double)Fct->Para[3];
    static double ane = (double)Fct->Para[4];
    static double NofPancakeCoils = (double)Fct->Para[5];
    static double h = (double)Fct->Para[6];
    static double gapBetweenPancakeCoils = (double)Fct->Para[7];


    static Winding& winding = Winding::get_winding_object(
        r_i, w_t, gap_t, theta_i, ane, h, NofPancakeCoils, gapBetweenPancakeCoils);
    double turnNumber = winding.getTurnNumberContactLayer(Vec3(x, y, z));

    V->Val[0] = turnNumber;
    V->Type = SCALAR;
}

bool rho_and_derivative_initialized = false;

void F_FiQuS_Pancake3D_WindingWithSuperConductorRhoAndDerivativeV1(F_ARG)
{
    // inputs:
    // variable parameters:
    // 0: time
    // 1: XYZ[]
    // 2: J[]
    // 3: B[]
    // 4: temperature
    // constant parameters:
    // 5: Ic reference temperature
    // 6: Ic reference magnetic field magnitude
    // 7: Ic reference magnetic field angle
    // 8: Ic given in arc length (1) or in turnNumber (0)
    // 9: isotropic (1) or anisotropic (0) materials
    // 10: NofIcValues
    // 11: lengthValues 1
    // 12: lengthValues 2
    // ...
    // ...: lengthValues NofIcValues
    // ...: IcValues 1
    // ...: IcValues 2
    // ...
    // ...: IcValues NofIcValues
    // ...: NofMaterials
    // ...: materialInteger 1
    // ...: materialInteger 2
    // ...
    // ...: materialInteger NofMaterials
    // ...: relativeThickness 1
    // ...: relativeThickness 2
    // ...
    // ...: relativeThickness NofMaterials
    // ...: RRR 1
    // ...: RRR 2
    // ...
    // ...: RRR NofMaterials
    // ...: RRRRefT 1
    // ...: RRRRefT 2
    // ...
    // ...: RRRRefT NofMaterials
    // ...: platingMaterialInteger
    // ...: axialShuntRelativeHeight
    // ...: platingRRR
    // ...: platingRRRRefT
    // ...: superConductorMaterialInteger
    // ...: superConductorRelativeThickness
    // ...: superConductorElectricFieldCriterion
    // ...: superConductorNValue
    // ...: minimumPossibleRho
    // ...: maximumPossibleRho
    // ...: localDefectStartTurn
    // ...: localDefectEndTurn
    // ...: whichPancakeCoil
    // ...: localDefectValue
    // ...: localDefectStartTime
    // ...: windingInnerRadius
    // ...: windingThickness
    // ...: windingGapThickness
    // ...: windingStartAngle
    // ...: windingAzimuthalNumberOfElements
    // ...: NofPancakeCoils
    // ...: windingTapeHeight
    // ...: windingGapBetweenPancakeCoils
    // ...: scalingFactorDueToTSA
    // ...: arbitraryjCrticalScalingNormalToWinding

    double time = (double)A->Val[0];

    double x = (double)(A + 1)->Val[0];
    double y = (double)(A + 1)->Val[1];
    double z = (double)(A + 1)->Val[2];

    double jx = (double)(A + 2)->Val[0];
    double jy = (double)(A + 2)->Val[1];
    double jz = (double)(A + 2)->Val[2];

    double Bx = (double)(A + 3)->Val[0];
    double By = (double)(A + 3)->Val[1];
    double Bz = (double)(A + 3)->Val[2];

    double temperature = (double)(A + 4)->Val[0];

    static double reference_temperature;
    static double reference_Bmagnitude;
    static double reference_Bangle;
    static int Ic_in_arc_length;
    static int isotropic_materials;
    static double platingRRR;
    static double platingRRRRefT;
    static MaterialName platingMaterialInteger;

    static std::vector<MaterialName> materialNames;
    static std::vector<double> relativeThicknesses;
    static std::vector<double> RRRs;
    static std::vector<double> RRRRefTs;
    static std::vector<double> fractions_times_normalConductorFraction;

    static double normalConductorFraction;
    static double axialShuntRelativeHeight;

    static MaterialName superConductorMaterialInteger;

    static double superConductorRelativeThickness;
    static double superConductorFraction;
    static double superConductorElectricFieldCriterion;
    static double superConductorNValue;

    static double windingCADThickness;
    static double localDefectStartTurn;
    static double localDefectEndTurn;
    static int whichPancakeCoil;
    static double localDefectValue;
    static double localDefectStartTime;

    static double scalingFactorDueToTSA;
    static double minimumPossibleRho;
    static double maximumPossibleRho;

    static double windingTapeHeight;
    static std::vector<double> lengthValues;
    static std::vector<double> IcValues;

    if (!rho_and_derivative_initialized) {
        reference_temperature = (double)Fct->Para[0];
        reference_Bmagnitude = (double)Fct->Para[1];
        reference_Bangle = (double)Fct->Para[2];
        Ic_in_arc_length = (int)Fct->Para[3];
        isotropic_materials = (int)Fct->Para[4];

        int index = 5;
        int NofIcValues = (int)Fct->Para[index];
        index++;
        lengthValues.resize(NofIcValues);
        IcValues.resize(NofIcValues);
        for (int i = 0; i < NofIcValues; i++) {
            lengthValues[i] = (double)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofIcValues; i++) {
            IcValues[i] = (double)Fct->Para[index];
            index++;
        }

        int NofMaterials = (int)Fct->Para[index];
        index++;
        materialNames.resize(NofMaterials);
        relativeThicknesses.resize(NofMaterials);
        RRRs.resize(NofMaterials);
        RRRRefTs.resize(NofMaterials);
        for (int i = 0; i < NofMaterials; i++) {
            materialNames[i] = (MaterialName)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            relativeThicknesses[i] = (double)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            RRRs[i] = (double)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            RRRRefTs[i] = (double)Fct->Para[index];
            index++;
        }
        platingMaterialInteger = (MaterialName)Fct->Para[index];
        index++;
        axialShuntRelativeHeight = (double)Fct->Para[index];
        index++;
        platingRRR = (double)Fct->Para[index];
        index++;
        platingRRRRefT = (double)Fct->Para[index];
        index++;

        std::vector<double> fractions(NofMaterials);
        for (int i = 0; i < NofMaterials; i++) {
            fractions[i] = relativeThicknesses[i] * (1.0 - axialShuntRelativeHeight);
        }

        superConductorMaterialInteger = (MaterialName)Fct->Para[index];
        index++;
        superConductorRelativeThickness = (double)Fct->Para[index];
        superConductorFraction = superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);
        index++;
        superConductorElectricFieldCriterion = (double)Fct->Para[index];
        index++;
        superConductorNValue = (double)Fct->Para[index];
        index++;

        normalConductorFraction = 1.0 - superConductorFraction;

        fractions_times_normalConductorFraction.resize(NofMaterials + 1);
        for (int i = 0; i < NofMaterials; i++) {
            fractions_times_normalConductorFraction[i] = fractions[i] * normalConductorFraction;
        }

        fractions_times_normalConductorFraction[NofMaterials] = axialShuntRelativeHeight * normalConductorFraction;

        minimumPossibleRho = (double)Fct->Para[index];
        index++;
        maximumPossibleRho = (double)Fct->Para[index];
        index++;

        localDefectStartTurn = (double)Fct->Para[index];
        index++;
        localDefectEndTurn = (double)Fct->Para[index];
        index++;
        whichPancakeCoil = (int)Fct->Para[index];
        index++;
        localDefectValue = (double)Fct->Para[index];
        index++;
        localDefectStartTime = (double)Fct->Para[index];
        index++;

        double windingInnerRadius = (double)Fct->Para[index];
        index++;
        windingCADThickness = (double)Fct->Para[index];
        index++;
        double windingGapThickness = (double)Fct->Para[index];
        index++;
        double windingStartAngle = (double)Fct->Para[index];
        index++;
        double windingAzimuthalNumberOfElements = (double)Fct->Para[index];
        index++;
        double NofPancakeCoils = (double)Fct->Para[index];
        index++;
        windingTapeHeight = (double)Fct->Para[index];
        index++;
        double windingGapBetweenPancakeCoils = (double)Fct->Para[index];
        index++;

        scalingFactorDueToTSA = (double)Fct->Para[index];
        index++;
        double arbitraryjCrticalScalingNormalToWinding = (double)Fct->Para[index];
        index++;

        // initialize winding object:
        Winding::get_winding_object(
            windingInnerRadius, windingCADThickness, windingGapThickness,
            windingStartAngle, windingAzimuthalNumberOfElements, windingTapeHeight,
            NofPancakeCoils, windingGapBetweenPancakeCoils);
    } 
    
    Vec3 jInWindingCoordinates = Vec3(jx, jy, jz).transformToWindingCoordinates(x, y, z);

    double magnetic_field_magnitude = sqrt(Bx * Bx + By * By + Bz * Bz);

    double rhoShunt = rho(platingRRR, platingRRRRefT, platingMaterialInteger, temperature,
        magnetic_field_magnitude); 

    double rhoNormalMaterialsParallelToWinding = rhoNormalConductorParallelToWinding(RRRs, RRRRefTs, temperature,
        magnetic_field_magnitude, materialNames, fractions_times_normalConductorFraction, rhoShunt);

    static double windingThickness;
    static double hts_surface_area;

    if (!rho_and_derivative_initialized) {
        windingThickness = windingCADThickness * scalingFactorDueToTSA;
        hts_surface_area = windingTapeHeight * windingThickness * superConductorRelativeThickness * (1.0 - axialShuntRelativeHeight);
    }
    
    double jCritical = jcritical(
        superConductorMaterialInteger,
        time, temperature, x, y, z, Bx, By, Bz,
        reference_temperature, reference_Bmagnitude, reference_Bangle, 
        lengthValues, IcValues,
        localDefectStartTurn,
        localDefectEndTurn, whichPancakeCoil, localDefectStartTime,
        localDefectValue, hts_surface_area, scalingFactorDueToTSA, Ic_in_arc_length);

    double lambdaValue = lambda(
        jInWindingCoordinates.x(), jInWindingCoordinates.y(),
        jInWindingCoordinates.z(), x, y, z, jCritical,
        rhoNormalMaterialsParallelToWinding, superConductorElectricFieldCriterion,
        superConductorNValue, superConductorFraction,
        normalConductorFraction, windingThickness, isotropic_materials);

    Vec3 lambdaDerivativeWrtJInWindingCoords = lambdaDerivative(
        lambdaValue, jInWindingCoordinates.x(), jInWindingCoordinates.y(),
        jInWindingCoordinates.z(), x, y, z, jCritical,
        rhoNormalMaterialsParallelToWinding, superConductorElectricFieldCriterion,
        superConductorNValue, superConductorFraction,
        normalConductorFraction, windingThickness, isotropic_materials);
    
    static Vec3 jHTSInWindingCoordinates;
    static Tensor jHTSDerivativewrtJInWindingCoords;

    if (isotropic_materials == 0) {
        jHTSInWindingCoordinates = Vec3(
            0,
            jInWindingCoordinates.y() * lambdaValue / superConductorFraction,
            jInWindingCoordinates.z() * lambdaValue / superConductorFraction);

        jHTSDerivativewrtJInWindingCoords = Tensor(0.0, 0.0, 0.0,
            0.0,
            lambdaDerivativeWrtJInWindingCoords.y() / superConductorFraction * jInWindingCoordinates.y() + lambdaValue / superConductorFraction,
            lambdaDerivativeWrtJInWindingCoords.z() / superConductorFraction * jInWindingCoordinates.y(),
            0.0,
            lambdaDerivativeWrtJInWindingCoords.y() / superConductorFraction * jInWindingCoordinates.z(),
            lambdaDerivativeWrtJInWindingCoords.z() / superConductorFraction * jInWindingCoordinates.z() + lambdaValue / superConductorFraction);
    }
    else {
        jHTSInWindingCoordinates = jInWindingCoordinates * lambdaValue / superConductorFraction;

        jHTSDerivativewrtJInWindingCoords = Tensor(            
            lambdaDerivativeWrtJInWindingCoords.x() / superConductorFraction * jInWindingCoordinates.x() + lambdaValue / superConductorFraction,
            lambdaDerivativeWrtJInWindingCoords.y() / superConductorFraction * jInWindingCoordinates.x(),
            lambdaDerivativeWrtJInWindingCoords.z() / superConductorFraction * jInWindingCoordinates.x(),
            lambdaDerivativeWrtJInWindingCoords.x() / superConductorFraction * jInWindingCoordinates.y(),
            lambdaDerivativeWrtJInWindingCoords.y() / superConductorFraction * jInWindingCoordinates.y() + lambdaValue / superConductorFraction,
            lambdaDerivativeWrtJInWindingCoords.z() / superConductorFraction * jInWindingCoordinates.y(),
            lambdaDerivativeWrtJInWindingCoords.x() / superConductorFraction * jInWindingCoordinates.z(),
            lambdaDerivativeWrtJInWindingCoords.y() / superConductorFraction * jInWindingCoordinates.z(),
            lambdaDerivativeWrtJInWindingCoords.z() / superConductorFraction * jInWindingCoordinates.z() + lambdaValue / superConductorFraction);
    }

    double rhoSuperConductorParallelToWinding = rhoSuperConductor(
        jHTSInWindingCoordinates.magnitude(), jCritical,
        superConductorElectricFieldCriterion, superConductorNValue);
    Vec3 rhoSuperConductorParallelToWindingDerivativeWrtJInWindingCoords = jHTSDerivativewrtJInWindingCoords * rhoSuperConductorDerivativeWRTj(jHTSInWindingCoordinates.x(), jHTSInWindingCoordinates.y(), jHTSInWindingCoordinates.z(), jCritical, superConductorElectricFieldCriterion, superConductorNValue);

    static std::vector<double> parallelRhos(2);

    parallelRhos[0] = rhoNormalMaterialsParallelToWinding;
    parallelRhos[1] = rhoSuperConductorParallelToWinding;
    
    const static std::vector<double> parallelFractions = {
        normalConductorFraction, superConductorFraction
    };
    double rhoParallelToWinding = parallel_connection_of_rhos(parallelRhos, parallelFractions);
    Vec3 rhoParallelToWindingDerivativeWRTjInWindingCoords = parallel_connection_of_rhos_derivative(
        rhoParallelToWinding, rhoSuperConductorParallelToWinding,
        superConductorFraction,
        rhoSuperConductorParallelToWindingDerivativeWrtJInWindingCoords.x(),
        rhoSuperConductorParallelToWindingDerivativeWrtJInWindingCoords.y(),
        rhoSuperConductorParallelToWindingDerivativeWrtJInWindingCoords.z());

    static double rhoNormalToWinding;

    if (isotropic_materials == 0) {
        rhoNormalToWinding = parallel_connection_of_rhos({ rhoShunt }, { axialShuntRelativeHeight });
    }

    if (isotropic_materials == 0) {

        rhoNormalToWinding = std::min(std::max(rhoNormalToWinding, minimumPossibleRho), maximumPossibleRho);

        static Tensor rhoTensorDerivativeInWindingCoordinatesTimesJ ;
        if (rhoParallelToWinding < minimumPossibleRho) {
            rhoParallelToWinding = minimumPossibleRho;
            rhoTensorDerivativeInWindingCoordinatesTimesJ = Tensor(0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else if (rhoParallelToWinding > maximumPossibleRho) {
            rhoParallelToWinding = maximumPossibleRho;
            rhoTensorDerivativeInWindingCoordinatesTimesJ = Tensor(0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            rhoTensorDerivativeInWindingCoordinatesTimesJ = Tensor(
                0,
                0,
                0,
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.x() * jInWindingCoordinates.y(),
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.y() * jInWindingCoordinates.y(),
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.z() * jInWindingCoordinates.y(),
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.x() * jInWindingCoordinates.z(),
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.y() * jInWindingCoordinates.z(),
                1.0 / scalingFactorDueToTSA * rhoParallelToWindingDerivativeWRTjInWindingCoords.z() * jInWindingCoordinates.z());
        }
        
        // in normal direction it is too large as the thickness of the bare material
        // increase --> we need to decrease it In parallel direction the resistivity
        // we have in the TSA model is too small due to the increase in cross-section
        // --> we need to increase it
        Tensor rhoTensor = Tensor(scalingFactorDueToTSA * rhoNormalToWinding, 0.0, 0.0, 0.0, rhoParallelToWinding / scalingFactorDueToTSA, 0.0, 0.0, 0.0, rhoParallelToWinding / scalingFactorDueToTSA).transformToXYZCoordinates(x, y, z);
        Tensor rhoDerivative = rhoTensorDerivativeInWindingCoordinatesTimesJ.transformToXYZCoordinates(
        x, y, z);

        V->Type = TWO_TENSORS;
        V->Val[0] = rhoTensor.xx();
        V->Val[1] = rhoTensor.xy();
        V->Val[2] = rhoTensor.xz();
        V->Val[3] = rhoTensor.yx();
        V->Val[4] = rhoTensor.yy();
        V->Val[5] = rhoTensor.yz();
        V->Val[6] = rhoTensor.zx();
        V->Val[7] = rhoTensor.zy();
        V->Val[8] = rhoTensor.zz();
        V->Val[9 + 0] = rhoDerivative.xx();
        V->Val[9 + 1] = rhoDerivative.xy();
        V->Val[9 + 2] = rhoDerivative.xz();
        V->Val[9 + 3] = rhoDerivative.yx();
        V->Val[9 + 4] = rhoDerivative.yy();
        V->Val[9 + 5] = rhoDerivative.yz();
        V->Val[9 + 6] = rhoDerivative.zx();
        V->Val[9 + 7] = rhoDerivative.zy();
        V->Val[9 + 8] = rhoDerivative.zz();
    } else {
        double rho = rhoParallelToWinding / scalingFactorDueToTSA;

        static Tensor rhoDerivativeTimesJ;

        if (rho < minimumPossibleRho) {
            rho = minimumPossibleRho;
            rhoDerivativeTimesJ = Tensor(0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else if (rho > maximumPossibleRho) {
            rho = maximumPossibleRho;
            rhoDerivativeTimesJ = Tensor(0, 0, 0, 0, 0, 0, 0, 0, 0);
        } else {
            rhoDerivativeTimesJ = Tensor(rhoParallelToWindingDerivativeWRTjInWindingCoords.x() * jInWindingCoordinates.x(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.y() * jInWindingCoordinates.x(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.z() * jInWindingCoordinates.x(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.x() * jInWindingCoordinates.y(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.y() * jInWindingCoordinates.y(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.z() * jInWindingCoordinates.y(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.x() * jInWindingCoordinates.z(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.y() * jInWindingCoordinates.z(),
                rhoParallelToWindingDerivativeWRTjInWindingCoords.z() * jInWindingCoordinates.z());
        }

        Tensor rhoDerivative = rhoDerivativeTimesJ.transformToXYZCoordinates(x, y, z);

        V->Type = TWO_TENSORS;
        V->Val[0] = rho;
        V->Val[1] = 0;
        V->Val[2] = 0;
        V->Val[3] = 0;
        V->Val[4] = 0;
        V->Val[5] = 0;
        V->Val[6] = 0;
        V->Val[7] = 0;
        V->Val[8] = 0;
        V->Val[9 + 0] = rhoDerivative.xx();
        V->Val[9 + 1] = rhoDerivative.xy();
        V->Val[9 + 2] = rhoDerivative.xz();
        V->Val[9 + 3] = rhoDerivative.yx();
        V->Val[9 + 4] = rhoDerivative.yy();
        V->Val[9 + 5] = rhoDerivative.yz();
        V->Val[9 + 6] = rhoDerivative.zx();
        V->Val[9 + 7] = rhoDerivative.zy();
        V->Val[9 + 8] = rhoDerivative.zz();
    }

    rho_and_derivative_initialized = true;
}

void F_FiQuS_GetFirstTensor(F_ARG)
{
    if (A->Type != TWO_TENSORS) {
        Message::Error("This is not a TWO_TENSORS object! %d", A->Type);
    }

    double xx = (double)(A)->Val[0];
    double xy = (double)(A)->Val[1];
    double xz = (double)(A)->Val[2];
    double yx = (double)(A)->Val[3];
    double yy = (double)(A)->Val[4];
    double yz = (double)(A)->Val[5];
    double zx = (double)(A)->Val[6];
    double zy = (double)(A)->Val[7];
    double zz = (double)(A)->Val[8];

    V->Type = TENSOR;
    V->Val[0] = xx;
    V->Val[1] = xy;
    V->Val[2] = xz;
    V->Val[3] = yx;
    V->Val[4] = yy;
    V->Val[5] = yz;
    V->Val[6] = zx;
    V->Val[7] = zy;
    V->Val[8] = zz;
}

void F_FiQuS_GetSecondTensor(F_ARG)
{
    if (A->Type != TWO_TENSORS) {
        Message::Error("This is not a TWO_TENSORS object! %d", A->Type);
    }

    double xx = (double)(A)->Val[9];
    double xy = (double)(A)->Val[10];
    double xz = (double)(A)->Val[11];
    double yx = (double)(A)->Val[12];
    double yy = (double)(A)->Val[13];
    double yz = (double)(A)->Val[14];
    double zx = (double)(A)->Val[15];
    double zy = (double)(A)->Val[16];
    double zz = (double)(A)->Val[17];

    V->Type = TENSOR;
    V->Val[0] = xx;
    V->Val[1] = xy;
    V->Val[2] = xz;
    V->Val[3] = yx;
    V->Val[4] = yy;
    V->Val[5] = yz;
    V->Val[6] = zx;
    V->Val[7] = zy;
    V->Val[8] = zz;
}


bool thermal_conductivity_initialized = false;

void F_FiQuS_Pancake3D_WindingThermalConductivity(F_ARG)
{
    // inputs:
    // variable parameters:
    // 0: XYZ[]
    // 1: B[]
    // 2: temperature
    // constant parameters:
    // 3: isotropic material
    // 4: NofMaterials
    // 5: materialInteger 1
    // 6: materialInteger 2
    // ...
    // ...: materialInteger NofMaterials
    // ...: relativeThickness 1
    // ...: relativeThickness 2
    // ...
    // ...: relativeThickness NofMaterials
    // ...: RRR 1
    // ...: RRR 2
    // ...
    // ...: RRR NofMaterials
    // ...: RRRRefT 1
    // ...: RRRRefT 2
    // ...
    // ...: RRRRefT NofMaterials
    // ...: platingMaterialInteger
    // ...: axialShuntRelativeHeight
    // ...: platingRRR
    // ...: platingRRRRefT
    // ...: windingInnerRadius
    // ...: windingThickness
    // ...: windingGapThickness
    // ...: windingStartAngle
    // ...: windingAzimuthalNumberOfElements
    // ...: NofPancakeCoils
    // ...: windingTapeHeight
    // ...: windingGapBetweenPancakeCoils
    // ...: scalingFactorDueToTSA

    double x = (double)(A)->Val[0];
    double y = (double)(A)->Val[1];
    double z = (double)(A)->Val[2];

    double Bx = (double)(A + 1)->Val[0];
    double By = (double)(A + 1)->Val[1];
    double Bz = (double)(A + 1)->Val[2];

    double temperature = (double)(A + 2)->Val[0];

    static int NofMaterials;

    static std::vector<MaterialName> materialNames;
    static std::vector<double> relativeThicknesses;
    static std::vector<double> RRRs;
    static std::vector<double> RRRRefTs;

    static double axialShuntRelativeHeight;
    static double platingRRR;
    static double platingRRRRefT;

    static MaterialName platingMaterialInteger;
    static int isotropic_materials;
    static double scalingFactorDueToTSA;
    static std::vector<double> fractions;

    if (!thermal_conductivity_initialized) {
        isotropic_materials = (int)Fct->Para[0];

        int index = 1;
        NofMaterials = (int)Fct->Para[index];
        index++;
        materialNames.resize(NofMaterials);
        relativeThicknesses.resize(NofMaterials);
        RRRs.resize(NofMaterials);
        RRRRefTs.resize(NofMaterials);
        for (int i = 0; i < NofMaterials; i++) {
            materialNames[i] = (MaterialName)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            relativeThicknesses[i] = (double)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            RRRs[i] = (double)Fct->Para[index];
            index++;
        }
        for (int i = 0; i < NofMaterials; i++) {
            RRRRefTs[i] = (double)Fct->Para[index];
            index++;
        }
    
        platingMaterialInteger = (MaterialName)Fct->Para[index];
        index++;
        axialShuntRelativeHeight = (double)Fct->Para[index];
        index++;
        platingRRR = (double)Fct->Para[index];
        index++;
        platingRRRRefT = (double)Fct->Para[index];
        index++;
    
        double windingInnerRadius = (double)Fct->Para[index];
        index++;
        double windingCADThickness = (double)Fct->Para[index];
        index++;
        double windingGapThickness = (double)Fct->Para[index];
        index++;
        double windingStartAngle = (double)Fct->Para[index];
        index++;
        double windingAzimuthalNumberOfElements = (double)Fct->Para[index];
        index++;
        double NofPancakeCoils = (double)Fct->Para[index];
        index++;
        double windingTapeHeight = (double)Fct->Para[index];
        index++;
        double windingGapBetweenPancakeCoils = (double)Fct->Para[index];
        index++;
    
        scalingFactorDueToTSA = (double)Fct->Para[index];
        index++;
    
        // initialize winding object:
        Winding::get_winding_object(
            windingInnerRadius, windingCADThickness, windingGapThickness,
            windingStartAngle, windingAzimuthalNumberOfElements, windingTapeHeight,
            NofPancakeCoils, windingGapBetweenPancakeCoils);

        fractions.resize(NofMaterials + 1);
        for (int i = 0; i < NofMaterials; i++) {
            fractions[i] = relativeThicknesses[i] * (1.0 - axialShuntRelativeHeight);
        }
        fractions[NofMaterials] = axialShuntRelativeHeight;

    }

    double magnetic_field_magnitude = sqrt(Bx * Bx + By * By + Bz * Bz);

    double kappaShunt = kappa(platingRRR, platingRRRRefT, platingMaterialInteger, temperature, 0.0);

    double kappaAzimuthalDirectionValue = kappaAzimuthalDirection(
        RRRs, RRRRefTs, temperature, magnetic_field_magnitude, materialNames,
        fractions, kappaShunt);

    if (isotropic_materials == 0){
        // kappa normal to winding 
        static std::vector<double> fractions_times_nonAxialShunt(NofMaterials);

        if(!thermal_conductivity_initialized){
            for (int i = 0; i < NofMaterials; i++) {
                fractions_times_nonAxialShunt[i] = fractions[i] * (1.0 - axialShuntRelativeHeight);
            }
        }
        
        double kappaNormalToWindingWithoutShuntValue = kappaNormalToWindingWithoutShunt(RRRs, RRRRefTs, temperature, magnetic_field_magnitude,
            materialNames, fractions_times_nonAxialShunt);

        static std::vector<double> kappasNormalToWinding(2);
        kappasNormalToWinding[0] = kappaShunt;
        kappasNormalToWinding[1] = kappaNormalToWindingWithoutShuntValue;
        static const std::vector<double> fractions_shunt_nonShunt = {
            axialShuntRelativeHeight, 1.0 - axialShuntRelativeHeight
        };
        double kappaNormalToWindingValue = parallel_connection_of_kappas(kappasNormalToWinding, fractions_shunt_nonShunt);

        // kappa axial
        static std::vector<double> fractions_div_nonAxialShunt(NofMaterials);
        if(!thermal_conductivity_initialized){
            for (int i = 0; i < NofMaterials; i++) {
                fractions_div_nonAxialShunt[i] = fractions[i] / (1.0 - axialShuntRelativeHeight);
            }
        }

        double kappaAxialDirectionWithoutShuntValue = kappaAxialDirectionWithoutShunt(RRRs, RRRRefTs, temperature, magnetic_field_magnitude,
            materialNames, fractions_div_nonAxialShunt);
        static std::vector<double> kappasAxialDirection(2);
        kappasAxialDirection[0] = kappaShunt;
        kappasAxialDirection[1] = kappaAxialDirectionWithoutShuntValue;
        
        double kappaAxialDirectionValue = series_connection_of_kappas(kappasAxialDirection, fractions_shunt_nonShunt);

        // in normal direction it is too large as the thickness of the bare material
        // increase --> we need to decrease it In parallel direction the resistivity
        // we have in the TSA model is too small due to the increase in cross-section
        // --> we need to increase it
        Tensor kappaTensor = Tensor(kappaNormalToWindingValue / scalingFactorDueToTSA, 0.0, 0.0, 0.0,
            kappaAzimuthalDirectionValue * scalingFactorDueToTSA, 0.0, 0.0, 0.0,
            kappaAxialDirectionValue * scalingFactorDueToTSA).transformToXYZCoordinates(x, y, z);

        V->Type = TENSOR;
        V->Val[0] = kappaTensor.xx();
        V->Val[1] = kappaTensor.xy();
        V->Val[2] = kappaTensor.xz();
        V->Val[3] = kappaTensor.yx();
        V->Val[4] = kappaTensor.yy();
        V->Val[5] = kappaTensor.yz();
        V->Val[6] = kappaTensor.zx();
        V->Val[7] = kappaTensor.zy();
        V->Val[8] = kappaTensor.zz();
    } else {
        V->Type = SCALAR;
        V->Val[0] = kappaAzimuthalDirectionValue * scalingFactorDueToTSA;
    }

    thermal_conductivity_initialized = true;
}