#include <math.h>
#include <stdlib.h>
#include "F.h"
#include "Message.h"
#include <float.h>
#include <cstring>
#include <limits>

constexpr int blockSize = 1;

// Here, the steam material functions will be added. On the runner, the files will be found as artifacts of the build.
