#include <stdlib.h>
#include "F_CERN_Utils.h"

 void initInput(int nArgs, int blockSize, double **inReal, double **inImag) {
  for (int i = 0; i < nArgs; ++i) {
    inReal[i] = new double[blockSize];
    inImag[i] = new double[blockSize];
  }
};

void freeInputOutput(int nArgs, double **inReal, double **inImag,
                double* outReal, double* outImag) {
    // free pointers
    for(int i = 0; i < nArgs; i++) {
      free(inReal[i]);
      free(inImag[i]);
    }
    free(inReal);
    free(inImag);
    free(outReal);
    free(outImag);
};
