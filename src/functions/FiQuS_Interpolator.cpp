#include <math.h>
#include <algorithm>
#include "F.h"
#include "Message.h"

#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>
#include <numeric> 
#include <unordered_map>
#include <array>

// usage in .pro
// CvCu[] = interpolate_equidistant_linear[$1]{2};

class NDimensionalTensor {
  private:
      // Helper function to convert multi-dimensional indices to a linear index
      int getLinearIndex(const std::vector<int>& indices) const {
  
          int linearIndex = 0;
          for (size_t i = 0; i < N; ++i) {
              linearIndex += indices[i] * strides[i];
          }
          return linearIndex;
      }
  
  public:
      // Default constructor
      NDimensionalTensor() : dims(), data() {}
  
      std::vector<std::vector<double>> grid;
      std::vector<double> data;
      std::vector<double> weights;
      std::vector<int> dims;
      std::vector<double> index_multiplier;
      std::vector<int> strides;
      int N;
      int totalSize;
  
      std::vector<int> indices;
      std::vector<double> args;
      std::vector<double> precomputed_weights;
      std::vector<int> corner_indices;
  
      // Constructor that takes the grid axes and fills the tensor with the given values
      NDimensionalTensor(const std::vector<std::vector<double>>& gridAxes, const std::vector<double>& values, const std::vector<double>& denominator_weights) {
          // Initialize the data
          data = values;
  
          // Init grid
          grid = gridAxes;
  
          // Init denominator_weights 
          weights = denominator_weights;
          
          N = gridAxes.size();
          dims.resize(N);
          index_multiplier.resize(N);
  
          for (int i = 0; i < N; ++i) {
              dims[i] = gridAxes[i].size();
              index_multiplier[i] = dims[i]/(grid[i].back() - grid[i].front());
          }
  
          // Calculate total number of elements
          totalSize = std::accumulate(dims.begin(), dims.end(), 1, std::multiplies<int>());
          if (values.size() != totalSize) {
              Message::Error("The number of values does not match the total grid size.");
          }
  
          strides.resize(dims.size());
          strides[dims.size() - 1] = 1;  // Last dimension has stride 1
      
          for (int i = dims.size() - 2; i >= 0; --i) {
              strides[i] = strides[i + 1] * dims[i + 1];
          }
  
          indices.resize(N);
          args.resize(N);
          precomputed_weights.resize(N);
          corner_indices.resize(N);
      }
  
  
      // Constant version of the access operator
      // Operator to access the tensor using multi-dimensional indices
      template <typename... Indices>
      const double& at(Indices... indices) const {
          std::vector<int> indexVec = { indices... };
          int linearIndex = getLinearIndex(indexVec);
          return data[linearIndex];
      }
  
      double interpolateValue1D() {
          // This assumes an equidistant grid in this direction
          // Clamp index to valid range
          int index = std::max(0, std::min(static_cast<int>(args[0] * index_multiplier[0]), dims[0] - 2));
  
          // Precompute weights for interpolation
          double result = data[index] + weights[index] * (args[0] - grid[0][index]);
          
          return result;
      }
  
      double interpolateValueND() {
  
          // fill indices and precompute weights
          for (int i = 0; i < N; i++) {
              indices[i] = std::max(0, std::min(static_cast<int>(args[i] * index_multiplier[i]), dims[i] - 2));
              precomputed_weights[i] = (args[i] - grid[i][indices[i]]) * weights[i];
          }
          
          // Iterate over all corners of the hypercube (2^N corners)
          double result = 0.0;
          for (int corner = 0; corner < (1 << N); ++corner) {
              double weight = 1.0;
          
              for (int i = 0; i < N; ++i) {
                  if (corner & (1 << i)) {
                      corner_indices[i] = indices[i] + 1;
                      weight *= precomputed_weights[i];
                  } else {
                      corner_indices[i] = indices[i];
                      weight *= (1.0 - precomputed_weights[i]);
                  }
              }
          
              result += weight * at(corner_indices);
          }
      
          return result;
          
      }
      
      // We could add an interpolation function for non-equidistant grids here
  }; 
   
  
  std::vector<std::vector<double>> readCSV(const std::string &filename) {
      std::vector<std::vector<double>> data;
      std::ifstream file(filename);
      
      if (!file.is_open()) {
      Message::Error((std::string("Error opening file: ") + filename).c_str());
          return data; // Return empty vector if file can't be opened
      }
  
      std::string line;
      while (std::getline(file, line)) {  // Read each line
          std::stringstream ss(line);
          std::string cell;
          std::vector<double> row;
  
          while (std::getline(ss, cell, ',')) {  // Split by comma
              try {
                  row.push_back(std::stod(cell));  // Convert to double
              } catch (const std::exception &e) {
          Message::Error(("Conversion error: " + cell + " -> " + std::string(e.what())).c_str());
              }
          }
  
          data.push_back(row);  // Store row in data
      }
  
      file.close();
      return data;
  }
  
  void writeResult(const std::string &filename, const std::vector<std::vector<double>> &data) {
      std::ofstream file(filename);
      
      if (!file.is_open()) {
          Message::Error((std::string("Error opening file for writing: ") + filename).c_str());
          return;
      }
  
      // Iterate over each row
      for (const auto &row : data) {
          // Write each element in the row, separated by commas
          for (size_t col = 0; col < row.size(); ++col) {
              file << row[col];
              if (col < row.size() - 1) {  // Avoid trailing comma at the end of the row
                  file << ",";
              }
          }
          file << "\n";  // New line at the end of each row
      }
  
      file.close();
  }
  
  std::vector<std::string> splitString(const std::string &input) {
      std::istringstream stream(input);
      std::vector<std::string> tokens;
      std::string token;
      
      while (stream >> token) { // Extract words separated by spaces
          tokens.push_back(token);
      }
      
      return tokens;
  }
  
  std::vector<std::vector<double>> transpose(const std::vector<std::vector<double>> &data) {
      if (data.empty()) {
          return {};  // Return empty if input is empty
      }
  
      size_t numRows = data.size();
      size_t numCols = data[0].size();
  
      std::vector<std::vector<double>> transposed(numCols, std::vector<double>(numRows));
  
      // Fill the transposed vector
      for (size_t i = 0; i < numRows; ++i) {
          for (size_t j = 0; j < numCols; ++j) {
              transposed[j][i] = data[i][j];
          }
      }
  
      return transposed;
  }
  
   // Global unordered map to store tensors and their slope with a string key
   std::vector<NDimensionalTensor> tensorVector;
    
  void F_setup_equidistant_linear_interpolation(F_ARG){
    
    if (!Fct->String)
      Message::Error("Function F_setup_equidistant_linear_interpolation expects a string input!");
    
    std::vector<std::string> strings = splitString(Fct->String);
    
    if (strings.size() != 3)
      Message::Error("Function F_setup_equidistant_linear_interpolation expects a string with 4 tokens seperated by spaces!");
      
    const int material_idx = (int) Fct->Para[0];
    // grid where each column is one axis
    const std::string grid_file = strings[0];
    // function values for each point of the rectilinear grid spanned by the N axis
    // should be of lnegth length(axis 1) x length(axis 2) x ... x length(axis N)
    const std::string values_file = strings[1];
    // weight vector for each point of the rectilinear grid spanned by the N axis
    // should be of length number of axis
    const std::string denominator_weights_file = strings[2];
    
    const std::vector<std::vector<double>> grid = transpose(readCSV(grid_file));
    const std::vector<double> values = transpose(readCSV(values_file))[0];
    const std::vector<double> denominator_weights = transpose(readCSV(denominator_weights_file))[0];
          
    tensorVector.emplace_back(grid, values, denominator_weights);
  }
  
  void F_interpolate_equidistant_linear(F_ARG) {
      const int material_idx = (int) Fct->Para[0];  // Use const reference to avoid unnecessary copying
      NDimensionalTensor& tensor = tensorVector[material_idx];  // Cache tensor reference
  
      for (int i = 0; i < tensor.N; ++i) {
          tensor.args[i] = static_cast<double>((A + i)->Val[0]);  // Avoid C-style cast
      }
  
      if (tensor.N == 1) {
          V->Val[0] = tensor.interpolateValue1D();
          V->Type = SCALAR;
      } else {
          Message::Error("Function F_interpolate_equidistant_linear expects a tensor of dimension 1!");
      }
  }