void F_TSA_constantMaterial_rhs(F_ARG);
void F_TSA_spatialMaterial_directOutput(F_ARG);
void F_TSA_spatialMaterial_constantThickness_rhs(F_ARG);
void F_TSA_constantMaterial_constantThickness_rhs(F_ARG);
void F_TSA_constantMaterial_mass(F_ARG);
void F_TSA_constantMaterial_constantThickness_stiffness(F_ARG);
void F_TSA_constantMaterial_constantThickness_mass(F_ARG);
void F_TSA_spatialMaterial_constantThickness_mass(F_ARG);
void F_TSA_constantMaterial_stiffness(F_ARG);
void F_TSA_spatialMaterial_constantThickness_stiffness(F_ARG);

void F_TSA_constantMaterial_triple(F_ARG);
void F_TSA_constantMaterial_constantThickness_triple(F_ARG);
void F_TSA_spatialMaterial_constantThickness_triple(F_ARG);

void F_TSA_constantMaterial_fct_only(F_ARG);
void F_TSA_spatialMaterial_constantThickness_fct_only(F_ARG);
void F_TSA_constantMaterial_constantThickness_fct_only(F_ARG);

void F_TSA_CFUN_QHCircuit_t_T_rhs(F_ARG);
void F_TSA_CFUN_kCu_T_stiffness(F_ARG);
void F_TSA_CFUN_kCu_T_mass(F_ARG);
void F_TSA_CFUN_rhoCu_T_stiffness(F_ARG);
void F_TSA_CFUN_rhoCu_T_mass(F_ARG);

void F_TSA_CFUN_kCu_T_constantThickness_stiffness(F_ARG);
void F_TSA_CFUN_kCu_T_constantThickness_mass(F_ARG);
void F_TSA_CFUN_rhoCu_T_constantThickness_stiffness(F_ARG);
void F_TSA_CFUN_rhoCu_T_constantThickness_mass(F_ARG);

{% for (getdp_name, num_const_params, num_var_params, rhs, mass, stiffness, triple, fct_only) in zipped_inputs%}
{% if rhs %}
void F_TSA_<<getdp_name>>_rhs(F_ARG);
void F_TSA_<<getdp_name>>_constantThickness_rhs(F_ARG);
{% endif %}
{% if stiffness %}
void F_TSA_<<getdp_name>>_stiffness(F_ARG);
void F_TSA_<<getdp_name>>_constantThickness_stiffness(F_ARG);
{% endif %}
{% if mass %}
void F_TSA_<<getdp_name>>_mass(F_ARG);
void F_TSA_<<getdp_name>>_constantThickness_mass(F_ARG);
{% endif %}
{% if triple %}
void F_TSA_<<getdp_name>>_triple(F_ARG);
void F_TSA_<<getdp_name>>_constantThickness_triple(F_ARG);
{% endif %}
{% if fct_only %}
void F_TSA_<<getdp_name>>_fct_only(F_ARG);
void F_TSA_<<getdp_name>>_constantThickness_fct_only(F_ARG);
{% endif %}
{% endfor %}


