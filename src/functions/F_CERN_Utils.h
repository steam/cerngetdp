void initInput(int nArgs, int blockSize, double **inReal, double **inImag);

void freeInputOutput(int nArgs, double **inReal, double **inImag,
                double* outReal, double* outImag);

static int blockSize = 1;