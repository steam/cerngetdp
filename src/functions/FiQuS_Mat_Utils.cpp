#include <math.h>
#include <algorithm>
#include "F.h"
#include "Message.h"
#include <limits>

// rule of mixtures to compute effective material property 
// mixture:= sum_i ratio_i * material_i
// ratio_i in (0, 1]
void F_RuleOfMixtures(F_ARG) {
    
    int nbrMaterialFcts = Fct->NbrArguments;
    int nbrRatios = Fct->NbrParameters;


    if(nbrMaterialFcts != nbrRatios)
        Message::Error("RuleOfMixtures: Need to provide the same number of material functions and ratios.");

    double material_mixture = 0;

    // mixture: sum_i ratio_i * material_i
    for (int i = 0; i < nbrMaterialFcts; i++) {
        double ratio_i = (double) Fct->Para[i]; 
        double material_i = (double) (A+i)->Val[0];
        material_mixture += ratio_i * material_i;
    }

    V->Val[0] = material_mixture;
    V->Type   = SCALAR;
}


// effective resistivity computation of electrically parallel materials
// effective resistivity:= (sum_i ratio_i/rho_i)^(-1)
// ratio_i in (0, 1]
void F_EffectiveResistivity(F_ARG) {
    
    int nbrMaterialFcts = Fct->NbrArguments;
    int nbrRatios = Fct->NbrParameters;

    if(nbrMaterialFcts != nbrRatios)
        Message::Error("RuleOfMixtures: Need to provide the same number of material functions and ratios.");

    double inv_material_mixture = 0;

    // mixture: sum_i ratio_i * material_i
    for (int i = 0; i < nbrMaterialFcts; i++) {
        double ratio_i = (double) Fct->Para[i]; 
        double material_i = (double) (A+i)->Val[0];
        inv_material_mixture += ratio_i / material_i;
    }

    V->Val[0] = 1.0/inv_material_mixture;
    V->Type   = SCALAR;
}

// derivative of the total effective resistivity with respect to the resistivity of the i-th material
// times that materials derivative with respect to j
void F_EffectiveResistivityDerivative(F_ARG) {
    double rhoEffective = (double) (A)->Val[0];
    double rho_i = (double) (A + 1)->Val[0];
    double rho_iDotx = (double) (A + 2)->Val[0];
    double rho_iDoty = (double) (A + 2)->Val[1];
    double rho_iDotz = (double) (A + 2)->Val[2];

    double ratio_i = (double) Fct->Para[0];

    double derivativex, derivativey, derivativez;
    if (rho_i <= 1e-20){
        derivativex = 0;
        derivativey = 0;
        derivativez = 0;
    } else{
        double common = rhoEffective*rhoEffective*ratio_i/(rho_i*rho_i);
        derivativex = common * rho_iDotx;
        derivativey = common * rho_iDoty;
        derivativez = common * rho_iDotz;
    }

    V->Val[0] = derivativex;
    V->Val[1] = derivativey;
    V->Val[2] = derivativez;
    V->Type   = VECTOR;
}


// power law
// rho = Ecrit/Jcrit * (lambda/fraction_sc * J/Jc)^(n - 1)
// lambda in [0, 1] is the fraction of current density in the superconductor 
void F_rho_PowerLaw_CS(F_ARG) {
    // current density norm in the HTS layer
    double Jnorm = (double) (A)->Val[0];
    double Jcrit = (double) (A + 1)->Val[0];
    double lambda = (double) (A + 2)->Val[0];

    double Ecrit = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];
    double f_SC = (double) Fct->Para[2];

    double rho;
    if (Jcrit > 0) {
        rho = Ecrit/Jcrit * pow(lambda/f_SC * Jnorm/Jcrit, n - 1);
    } else {
        rho = std::numeric_limits<double>::max();
    }
    
    V->Val[0] = rho;
    V->Type   = SCALAR;
}

// Find lambda for NI coils. The input j is in winding coordinates:
// J_1 = J_r: J_hom_cc_r
// J_2 = J_theta: J_hom_cc_phi
// J_3 = J_z: J_hom_cc_z
void F_CurrentSharing_anisotropic(F_ARG){
    double J_hom_cc_r = (double) (A)->Val[0];
    double J_hom_cc_phi = (double) (A)->Val[1];
    double J_hom_cc_z = (double) (A)->Val[2];
    double Jc = (double) (A + 1)->Val[0];
    double rhoNc = (double) (A + 2)->Val[0];

    double Ec = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];
    double hSc = (double) Fct->Para[2];
    double hNc = (double) Fct->Para[3];
    double scalingTSA = (double) Fct->Para[4];
    double NO_OF_MAX_ITER = (double) Fct->Para[5];
    double ABS_TOL = (double) Fct->Para[6];

    // Consistency check: arguments
	// if(Jc    <  0){error = "Jc    is <  0!"; return 0; }
	// if(Ec    <= 0){error = "Ec    is <= 0!"; return 0; }
	// if(n     <  0){error = "n     is <  0!"; return 0; }
	// if(hSc   <= 0){error = "hSc   is <= 0!"; return 0; }
	// if(hNc   <= 0){error = "hNc   is <= 0!"; return 0; }
	// if(rhoNc <= 0){error = "rhoNc is <= 0!"; return 0; }

    // Global index
    int i; 
    int iter_i; 

    // Binary search algorithm
    double alpha;
    double lambda;
    double lambda_MIN; 
    double lambda_MAX;
    double fun;
    double err; 

	// Quench
	if (Jc==0){
		lambda = 0;
    }		
	// Current Sharing	
	else {
		// Binary search algorithm 
		lambda_MIN = 0; 	
		lambda_MAX = 1;	
        double fSC = hSc/(hNc + hSc);
		alpha      = (hNc/rhoNc)*((1.0/hSc)*(Ec/Jc));
	
		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
	
			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*lambda*pow(sqrt(pow(J_hom_cc_r, 2) + pow(1.0/scalingTSA * J_hom_cc_phi * lambda/fSC, 2) + pow(1.0/scalingTSA * J_hom_cc_z * lambda/fSC, 2))/Jc, n - 1)+lambda-1;

			err    = lambda_MAX-lambda_MIN;
	
	
			if (err>ABS_TOL){ // Update boundaries
				if (fun>=0){ // too much current in HTS
					lambda_MAX = lambda;
					}				
				else if (fun<0){ // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;	
            }			
        }
	
	    // Consistency check: number of iterations	
		if (iter_i==NO_OF_MAX_ITER){
            Message::Debug("F_CurrentSharing_anisotropic: Maximum number of bisection iterations reached");
			}   
    }
	
	//Consistency check: output
	if(lambda!=lambda){
		Message::Debug("F_CurrentSharing_anisotropic: Output is nan"); 
    }
	
	if (fabs(lambda)>std::numeric_limits<double>::max()){
        Message::Debug("F_CurrentSharing_anisotropic: Output is inf");
    }

    V->Val[0] = lambda;
    V->Type   = SCALAR;
}

void F_CurrentSharingDerivative_anisotropic(F_ARG){
    double J_hom_cc_r = (double) (A)->Val[0];
    double J_hom_cc_phi = (double) (A)->Val[1];
    double J_hom_cc_z = (double) (A)->Val[2];
    double Jc = (double) (A + 1)->Val[0];
    double rhoNc = (double) (A + 2)->Val[0];
    double lambda = (double) (A + 3)->Val[0];

    double Ec = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];
    double hSc = (double) Fct->Para[2];
    double hNc = (double) Fct->Para[3];
    double scalingTSA = (double) Fct->Para[4];

    // Consistency check: arguments
	// if(Jc    <  0){error = "Jc    is <  0!"; return 0; }
	// if(Ec    <= 0){error = "Ec    is <= 0!"; return 0; }
	// if(n     <  0){error = "n     is <  0!"; return 0; }
	// if(hSc   <= 0){error = "hSc   is <= 0!"; return 0; }
	// if(hNc   <= 0){error = "hNc   is <= 0!"; return 0; }
	// if(rhoNc <= 0){error = "rhoNc is <= 0!"; return 0; }

    double lambdaDot1, lambdaDot2, lambdaDot3;
    double fSC;
    double jNorm;
    double alpha;
    double a;
    double a_to_nminus1;
    double a_to_nminus2;
    double b;
    double c;
    double e;
    double k;
	// Quench
	if (Jc==0){
		lambdaDot1 = 0;
        lambdaDot2 = 0;
        lambdaDot3 = 0;
    }		
	// Current Sharing	
	else {
        fSC = hSc/(hNc + hSc);
        jNorm = sqrt(pow(J_hom_cc_r, 2) + pow(1.0/scalingTSA * J_hom_cc_phi * lambda/fSC, 2) + pow(1.0/scalingTSA * J_hom_cc_z * lambda/fSC, 2));
        if (fabs(jNorm)<1e-10)
        {
            lambdaDot1 = 0;
            lambdaDot2 = 0;
            lambdaDot3 = 0;
        } else{
            alpha = (hNc/rhoNc)*((1.0/hSc)*(Ec/Jc));
            a = jNorm/Jc;
            a_to_nminus1 = pow(a, n - 1);
            a_to_nminus2 = pow(a, n - 2);
            if(fabs(a_to_nminus1)>std::numeric_limits<double>::max()){
                lambdaDot1 = 0;
                lambdaDot2 = 0;
                lambdaDot3 = 0;
            }else{
                b = lambda/(scalingTSA*scalingTSA*fSC*fSC);
                c = J_hom_cc_phi*J_hom_cc_phi + J_hom_cc_z*J_hom_cc_z;
                e = 1/(jNorm*Jc);
                k = -alpha*lambda*a_to_nminus2*(n-1)*e/(alpha*lambda*a_to_nminus2*(n-1)*e*b*c+alpha*a_to_nminus1+1);

                lambdaDot1 = k*J_hom_cc_r;
                lambdaDot2 = k*b*lambda*J_hom_cc_phi;
                lambdaDot3 = k*b*lambda*J_hom_cc_z;
            }
        }
    }
	
	//Consistency check: output
	if(lambdaDot1!=lambdaDot1 || lambdaDot2!=lambdaDot2 || lambdaDot3!=lambdaDot3){
        Message::Debug("alpha: %g", alpha);
        Message::Debug("a: %g", a);
        Message::Debug("a_to_nminus1: %g", a_to_nminus1);
        Message::Debug("a_to_nminus2: %g", a_to_nminus2);
        Message::Debug("b: %g", b);
        Message::Debug("c: %g", c);
        Message::Debug("e: %g", e);
        Message::Debug("k: %g", k);
		Message::Debug("F_CurrentSharingDerivative_anisotropic: Output is nan"); 
    }
	
	if (fabs(lambdaDot1)>std::numeric_limits<double>::max() || 
        fabs(lambdaDot2)>std::numeric_limits<double>::max() ||
        fabs(lambdaDot3)>std::numeric_limits<double>::max()){
        Message::Debug("F_CurrentSharingDerivative_anisotropic: Output is inf");
    }

    V->Val[0] = lambdaDot1;
    V->Val[1] = lambdaDot2;
    V->Val[2] = lambdaDot3;
    V->Type   = VECTOR;
}

// power law
// rho = Ecrit/Jc * (J/Jc)^(n - 1)
void F_rho_PowerLaw(F_ARG) {
    // current density norm in the HTS layer
    double Jnorm = (double) (A)->Val[0];
    double Jcrit = (double) (A + 1)->Val[0];

    double Ecrit = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];

    double rho;
    if (Jcrit > 0) {
        rho = Ecrit/Jcrit * pow(Jnorm/Jcrit, n - 1);
    } else {
        rho = std::numeric_limits<double>::max();
    }
    
    V->Val[0] = rho;
    V->Type   = SCALAR;
}

// derivative of the power law with respect to the current density
// because current density is a vector, the derivative will be a vector
// d(rho)/d(j) = Ecrit/Jc * (n-1) * (sqrt(jx^2+jy^2+jz^2)/Jc)^(n - 2) * (1/Jc) * (1/2) * (1/sqrt(jx^2+jy^2+jz^2)) * <2jx, 2jy, 2jz>
void F_drhodj_PowerLaw(F_ARG) {
    // current density norm in the HTS layer
    double Jx = (double) (A)->Val[0];
    double Jy = (double) (A)->Val[1];
    double Jz = (double) (A)->Val[2];
    
    double Jcrit = (double) (A + 1)->Val[0];

    double Ecrit = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];

    double rhoDotx, rhoDoty, rhoDotz;
    if (Jcrit > 0) {
        double jNorm = sqrt(Jx*Jx+Jy*Jy+Jz*Jz);
        if (jNorm>1e-10) {
            double rhoDotCommon = Ecrit/Jcrit * (n-1) * pow(jNorm/Jcrit, n - 2) * (1.0/Jcrit) * (1.0/2.0) * (1.0/jNorm);
            rhoDotx = rhoDotCommon * 2.0 * Jx;
            rhoDoty = rhoDotCommon * 2.0 * Jy;
            rhoDotz = rhoDotCommon * 2.0 * Jz;
        } else{
            rhoDotx = 0;
            rhoDoty = 0;
            rhoDotz = 0;
        }
    } else {
        rhoDotx = std::numeric_limits<double>::max();
        rhoDoty = std::numeric_limits<double>::max();
        rhoDotz = std::numeric_limits<double>::max();
    }
    V->Val[0] = rhoDotx;
    V->Val[1] = rhoDoty;
    V->Val[2] = rhoDotz;
    V->Type   = VECTOR;
}

// derivative tensor of aniostropic rho with respect to the current density for Pancake3D
void F_Pancake3DDerivativeTensorWRTjINWindingCoordsTimesJ(F_ARG){
    double rhoDerivativeNormalToWinding1 = (double) (A)->Val[0];
    double rhoDerivativeNormalToWinding2 = (double) (A)->Val[1];
    double rhoDerivativeNormalToWinding3 = (double) (A)->Val[2];

    double rhoDerivativeParallelToWinding1 = (double) (A+1)->Val[0];
    double rhoDerivativeParallelToWinding2 = (double) (A+1)->Val[1];
    double rhoDerivativeParallelToWinding3 = (double) (A+1)->Val[2];

    double rhoParallelToWinding = (double) (A+2)->Val[0];

    double rhoNormalToWinding = (double) (A+3)->Val[0];

    double j1 = (double) (A+4)->Val[0];
    double j2 = (double) (A+4)->Val[1];
    double j3 = (double) (A+4)->Val[2];

    double windingMinimumPossibleRho = (double)  Fct->Para[0];
    double windingMaximumPossibleRho = (double) Fct->Para[1];

    if(fabs(windingMaximumPossibleRho-rhoNormalToWinding)<1e-15){
        // Maximum possible rho is achieved, so the derivative's first row is zero
        V->Val[0] = 0;
        V->Val[1] = 0;
        V->Val[2] = 0;
    }
    else{
        // Maximum possible rho is not achieved, so the derivative's first row is not zero
        V->Val[0] = rhoDerivativeNormalToWinding1*j1;
        V->Val[1] = rhoDerivativeNormalToWinding2*j1;
        V->Val[2] = rhoDerivativeNormalToWinding3*j1;
    }
    if(fabs(windingMinimumPossibleRho-rhoParallelToWinding)<1e-15){
        // Minimum possible rho is achieved, so the derivative's second and third rows are zero
        V->Val[3] = 0;
        V->Val[4] = 0;
        V->Val[5] = 0;

        V->Val[6] = 0;
        V->Val[7] = 0;
        V->Val[8] = 0;
    }
    else{
        // Minimum possible rho is not achieved, so the derivative's second and third rows are not zero
        V->Val[3] = rhoDerivativeParallelToWinding1*j2;
        V->Val[4] = rhoDerivativeParallelToWinding2*j2;
        V->Val[5] = rhoDerivativeParallelToWinding3*j2;

        V->Val[6] = rhoDerivativeParallelToWinding1*j3;
        V->Val[7] = rhoDerivativeParallelToWinding2*j3;
        V->Val[8] = rhoDerivativeParallelToWinding3*j3;
    }
    V->Type   = TENSOR;
}

// derivative of effective resistivity in current sharing setting:
// parallel NC and SC with SC given a power law

// drho/dj = f_SC * drho_SC/dj * 1 / (rhoSC^2 * [f_SC/rho_SC + f_NC/rho_NC]^2)
//         = f_SC * drho_SC/dj * 1 / (rhoSC^2) * rhoEff(NC, SC)^2
void F_drhodj_CS(F_ARG) {
    // current density norm in the HTS layer
    double Jnorm = (double) (A)->Val[0]; 
    double rhoSC = (double) (A + 1)->Val[0];
    double rhoEff_CS = (double) (A + 2)->Val[0];

    // Jvec = (A + 3) is the current density vector(!)

    double f_SC = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];
    double min_rhoSC = (double) Fct->Para[2];
    double max_rhoSC = (double) Fct->Para[3];

    if (rhoSC < min_rhoSC || rhoSC > max_rhoSC) {
        V->Val[0] = 0;
        V->Type = SCALAR;
    } 
    else {
        // no division by f_SC here since d/dx norm(x) = x/norm(x)
        // the norm thus is eliminated in the inner derivative
        double drho_dj_withoutJ = (n - 1.0) * rhoSC * pow(Jnorm, -2.0);
        double deff_dj_withoutJ = f_SC * drho_dj_withoutJ * pow(rhoSC, -2.0) * pow(rhoEff_CS, 2.0);

        Function Fct_arg; 
        Value V_arg;
        F_SquDyadicProduct(&Fct_arg, (A + 3), &V_arg);

        for (int i = 0; i < 6; i++)
            V->Val[i] = V_arg.Val[i] * deff_dj_withoutJ;

        V -> Type = TENSOR_SYM;
    } 
}

void F_dedj_CS(F_ARG) {
    // current density norm in the HTS layer
    double Jnorm = (double) (A)->Val[0]; 
    double rhoSC = (double) (A + 1)->Val[0];
    double rhoEff_CS = (double) (A + 2)->Val[0];

    // Jvec = (A + 3) is the current density vector(!)

    double f_SC = (double) Fct->Para[0];
    double n = (double) Fct->Para[1];
    double min_rhoSC = (double) Fct->Para[2];
    double max_rhoSC = (double) Fct->Para[3];
    double winding_nonZero = (double) Fct->Para[4];

    if (rhoSC < min_rhoSC || rhoSC > max_rhoSC) {
        V->Val[0] = 0;
        V->Type = SCALAR;
    } 
    else {
        // no division by f_SC here since d/dx norm(x) = x/norm(x)
        // the norm thus is eliminated in the inner derivative
        double drho_dj_withoutJ = (n - 1.0) * rhoSC * pow(Jnorm, -2.0);
        double deff_dj_withoutJ = f_SC * drho_dj_withoutJ * pow(rhoSC, -2.0) * pow(rhoEff_CS, 2.0);

        Function Fct_arg; 
        Value V_arg;
        F_SquDyadicProduct(&Fct_arg, (A + 3), &V_arg);

        for (int i = 0; i < 6; i++) {
            V->Val[i] = V_arg.Val[i] * deff_dj_withoutJ;

            // additional contribution for dedj
            if(i == 0 || i == 3 || i == 5)
                V->Val[i] += rhoEff_CS + winding_nonZero;
        }

        V -> Type = TENSOR_SYM;
    } 
}            
