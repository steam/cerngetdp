#include "F.h"
#include "Message.h"
#include "Gauss.h"
#include <math.h>
// not ideal; should include a header file instead
#include "FiQuS_Pancake3D_2.h"

namespace TSANameSpace {
  // Declare global map within namespace
    static std::unordered_map<std::string, std::unordered_map<Vec3, double, Vec3Hash>> map_container;
}   

// linear material functions
void F_TSA_constantMaterial_rhs(F_ARG) {
  double material_i = (double) Fct->Para[0];

  double thickness_layer_i = (double) A->Val[0];

  V->Val[0] = 0.5 * thickness_layer_i * material_i ;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_constantThickness_rhs(F_ARG) {
  double thickness_layer_i = (double) Fct->Para[0];
  double material_i = (double) Fct->Para[1];

  V->Val[0] = 0.5 * thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_directOutput(F_ARG) {
	double x = (double) A->Val[0];
	double y = (double) A->Val[1];
	double z = (double) A->Val[2];
	Vec3 point = Vec3(x, y, z);

	std::string map_name = std::to_string(Fct->Para[0]);
  	double material_i;

	if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
		Message::Debug("found point in material map");
		material_i = TSANameSpace::map_container[map_name][point];
	} else {
		Message::Debug("didn't find point in map");
		
		int material_in_arc_length = (int)Fct->Para[1];
		int NoOfMaterialValues = (int)Fct->Para[2];
		
		static std::vector<double> lengthValues(NoOfMaterialValues);
		static std::vector<double> materialValues(NoOfMaterialValues);
		  
		int index = 3;
		for (int i = 0; i < NoOfMaterialValues; i++) {
			lengthValues[0] = (double)Fct->Para[index];
			index++;
		}
		for (int i = 0; i < NoOfMaterialValues; i++) {
			materialValues[i] = (double)Fct->Para[index];
			index++;
		}
	  
		Winding& winding = Winding::get_winding_object();
		double length;
		
		if (material_in_arc_length == 1)
			length = winding.getArcLength(point);
		else
			length = winding.getTurnNumber(point);
		
		material_i = linear_interpolation(lengthValues, materialValues, length);
		TSANameSpace::map_container[map_name][point] = material_i;
    }

	V->Val[0] = material_i;
	V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_constantThickness_rhs(F_ARG) {
  double x = (double) A->Val[0];
  double y = (double) A->Val[1];
  double z = (double) A->Val[2];
  Vec3 point = Vec3(x, y, z);
  double material_i;
  
  std::string map_name = std::to_string(Fct->Para[0]);
  double thickness_layer_i = (double) Fct->Para[1];

  if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
      material_i = TSANameSpace::map_container[map_name][point];
    } else {	

		int material_in_arc_length = (int)Fct->Para[2];
		
		int NoOfMaterialValues = (int)Fct->Para[3];
		static std::vector<double> lengthValues(NoOfMaterialValues);
		static std::vector<double> materialValues(NoOfMaterialValues);
		  
		int index = 4;
		for (int i = 0; i < NoOfMaterialValues; i++) {
			lengthValues[i] = (double)Fct->Para[index];
			index++;
		}
		for (int i = 0; i < NoOfMaterialValues; i++) {
			materialValues[i] = (double)Fct->Para[index];
			index++;
		}
		  
		Winding& winding = Winding::get_winding_object();
		double length;
	
	    if (material_in_arc_length == 1)
			length = winding.getArcLength(point);
		else
			length = winding.getTurnNumber(point);
		
		material_i = linear_interpolation(lengthValues, materialValues, length);
		TSANameSpace::map_container[map_name][point] = material_i;
    }

    V->Val[0] = 0.5 * thickness_layer_i * material_i;
    V->Type   = SCALAR;
}

void F_TSA_constantMaterial_mass(F_ARG) {
  double material_i = (double) Fct->Para[0];
  int k = (int) Fct->Para[1];
  int l = (int) Fct->Para[2];

  double thickness_layer_i = (double) A->Val[0];

  V->Val[0] = ((k == l)? 2.0: 1.0) * material_i * thickness_layer_i/6.0;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_stiffness(F_ARG) {
  double material_i = (double) Fct->Para[0];
  int k = (int) Fct->Para[1];
  int l = (int) Fct->Para[2];

  double thickness_layer_i = (double) A->Val[0];

  V->Val[0] = ((k == l)? 1.0: -1.0) *  material_i/thickness_layer_i;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_constantThickness_mass(F_ARG) {
  double thickness_layer_i = (double) Fct->Para[0];
  double material_i = (double) Fct->Para[1];
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];

  V->Val[0] = ((k == l)? 2.0: 1.0) * material_i * thickness_layer_i/6.0;
  V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_constantThickness_mass(F_ARG) {
	double x = (double) A->Val[0];
	double y = (double) A->Val[1];
	double z = (double) A->Val[2];
  
  	Vec3 point = Vec3(x, y, z);
  	double material_i;
	
	std::string map_name = std::to_string(Fct->Para[0]);
	double thickness_layer_i = (double) Fct->Para[1];
	int k = (int) Fct->Para[2];
	int l = (int) Fct->Para[3];

	if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
		Message::Debug("found point in material map");
		material_i = TSANameSpace::map_container[map_name][point];
	} else {
		Message::Debug("didn't find point in map");
		
		int material_in_arc_length = (int)Fct->Para[4];
		int NoOfMaterialValues = (int)Fct->Para[5];

		static std::vector<double> lengthValues(NoOfMaterialValues);
		static std::vector<double> materialValues(NoOfMaterialValues);
		  
		int index = 6;
		for (int i = 0; i < NoOfMaterialValues; i++) {
			lengthValues[i] = (double)Fct->Para[index];
			index++;
		}
		for (int i = 0; i < NoOfMaterialValues; i++) {
			materialValues[i] = (double)Fct->Para[index];
			index++;
		}

		Winding& winding = Winding::get_winding_object();
		double length;
		
		if (material_in_arc_length == 1)
			length = winding.getArcLength(point);
		else
			length = winding.getTurnNumber(point);
		
		material_i = linear_interpolation(lengthValues, materialValues, length);
		TSANameSpace::map_container[map_name][point] = material_i;
    }
  
	V->Val[0] = ((k == l)? 2.0: 1.0) * material_i * thickness_layer_i/6.0;
	V->Type   = SCALAR;
}

void F_TSA_constantMaterial_constantThickness_stiffness(F_ARG) {
  double thickness_layer_i = (double) Fct->Para[0];
  double material_i = (double) Fct->Para[1];
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];

  V->Val[0] = ((k == l)? 1.0: -1.0) *  material_i/thickness_layer_i;
  V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_constantThickness_stiffness(F_ARG) {
  double x = (double) A->Val[0];
  double y = (double) A->Val[1];
  double z = (double) A->Val[2];
  Vec3 point = Vec3(x, y, z);

  std::string map_name = std::to_string(Fct->Para[0]);
  double thickness_layer_i = (double) Fct->Para[1];
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  
  double material_i;
	
  if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
	Message::Debug("found point in material map");
	material_i = TSANameSpace::map_container[map_name][point];
  } else {
	Message::Debug("didn't find point in map");
	  
    int material_in_arc_length = (int)Fct->Para[4];
    int NoOfMaterialValues = (int)Fct->Para[5];
	
	static std::vector<double> lengthValues(NoOfMaterialValues);
	static std::vector<double> materialValues(NoOfMaterialValues);
	  
	int index = 6;
	for (int i = 0; i < NoOfMaterialValues; i++) {
		lengthValues[i] = (double)Fct->Para[index];
		index++;
	}
	for (int i = 0; i < NoOfMaterialValues; i++) {
		materialValues[i] = (double)Fct->Para[index];
		index++;
	}

	Winding& winding = Winding::get_winding_object();
	double material_i;
	double length;
	
	if (material_in_arc_length == 1)
		length = winding.getArcLength(point);
	else
		length = winding.getTurnNumber(point);
	
	material_i = linear_interpolation(lengthValues, materialValues, length);
	TSANameSpace::map_container[map_name][point] = material_i;
    }

  V->Val[0] = ((k == l)? 1.0: -1.0) *  material_i/thickness_layer_i;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_triple(F_ARG) {
  double material_i = (double) Fct->Para[0];
  int k = (int) Fct->Para[1];
  int l = (int) Fct->Para[2];
  int m = (int) Fct->Para[3];

  double thickness_layer_i = (double) A->Val[0];

  double multiplier; 
  if (k == l && l == m)
    multiplier = 1.0/4.0;
  else
    multiplier = 1.0/12.0;
  V->Val[0] = multiplier * thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_constantThickness_triple(F_ARG) {
  double thickness_layer_i = (double) Fct->Para[0];
  double material_i = (double) Fct->Para[1];
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int m = (int) Fct->Para[4];

  double multiplier; 
  if (k == l && l == m)
    multiplier = 1.0/4.0;
  else
    multiplier = 1.0/12.0;
  V->Val[0] = multiplier * thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_constantThickness_triple(F_ARG) {
  double x = (double) A->Val[0];
  double y = (double) A->Val[1];
  double z = (double) A->Val[2];
  Vec3 point = Vec3(x, y, z);

  std::string map_name = std::to_string(Fct->Para[0]);
  double thickness_layer_i = (double) Fct->Para[1];
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int m = (int) Fct->Para[4];

  double material_i;
	
  if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
	Message::Debug("found point in material map");
	material_i = TSANameSpace::map_container[map_name][point];
  } else {
	Message::Debug("didn't find point in map");
	  
    int material_in_arc_length = (int)Fct->Para[5];
    int NoOfMaterialValues = (int)Fct->Para[6];
	
	static std::vector<double> lengthValues(NoOfMaterialValues);
	static std::vector<double> materialValues(NoOfMaterialValues);
	  
	int index = 7;
	for (int i = 0; i < NoOfMaterialValues; i++) {
		lengthValues[i] = (double)Fct->Para[index];
		index++;
	}
	for (int i = 0; i < NoOfMaterialValues; i++) {
		materialValues[i] = (double)Fct->Para[index];
		index++;
	}

	Winding& winding = Winding::get_winding_object();
	double material_i;
	double length;
	
	if (material_in_arc_length == 1)
		length = winding.getArcLength(point);
	else
		length = winding.getTurnNumber(point);
	
	material_i = linear_interpolation(lengthValues, materialValues, length);
	TSANameSpace::map_container[map_name][point] = material_i;
    }
 
  double multiplier; 
  if (k == l && l == m)
    multiplier = 1.0/4.0;
  else
    multiplier = 1.0/12.0;
  V->Val[0] = multiplier * thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_fct_only(F_ARG) {
  double material_i = (double) Fct->Para[0];
  double thickness_layer_i = (double) A->Val[0];

  V->Val[0] = thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_constantMaterial_constantThickness_fct_only(F_ARG) {
  double thickness_layer_i = (double) Fct->Para[0];
  double material_i = (double) Fct->Para[1];

  V->Val[0] = thickness_layer_i * material_i;
  V->Type   = SCALAR;
}

void F_TSA_spatialMaterial_constantThickness_fct_only(F_ARG) {
  
	double x = (double) A->Val[0];
	double y = (double) A->Val[1];
	double z = (double) A->Val[2];
	Vec3 point = Vec3(x, y, z);

	std::string map_name = std::to_string(Fct->Para[0]);
	double thickness_layer_i = (double) Fct->Para[1];

	double material_i;

	if (TSANameSpace::map_container[map_name].find(point) != TSANameSpace::map_container[map_name].end()) {
		Message::Debug("found point in material map");
		material_i = TSANameSpace::map_container[map_name][point];
	} else {
		Message::Debug("didn't find point in map");
		
		int material_in_arc_length = (int)Fct->Para[2];
		int NoOfMaterialValues = (int)Fct->Para[3];
	
		static std::vector<double> lengthValues(NoOfMaterialValues);
		static std::vector<double> materialValues(NoOfMaterialValues);
	  
		int index = 4;
		for (int i = 0; i < NoOfMaterialValues; i++) {
			lengthValues[i] = (double)Fct->Para[index];
			index++;
		}
		for (int i = 0; i < NoOfMaterialValues; i++) {
			materialValues[i] = (double)Fct->Para[index];
			index++;
		}
	  
		Winding& winding = Winding::get_winding_object();
		double length;
		
		if (material_in_arc_length == 1)
			length = winding.getArcLength(point);
		else
			length = winding.getTurnNumber(point);
		
		material_i = linear_interpolation(lengthValues, materialValues, length);
		TSANameSpace::map_container[map_name][point] = material_i;
    }
  
	V->Val[0] = thickness_layer_i * material_i;
	V->Type   = SCALAR;
}

// non-linear function 
void F_TSA_CFUN_QHCircuit_t_T_rhs(F_ARG) {
  {% for idx in range(8) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, nbr_points always last arguments
  int k = (int) Fct->Para[8];
  int nbr_points = (int) Fct->Para[9];

  {% for idx in range(3) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+3)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_rhoSS;
  static Value A_rhoSS[1];
  static Value V_rhoSS;

  static Function Fct_arg;
  static double Fct_params[8];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[2];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
	  double temp = (var_param_2 - var_param_1)/2.0 * (u + 1.0) + var_param_1;

    A_rhoSS[0].Type = SCALAR; 
    A_rhoSS[0].Val[0] = temp;

    F_CFUN_rhoSS_T(&Fct_rhoSS, &A_rhoSS[0], &V_rhoSS);

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = var_param_0;
	
	  A_arg[1].Type = SCALAR; 
    A_arg[1].Val[0] = V_rhoSS.Val[0];

    {% for idx in range(8) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_CFUN_QHCircuit_t_rhoSS(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1)
      int_basis = 1.0 - (u + 1.0)/2.0;
    else
      int_basis = (u + 1.0)/2.0;

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_TSA_CFUN_kCu_T_stiffness(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];
  double delta = (double)(A+2)->Val[0];

  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[1];
  Fct_params[0] = RRR;
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[3];
  static Value V_arg;

  static Function Fct_rhoCu0;
  static Value A_rhoCu0[1];
  static Value V_rhoCu0;
  static double Fct_params_rhoCu0[2];
  Fct_params_rhoCu0[0] = 0;
  Fct_params_rhoCu0[1] = RRR;
  Fct_rhoCu0.Para = &Fct_params_rhoCu0[0];

  static Function Fct_rhoCuB;
  static Value A_rhoCuB[1];
  static Value V_rhoCuB;
  static double Fct_params_rhoCuB[2];
  Fct_params_rhoCuB[0] = B;
  Fct_params_rhoCuB[1] = RRR;
  Fct_rhoCuB.Para = &Fct_params_rhoCuB[0];

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_rhoCu0[0].Type = SCALAR; 
    A_rhoCu0[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCu0, &A_rhoCu0[0], &V_rhoCu0);

    A_rhoCuB[0].Type = SCALAR; 
    A_rhoCuB[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCuB, &A_rhoCuB[0], &V_rhoCuB);

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    A_arg[1].Type = SCALAR; 
    A_arg[1].Val[0] = V_rhoCu0.Val[0];

    A_arg[2].Type = SCALAR; 
    A_arg[2].Val[0] = V_rhoCuB.Val[0];

    F_CFUN_kCu_T_rho0_rho(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}

void F_TSA_CFUN_rhoCu_T_stiffness(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];
  double delta = (double)(A+2)->Val[0];

  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[2];
  Fct_params[0] = B;
  Fct_params[1] = RRR;

  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[1];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}

void F_TSA_CFUN_kCu_T_mass(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];
  double delta = (double)(A+2)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg;
  static double Fct_params[1] ;
  Fct_params[0] = {RRR};
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[3];
  static Value V_arg;

  static Function Fct_rhoCu0;
  static Value A_rhoCu0[1];
  static Value V_rhoCu0;
  static double Fct_params_rhoCu0[2];
  Fct_params_rhoCu0[0] = 0;
  Fct_params_rhoCu0[1] = RRR;
  Fct_rhoCu0.Para = &Fct_params_rhoCu0[0];

  static Function Fct_rhoCuB;
  static Value A_rhoCuB[1];
  static Value V_rhoCuB;
  static double Fct_params_rhoCuB[2];
  Fct_params_rhoCuB[0] = B;
  Fct_params_rhoCuB[1] = RRR;
  Fct_rhoCuB.Para = &Fct_params_rhoCuB[0];

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_rhoCu0[0].Type = SCALAR; 
    A_rhoCu0[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCu0, &A_rhoCu0[0], &V_rhoCu0);

    A_rhoCuB[0].Type = SCALAR; 
    A_rhoCuB[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCuB, &A_rhoCuB[0], &V_rhoCuB);

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    A_arg[1].Type = SCALAR; 
    A_arg[1].Val[0] = V_rhoCu0.Val[0];

    A_arg[2].Type = SCALAR; 
    A_arg[2].Val[0] = V_rhoCuB.Val[0];

    F_CFUN_kCu_T_rho0_rho(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_TSA_CFUN_rhoCu_T_mass(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];
  double delta = (double)(A+2)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[2];
  Fct_params[0] = B;
  Fct_params[1] = RRR;
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[1];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_TSA_CFUN_kCu_T_constantThickness_stiffness(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];

  double delta = (double) Fct->Para[5];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];
  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[1];
  Fct_params[0] = RRR;
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[3];
  static Value V_arg;

  static Function Fct_rhoCu0;
  static Value A_rhoCu0[1];
  static Value V_rhoCu0;

  static double Fct_params_rhoCu0[2];
  Fct_params_rhoCu0[0] = 0;
  Fct_params_rhoCu0[1] = RRR;
  Fct_rhoCu0.Para = &Fct_params_rhoCu0[0];

  static Function Fct_rhoCuB;
  static Value A_rhoCuB[1];
  static Value V_rhoCuB;
  static double Fct_params_rhoCuB[2];
  Fct_params_rhoCuB[0] = B;
  Fct_params_rhoCuB[1] = RRR;
  Fct_rhoCuB.Para = &Fct_params_rhoCuB[0];

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_rhoCu0[0].Type = SCALAR; 
    A_rhoCu0[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCu0, &A_rhoCu0[0], &V_rhoCu0);

    A_rhoCuB[0].Type = SCALAR; 
    A_rhoCuB[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCuB, &A_rhoCuB[0], &V_rhoCuB);

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    A_arg[1].Type = SCALAR; 
    A_arg[1].Val[0] = V_rhoCu0.Val[0];

    A_arg[2].Type = SCALAR; 
    A_arg[2].Val[0] = V_rhoCuB.Val[0];

    F_CFUN_kCu_T_rho0_rho(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}

void F_TSA_CFUN_rhoCu_T_constantThickness_stiffness(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];
  double delta = (double) Fct->Para[5];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];

  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[2];
  Fct_params[0] = B;
  Fct_params[1] = RRR;

  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[1];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}

void F_TSA_CFUN_kCu_T_constantThickness_mass(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];
  double delta = (double) Fct->Para[5];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[1];
  Fct_params[0] = RRR;
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[3];
  static Value V_arg;

  static Function Fct_rhoCu0;
  static Value A_rhoCu0[1];
  static Value V_rhoCu0;
  static double Fct_params_rhoCu0[2];
  Fct_params_rhoCu0[0] = 0;
  Fct_params_rhoCu0[1] = RRR;
  Fct_rhoCu0.Para = &Fct_params_rhoCu0[0];

  static Function Fct_rhoCuB;
  static Value A_rhoCuB[1];
  static Value V_rhoCuB;
  static double Fct_params_rhoCuB[2];
  Fct_params_rhoCuB[0] = B;
  Fct_params_rhoCuB[1] = RRR;
  Fct_rhoCuB.Para = &Fct_params_rhoCuB[0];

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_rhoCu0[0].Type = SCALAR; 
    A_rhoCu0[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCu0, &A_rhoCu0[0], &V_rhoCu0);

    A_rhoCuB[0].Type = SCALAR; 
    A_rhoCuB[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_rhoCuB, &A_rhoCuB[0], &V_rhoCuB);

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    A_arg[1].Type = SCALAR; 
    A_arg[1].Val[0] = V_rhoCu0.Val[0];

    A_arg[2].Type = SCALAR; 
    A_arg[2].Val[0] = V_rhoCuB.Val[0];

    F_CFUN_kCu_T_rho0_rho(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_TSA_CFUN_rhoCu_T_constantThickness_mass(F_ARG) {

  double B = (double) Fct->Para[0];
  double RRR = (double) Fct->Para[1];

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[2];
  int l = (int) Fct->Para[3];
  int nbr_points = (int) Fct->Para[4];
  double delta = (double) Fct->Para[5];

  double T_i = (double) (A+0)->Val[0];
  double T_iPlusOne = (double) (A+1)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[2];
  Fct_params[0] = B;
  Fct_params[1] = RRR;
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[1];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double temp = (T_iPlusOne - T_i)/2.0 * (u + 1.0) + T_i;

    A_arg[0].Type = SCALAR; 
    A_arg[0].Val[0] = temp;

    F_CFUN_rhoCu_T(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

// automatically defined functions
{% for (getdp_name, num_const_params, num_var_params, rhs, mass, stiffness, triple, fct_only, function_name) in zipped_inputs%}
{% if rhs %}
void F_TSA_<<getdp_name>>_rhs(F_ARG) {

  // constants: loop - 2 vals for k, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 1>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+<<num_var_params - 1>>)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg;
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1)
      int_basis = 1.0 - (u + 1.0)/2.0;
    else
      int_basis = (u + 1.0)/2.0;

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}
void F_TSA_<<getdp_name>>_constantThickness_rhs(F_ARG) {

  // constants: loop - 2 vals for k, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 1>>];
  double delta = (double) Fct->Para[<<num_const_params + 2>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg;
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0]; 
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1)
      int_basis = 1.0 - (u + 1.0)/2.0;
    else
      int_basis = (u + 1.0)/2.0;

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}
{% endif %}

{% if stiffness %}
void F_TSA_<<getdp_name>>_stiffness(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 2>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+<<num_var_params - 1>>)->Val[0];

  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}
void F_TSA_<<getdp_name>>_constantThickness_stiffness(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 2>>];
  double delta = (double) Fct->Para[<<num_const_params + 3>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}

  double u,v,w,wght;

  double num_int = 0;

  static Function Fct_arg;
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0]; 
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  if (k != l)
    num_int *= -1.0;

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}
{% endif %}

{% if mass %}
void F_TSA_<<getdp_name>>_mass(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 2>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+<<num_var_params - 1>>)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg;
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);


    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}
void F_TSA_<<getdp_name>>_constantThickness_mass(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 2>>];
  double delta = (double) Fct->Para[<<num_const_params + 3>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k == 2 && l == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);


    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}
{% endif %}

{% if triple %}
void F_TSA_<<getdp_name>>_triple(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int m = (int) Fct->Para[<<num_const_params + 2>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 3>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+<<num_var_params - 1>>)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1 && m == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 3.0);
    else if (k == 2 && l == 2 && m == 2)
      int_basis = pow((u + 1.0)/2.0, 3.0);
    else if (k + l + m == 4)
      int_basis = (u + 1.0)/2.0 * pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k + l + m == 5)
      int_basis = pow((u + 1.0)/2.0, 2.0) * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_TSA_<<getdp_name>>_constantThickness_triple(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int k = (int) Fct->Para[<<num_const_params>>];
  int l = (int) Fct->Para[<<num_const_params + 1>>];
  int m = (int) Fct->Para[<<num_const_params + 2>>];
  int nbr_points = (int) Fct->Para[<<num_const_params + 3>>];
  double delta = (double) Fct->Para[<<num_const_params + 4>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    if (k == 1 && l == 1 && m == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 3.0);
    else if (k == 2 && l == 2 && m == 2)
      int_basis = pow((u + 1.0)/2.0, 3.0);
    else if (k + l + m == 4)
      int_basis = (u + 1.0)/2.0 * pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (k + l + m == 5)
      int_basis = pow((u + 1.0)/2.0, 2.0) * (1.0 - (u + 1.0)/2.0);

    num_int += wght * V_arg.Val[0] * int_basis;
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}
{% endif %}

{% if fct_only %}
void F_TSA_<<getdp_name>>_fct_only(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  int nbr_points = (int) Fct->Para[<<num_const_params + 3>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}
  double delta = (double)(A+<<num_var_params - 1>>)->Val[0];

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * delta * 0.5;
  V->Type   = SCALAR;
}

void F_TSA_<<getdp_name>>_constantThickness_fct_only(F_ARG) {

  // constants: loop - 3 vals for k, l, nbr_points
  {% for idx in range(num_const_params) %}
  double const_param_<<idx>> = (double) Fct->Para[<<idx>>];
  {% endfor %}

  // k, l, nbr_points always last arguments
  int nbr_points = (int) Fct->Para[<<num_const_params + 3>>];
  double delta = (double) Fct->Para[<<num_const_params + 4>>];

  // TODO: this won't work for vectorial parameters (e.g., magnetic field)
  // TODO: for vectors, we should apply the norm only when calling the material function
  // variables: loop - 1 vals for delta
  {% for idx in range(num_var_params - 1) %}
  double var_param_<<idx>> = (double) (A+<<idx>>)->Val[0];
  {% endfor %}

  double u,v,w,wght,int_basis;

  double num_int = 0;

  static Function Fct_arg; 
  static double Fct_params[<<num_const_params>>];
  Fct_arg.Para = &Fct_params[0];
  static Value A_arg[<<num_var_params - 2>>];
  static Value V_arg;

  for(int n = 0; n < nbr_points; n++) {

    Gauss_Line(nbr_points, n, &u, &v, &w, &wght);

    {% for idx in range(floor((num_var_params - 1)/2)) %}
    // transform integrand to integration from -1 to 1
    double arg_<<idx>> = (var_param_<<2 * idx + 1>> - var_param_<<2 * idx>>)/2.0 * (u + 1.0) + var_param_<<2 * idx>>;

    A_arg[<<idx>>].Type = SCALAR; 
    A_arg[<<idx>>].Val[0] = arg_<<idx>>;
    {% endfor %}

    {% for idx in range(num_const_params) %}
    Fct_arg.Para[<<idx>>] = const_param_<<idx>>;
    {% endfor %}

    F_<<function_name>>(&Fct_arg, &A_arg[0], &V_arg);

    num_int += wght * V_arg.Val[0];
  }

  /* if(isnan(num_int))
      Message::Error("Output is nan"); */

  V->Val[0] = num_int * delta * 0.5;
  V->Type   = SCALAR;
}
{% endif %}

{% endfor %}
