#ifndef GEOMETRY_UTILS_H
#define GEOMETRY_UTILS_H

#include "F.h"
#include "Message.h"
#include "ProData.h"
#include <cmath>
#include <unordered_map>
#include <limits>
#include <algorithm>
#include <iostream>


#define PI 3.14159265358979323846
#define EPSILON 5e-7

class Vec3 {
public:
    Vec3(double x, double y, double z);
    Vec3();
    
    void normalize();
    void rotateWithRespectToXYPlane(double radians);
    double XYplanarMagnitude() const;
    double x() const;
    double y() const;
    double z() const;
    double magnitude() const;
    
    Vec3 operator*(double scalar) const;
    Vec3 operator/(double scalar) const;
    bool operator==(const Vec3& other) const;
    
    Vec3 transformToWindingCoordinates(double x, double y, double z) const;
    Vec3 transformToXYZCoordinates(double x, double y, double z) const;

private:
    double m_x, m_y, m_z;
};

struct Vec3Hash {
    std::size_t operator()(const Vec3& v) const;
};

class Tensor {
public:
    Tensor(double xx, double xy, double xz, double yx, double yy, double yz, double zx, double zy, double zz);
    Tensor();
    
    double xx() const;
    double xy() const;
    double xz() const;
    double yx() const;
    double yy() const;
    double yz() const;
    double zx() const;
    double zy() const;
    double zz() const;
    
    Tensor transpose() const;
    Tensor operator*(const Tensor& tensor) const;
    Vec3 operator*(const Vec3& vector) const;
    Tensor transformToWindingCoordinates(double x, double y, double z) const;
    Tensor transformToXYZCoordinates(double x, double y, double z) const;

private:
    double m_xx, m_xy, m_xz;
    double m_yx, m_yy, m_yz;
    double m_zx, m_zy, m_zz;
};

class Winding {
public:
    static Winding& getInstance();
	
	static Winding& get_winding_object(double innerRadius = 0.0, double thickness = 0.0, double gapThickness = 0.0, double startAngle = 0.0, int azimuthalNumberOfElements = 0.0,
		double tapeHeight = 0.0, int numberOfPancakeCoils = 0.0, double gapBetweenPancakeCoils = 0.0);
    void init(double innerRadius, double thickness, double gapThickness, double startAngle, int azimuthalNumberOfElements, double tapeHeight, int numberOfPancakeCoils, double gapBetweenPancakeCoils);
    bool isInitialized() const;
    double getGlobalStartingZ() const;
    double getStartingZ(int whichPancakeCoil) const;
    double getEndingZ(int whichPancakeCoil) const;
    int findTurnNumberFromElementIndex(int elementIndex) const;
    int getRotationMultiplier(const Vec3& point) const;
    bool isClockwise(const Vec3& point) const;
    int getElementIndex(const Vec3& point) const;
    double getArcLength(const Vec3& point);
    double getArcLengthContactLayer(const Vec3& point);
    double getTurnNumber(const Vec3& point);
    double getTurnNumberContactLayer(const Vec3& point);
    Vec3 getNormal(const Vec3& point);
    Tensor getTransformationTensor(const Vec3& point);
    void setRhoDerivative(const Tensor& rho_derivative);
    Tensor getRhoDerivative() const;

private:
    Winding();
    Winding(Winding&) = delete;
    void operator=(Winding const&) = delete;

    double m_innerRadius, m_thickness, m_gapThickness, m_startAngle;
    int m_azimuthalNumberOfElements, m_numberOfPancakeCoils;
    double m_tapeHeight, m_gapBetweenPancakeCoils;
    bool m_initialized;
    Tensor m_rho_derivative;
    
    std::unordered_map<Vec3, double, Vec3Hash> m_turn_number_map;
    std::unordered_map<Vec3, Vec3, Vec3Hash> m_normal_map;
    std::unordered_map<Vec3, double, Vec3Hash> m_arc_length_map;
    std::unordered_map<Vec3, Tensor, Vec3Hash> m_transformation_tensor_map;
    std::unordered_map<Vec3, double, Vec3Hash> m_arc_length_contact_layer_map;
    std::unordered_map<Vec3, double, Vec3Hash> m_turn_number_contact_layer_map;
};

bool isInsideTheTriangle(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3);
int isInsideTheMeshPolygon(double x, double y, double ane, double r_i, double theta_i, double gap, bool clockwise);
double spiralsSpeedFunction(double t, double a, double b);
double integrateSpiralsSpeedFunction(double r_2, double theta_2, double r_1, double theta_1, double dt);

double linear_interpolation(const std::vector<double>& x_values, const std::vector<double>& y_values, double x);

#endif // GEOMETRY_UTILS_H