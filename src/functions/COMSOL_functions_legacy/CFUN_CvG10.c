#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double a0  = -2.4083;
  double a1  = 7.6006;
  double a2  = -8.2982;
  double a3  = 7.3301;
  double a4  = -4.2386;
  double a5  = 1.4294;
  double a6  = -0.24396;
  double a7  = 0.015236;
  double rho = 1.9e3; //[kg/m^3]
  double log_T; 
  double f_exp; 
  
  if (strcmp("CFUN_CvG10", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  	  
	  log_T = log10(T);
	  f_exp = (a0 + a1*log_T + a2*pow(log_T,2) + a3*pow(log_T,3) + a4*pow(log_T,4) + a5*pow(log_T,5) + a6*pow(log_T,6) + a7*pow(log_T,7));
	  outReal[i] = rho*pow(10,f_exp);
		
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
	
	}
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
