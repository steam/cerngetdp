//#include "Header.h"

static const char *error = NULL;

// int init(const char *str)
// {
// 	return 1;
// }
//
//
//
// const char * getLastError() {
// 	return error;
// }
//
// int evalMATLAB2(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_Tc,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	double *inReal[2] = { arr_T,arr_Tc };
// 	int n = 0;
// 	return n;
// }
//
// int evalMATLAB3(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_fscaling,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	double *inReal[3] = { arr_T,arr_B,arr_fscaling };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
// int evalMATLAB4(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_Tc,
// 	const double *arr_Tc0,
// 	const double *arr_Tcs,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	double *inReal[4] = { arr_T,arr_Tc,arr_Tc0,arr_Tcs };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
//
//
//
//
// int evalMATLAB5(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_Jc,
// 	const double *arr_Tc0,
// 	const double *arr_Bc2,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	double *inReal[5] = { arr_T,arr_B,arr_Jc,arr_Tc0,arr_Bc2 };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
// int evalMATLAB6(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_C0,
// 	const double *arr_Tc0,
// 	const double *arr_Bc2,
// 	const double *arr_alpha,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	double *inReal[6] = { arr_T,arr_B,arr_C0,arr_Tc0,arr_Bc2,arr_alpha };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }

int eval(const char *func,
	int nArgs,
	const double **inReal,
	const double **inImag,
	int blockSize,
	double *outReal,
	double *outImag) {

	int i;
	double a0 = 0.0;
	double a1 = 0.0;
	double a2 = 0.0;
	double a3 = 0.0;
	double a4 = 0.0;
	double a5 = 0.0;
	double a6 = 0.0;
	double a7 = 0.0;
	double a8 = 0.0;
	double T1 = 1.0;
	double T2 = 20.0;
	double T3 = 140.0;
	double log_T;
	double f_exp=0.0;

	if (strcmp("CFUN_kAg", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
		}

		for (i = 0; i < blockSize; i++) {

			double T = inReal[0][i];


			/*Input consistency check for negative T*/
			if (T < 1.0)
			{
				T = 1.0;
				f_exp = 67;
			}
			else if (T <= 20.0)
			{
				a0 = 1.82603764562617;
				a1 = 1.04530563240476;
				a2 = -0.316726715333209;
				a3 = 0.551121278546028;
				a4 = -0.326314237655585;
				log_T = log10(T);
				f_exp = a0 + a1*pow(log_T, 1) + a2*pow(log_T, 2) + a3*pow(log_T, 3) + a4*pow(log_T, 4);
				f_exp = pow(10,f_exp);

			}
			else if (T <= 140.0)
			{
				a0 = 10734.3798030775;
				a1 = -48134.2889933188;
				a2 = 93783.5641105213;
				a3 = -103708.203166996;
				a4 = 71220.813873466;
				a5 = -31116.4147264840;
				a6 = 8449.57931641046;
				a7 = -1304.32557090996;
				a8 = 87.6603250260841;
				log_T = log10(T);
				f_exp = a0 + a1*pow(log_T, 1) + a2*pow(log_T, 2) + a3*pow(log_T, 3) + a4*pow(log_T, 4) + a5*pow(log_T, 5) + a6*pow(log_T, 6) + a7*pow(log_T, 7) + a8*pow(log_T, 8);
				f_exp = pow(10, f_exp);
			}
			else
			{

				f_exp = 421.0;
			}





			outReal[i] = f_exp;

			/*Output consistency check*/
			if (outReal[i] != outReal[i]) {
				error = "Output is nan";
				return 0;
			}
			if (fabs(outReal[i])>DBL_MAX) {
				error = "Output is inf";
				return 0;
			}
		}
		return 1;
	}

	else {
		error = "Unknown function";
		return 0;
	}
}
