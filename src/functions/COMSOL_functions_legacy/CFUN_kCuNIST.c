#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  int i;

  if (strcmp("CFUN_kCuNIST", func) == 0) {
    if (nArgs != 4) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T   = inReal[0][i];
	  double RRR = inReal[1][i];
	  double rho_B0 = inReal[2][i];
	  double rho = inReal[3][i];	  
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(RRR < 0)   {error = "RRR is negative!"; return 0; } 
	  if(rho_B0 < 0)   {error = "rhoCu is negative!"; return 0; }
	  if(rho < 0)   {error = "rhoCu is negative!"; return 0; }	  
	  
	  double beta = 0.634/ RRR;
	  double beta_r = beta/3e-4;
	  	  
	  double P1 = 1.754e-8;
	  double P2 = 2.763;
	  double P3 = 1102;
	  double P4 = -0.165;
	  double P5 = 70;
	  double P6 = 1.756;
	  double P7 = 0.838/(pow(beta_r,0.1661));
	  
	  double W_0 = beta / T;
	  double W_i = P1*pow(T,P2)/(1+P1*P3*pow(T,(P2+P4))*exp(-pow((P5/T),P6)));
      double W_i0 = P7 * (W_i*W_0)/(W_i+W_0);
		
	  double k_n = 1/( W_0 + W_i + W_i0);
		
	  outReal[i] = k_n * (rho_B0 / rho);
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
