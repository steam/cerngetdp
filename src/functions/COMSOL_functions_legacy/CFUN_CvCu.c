#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i, j;
  double aCp[6] = {-3.0800E-02, -3.0450E-01,  4.1900E-02, -8.4800E-04, -4.8000E-05,  0.0000E+00};
  double bCp[6] = { 7.2290E+00,  2.9871E+01, -1.4024E+01,  8.4190E-01,  9.1730E-02,  1.2000E-05};
  double cCp[6] = {-2.1286E+00, -4.5561E+02,  1.5089E+03, -3.2552E+02, -6.4120E+01, -2.1486E-01};
  double dCp[6] = { 1.0189E+02,  3.4695E+03, -3.1595E+04,  6.0590E+04,  2.0363E+04,  1.0038E+03};
  double eCp[6] = { 2.5631E+00, -8.2503E+03,  1.7843E+05, -1.2900E+06,  1.0300E+06,  3.1800E+06};

  double T1 = 9.441;
  double T2 = 31.134;
  double T3 = 123.34;
  double T4 = 306.12;
  double T5 = 498.15;

  if (strcmp("CFUN_CvCu", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  
	  if(T <= T1) {
		outReal[i] = aCp[0]*pow(T,4) + bCp[0]*pow(T,3) + cCp[0]*pow(T,2) + dCp[0]*T + eCp[0];
	  }
      else if (T > T1 && T<= T2) {
		outReal[i] = aCp[1]*pow(T,4) + bCp[1]*pow(T,3) + cCp[1]*pow(T,2) + dCp[1]*T + eCp[1];
	  }
	  else if (T > T2 && T<= T3) {
		outReal[i] = aCp[2]*pow(T,4) + bCp[2]*pow(T,3) + cCp[2]*pow(T,2) + dCp[2]*T + eCp[2];
	  }
	  else if (T > T3 && T<= T4) {
		outReal[i] = aCp[3]*pow(T,4) + bCp[3]*pow(T,3) + cCp[3]*pow(T,2) + dCp[3]*T + eCp[3];
	  }
	  else if (T > T4 && T<= T5) {
		outReal[i] = aCp[4]*pow(T,4) + bCp[4]*pow(T,3) + cCp[4]*pow(T,2) + dCp[4]*T + eCp[4];
	  }
	  else {
		outReal[i] = aCp[5]*pow(T,4) + bCp[5]*pow(T,3) + cCp[5]*pow(T,2) + dCp[5]*T + eCp[5];
	  }
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}