#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

  int i;

  double c0       = 1.45; //(for RRR=100=R_295/R_4)
  double Tref_RRR = 295;
  double c1       = 1*1e9;
  double c2       = 0;
  double c3       = -1e5;
  double c4       = 1e4;
  double c5       = 150;
  double f_mr     = 2e-3;

  double rho_0;
  double rho_mr;

  if (strcmp("CFUN_rhoAgCUDI", func) == 0) {
    if (nArgs != 4) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {

	  double T       = inReal[0][i];
	  double B       = inReal[1][i];
	  double RRR     = inReal[2][i];
	  double Tup_RRR = inReal[3][i];
	  double c0_scale = Tup_RRR/Tref_RRR;

	  /*Input consistency check with hotfix for negative T*/
	  	  if(T < 1.9)     {T=1.9;}
		  if(B < 0)       {error = "normB is negative!"; return 0; }
		  if(RRR < 0)     {error = "RRR is negative!"; return 0; }
		  if(Tup_RRR < 0) {error = "Tup_RRR is negative!"; return 0; }

	  rho_0      = ( c0_scale * c0 / RRR + (1/(c1/pow(T,5) + c2/pow(T,4) + c3/pow(T,3) + c4/pow(T,2) + c5/T)))*1e-8 ;

	  rho_mr     =  rho_0 * ( 1 + f_mr*RRR*B );

	  outReal[i] =  rho_mr;

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan";
		return 0;
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf";
		return 0;
	  }

    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
