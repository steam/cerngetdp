#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

double CvHast(double T) {
	double CvHast;
    double a =  6.24337055e-10;
    double b = -1.39265083e-07;
    double c =  1.52127960e-05;
    double d =  2.29168247e-07;
    double e = -1.87730865e-01;
    double f =  1.70166185e+01;
	double g = -2.77114255e+02;
	double h =  3.09769670e+03;
	double i =  2.95618392e+03;
	double j = -1.57110370e-12;
	double k =  2.12749836e-15;
	double l = -1.21101931e-18;

	double a2 =  2.2395343249202704e-001;
    double b2 =  3.4472427467879702e-001;
    double c2 = -2.5464259199394670e-002;
    double d2 =  1.5172451948324440e-003;
    double e2 = -1.2905644232172242e-005;
	
	double m = 2.8333357325708231e-001*8890;
	
	double n = 0;
	if(T < 316 && T > 34) {
        CvHast = l*pow(T,11) + k*pow(T,10) + j*pow(T,9) + a*pow(T,8) + b*pow(T,7) + c*pow(T,6) + pow(d,pow(T,5)) + e*pow(T,4) + f*pow(T,3) + g*pow(T,2) + h*T + i;
    }   
	
	//316 is the temperature 
	if(T >= 316) {
		double T_const = 316;
		n = l*pow(T_const,11) + k*pow(T_const,10) + j*pow(T_const,9) + a*pow(T_const,8) + b*pow(T_const,7) + c*pow(T_const,6) + pow(d,pow(T_const,5)) + e*pow(T_const,4) + f*pow(T_const,3) + g*pow(T_const,2) + h*T_const + i - m*T_const;
        CvHast = m*T + n;
    }
	if(T < 35) {
		double s = 35;
		a2 = l*pow(s,11) + k*pow(s,10) + j*pow(s,9) + a*pow(s,8) + b*pow(s,7) + c*pow(s,6) + pow(d,pow(s,5)) + e*pow(s,4) + f*pow(s,3) + g*pow(s,2) + h*s + i - ((e2*pow(s,4) + d2*pow(s,3) + c2*pow(s,2) + b2*s)*8890);
        CvHast = (e2*pow(T,4) + d2*pow(T,3) + c2*pow(T,2) + b2*T)*8890 + a2;
    } 

	return CvHast;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_CvHast", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 0)     {
				error = "T is negative!"; return 0; 
			} 
	
			if(T > 1400){
				error = "Temperature is above 300 K - outside of the validated range!"; 
				return 0;	  
			} 
	
			
			outReal[i] = CvHast( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

/*else {
    error = "Unknown function";
    return 0;
}*/
}