//#include "Header.h"

static const char *error = NULL;

// int init(const char *str) {
// 	return 1;
// }
//
//
// const char * getLastError() {
// 	return error;
// }
//
// int evalMATLAB2(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_Tc,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	const double *inReal[2] = { arr_T,arr_Tc };
// 	int n = 0;
// 	return n;
// }
//
// int evalMATLAB3(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_fscaling,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	const double *inReal[3] = { arr_T,arr_B,arr_fscaling };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
// int evalMATLAB4(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_Tc,
// 	const double *arr_Tc0,
// 	const double *arr_Tcs,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	const double *inReal[4] = { arr_T,arr_Tc,arr_Tc0,arr_Tcs };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
// int evalMATLAB5(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_Jc,
// 	const double *arr_Tc0,
// 	const double *arr_Bc2,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	const double *inReal[5] = { arr_T,arr_B,arr_Jc,arr_Tc0,arr_Bc2 };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }
//
// int evalMATLAB6(const char *func,
// 	int nArgs,
// 	const double *arr_T,
// 	const double *arr_B,
// 	const double *arr_C0,
// 	const double *arr_Tc0,
// 	const double *arr_Bc2,
// 	const double *arr_alpha,
// 	const double **inImag,
// 	int blockSize,
// 	double *outReal,
// 	double *outImag)
// {
// 	const double *inReal[6] = { arr_T,arr_B,arr_C0,arr_Tc0,arr_Bc2,arr_alpha };
// 	return eval(func, nArgs, inReal, inImag, blockSize, outReal, outImag);
// }


int eval(const char *func,
	int nArgs,
	const double **inReal,
	const double **inImag,
	int blockSize,
	double *outReal,
	double *outImag) {

	int i;
	double a0 = 0.0;
	double a1 = 0.0;
	double a2 = 0.0;
	double a3 = 0.0;

	double f_exp;

	if (strcmp("CFUN_rhoSSCERN", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
		}

		for (i = 0; i < blockSize; i++) {

			double T = inReal[0][i];

			if (T < 0)
			{
				T=0;
			}

			if (T < 300.0)
			{
				a0 = 4.996779239900617*pow(10, -7);
				a1 = 1.582773534554155*pow(10, -10);
				a2 = 3.239164442808503*pow(10, -12);
				a3 = -5.668537774914880*pow(10, -15);
			}
			else
			{
				a0 = 4.866752553602651*pow(10, -7);
				a1 = 6.662372319867489*pow(10, -10);
			}

			f_exp = a0 + a1*pow(T, 1) + a2*pow(T, 2) + a3*pow(T, 3);

			outReal[i] = f_exp;

			/*Output consistency check*/
			if (outReal[i] != outReal[i]) {
				error = "Output is nan";
				return 0;
			}
			if (fabs(outReal[i])>DBL_MAX) {
				error = "Output is inf";
				return 0;
			}
		}
		return 1;
	}

	else {
		error = "Unknown function";
		return 0;
	}
}
