#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

double kSS(double T) {
	int iter;
	double a = -1.4087; // 1/log(K)^8
	double b = 1.3982;  // 1/log(K)^7
	double c = 0.2543;  // 1/log(K)^6
	double d = -0.6260; // 1/log(K)^5
	double e = 0.2334;  // 1/log(K)^4
    double f = 0.4256;  // 1/log(K)^3
    double g = -0.4658; // 1/log(K)^2
    double h = 0.1650;  // 1/log(K)
    double i = -0.0199; // 1
	
	double coeffs[9] = {a, b, c, d, e, f, g, h, i};
	
	double log10_T = log10(T);
	double log10y = 0;
	
	for(iter = 0; iter < 9; iter++) {
		log10y += coeffs[iter] * pow(log10_T, iter);
	}
	
	return pow(10, log10y);
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;


  if (strcmp("CFUN_kSteel", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 	  
	  if(T > 300) { error = "Temperature is above 300 K - outside of the validated range!"; return 0;}
	  
	  outReal[i] = kSS( T );
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}