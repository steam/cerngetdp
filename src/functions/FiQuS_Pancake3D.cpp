// #include "F.h"
// #include <math.h>
// #define PI 3.14159265358979323846
// #define EPSILON 5e-7

// class vec3 {
//   public:
//     vec3(double x, double y, double z) {
//         m_x = x;
//         m_y = y;
//         m_z = z;
//     }

//     void normalize() {
//         double length = sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
//         m_x = m_x / length;
//         m_y = m_y / length;
//         m_z = m_z / length;
//     }

//     void rotateWithRespectToXYPlane(double radians) {
//         double x = m_x;
//         double y = m_y;
//         double z = m_z;
//         m_x = x * cos(radians) - y * sin(radians);
//         m_y = x * sin(radians) + y * cos(radians);
//         m_z = z;
//     }

//     double XYplanarMagnitude() const { return sqrt(m_x * m_x + m_y * m_y); }

//     double x() const { return m_x; }
//     double y() const { return m_y; }
//     double z() const { return m_z; }

//   private:
//     double m_x, m_y, m_z;
// };

// bool isInsideTheTriangle(double x, double y, double x1, double y1, double x2,
//                          double y2, double x3, double y3) {
//     double A = 0.5 * (-y2 * x3 + y1 * (-x2 + x3) + x1 * (y2 - y3) + x2 * y3);
//     double sign = A < 0 ? -1 : 1;
//     double s = (y1 * x3 - x1 * y3 + (y3 - y1) * x + (x1 - x3) * y) * sign;
//     double t = (x1 * y2 - y1 * x2 + (y1 - y2) * x + (x2 - x1) * y) * sign;

//     return s >= 0 && t >= 0 && (s + t) <= 2 * A * sign;
// }

// int isInsideTheMeshPolygon(double x, double y, double ane, double r_i,
//                            double theta_i, double gap, bool clockwise) {
//     double theta = theta_i;
//     double theta_step;
//     if (clockwise) {
//         theta_step = -2 * PI / ane;
//     } else {
//         theta_step = 2 * PI / ane;
//     }
//     double r = r_i;
//     double r_step = gap / ane;

//     double x1, y1, x2, y2, x3, y3;
//     for (int i = 0; i < ane; i++) {
//         x1 = r * cos(theta);
//         y1 = r * sin(theta);
//         x2 = (r + r_step) * cos(theta + theta_step);
//         y2 = (r + r_step) * sin(theta + theta_step);
//         x3 = 0;
//         y3 = 0;
//         if (isInsideTheTriangle(x, y, x1, y1, x2, y2, x3, y3)) {
//             return i;
//         }
//         theta += theta_step;
//         r += r_step;
//     }
//     return -1;
// }

// double spiralsSpeedFunction(double t, double a, double b) {
//     return sqrt(a * a + (a * t + b) * (a * t + b));
// }

// double integrateSpiralsSpeedFunction(double r_2, double theta_2, double r_1,
//                                      double theta_1, double dt) {
//     theta_2 = theta_2 - theta_1;
//     theta_1 = 0;

//     if (theta_2 <= PI / 36000) {
//         return 0;
//     }

//     double b = r_1;
//     double a = (r_2 - b) / theta_2;

//     // integrate spiralsSpeedFunction with trapezoidal rule
//     double arcLength = spiralsSpeedFunction(0, a, b);
//     for (double t = dt; t < theta_2; t = t + dt) {
//         arcLength = arcLength + 2 * spiralsSpeedFunction(t, a, b);
//     }
//     arcLength = arcLength + spiralsSpeedFunction(theta_2, a, b);

//     arcLength = arcLength * dt / 2;

//     return arcLength;
// }

// class winding {
//   public:
//     winding() = default;

//     void setInnerRadius(double innerRadius) { m_innerRadius = innerRadius; }
//     void setThickness(double thickness) { m_thickness = thickness; }
//     void setGapThickness(double gapThickness) { m_gapThickness = gapThickness; }
//     void setStartAngle(double startAngle) { m_startAngle = startAngle; }
//     void setAzimuthalNumberOfElements(int azimuthalNumberOfElements) {
//         m_azimuthalNumberOfElements = azimuthalNumberOfElements;
//     }
//     void setTapeHeight(double tapeHeight) { m_tapeHeight = tapeHeight; }
//     void setNumberOfPancakeCoils(int numberOfPancakeCoils) {
//         m_numberOfPancakeCoils = numberOfPancakeCoils;
//     }
//     void setGapBetweenPancakeCoils(double gapBetweenPancakeCoils) {
//         m_gapBetweenPancakeCoils = gapBetweenPancakeCoils;
//     }

//     double getStartingZ() const {
//         // Find the z-coordinate of the bottom of the winding:
//         double startZ =
//             -(m_numberOfPancakeCoils * m_tapeHeight +
//               (m_numberOfPancakeCoils - 1) * m_gapBetweenPancakeCoils) /
//             2;

//         return startZ;
//     }

//     int getRotationMultiplier(const vec3 &point) const {
//         double startZ = this->getStartingZ();
//         double z = point.z();

//         // Determine which pancake coil the point is in and set the winding's
//         // rotation direction accordingly:
//         int rotationMultiplier = 1;
//         for (int i = 0; i < m_numberOfPancakeCoils; i++) {
//             if (z >= startZ +
//                          i * (m_tapeHeight + m_gapBetweenPancakeCoils / 2) &&
//                 z <= startZ + (i + 1) * (m_tapeHeight +
//                                          m_gapBetweenPancakeCoils / 2)) {
//                 if (i % 2 == 1) {
//                     rotationMultiplier = -1;
//                 }
//             }
//         }

//         return rotationMultiplier;
//     }

//     bool isClockwise(const vec3 &point) const {
//         // 1st pancake coil: counter-clockwise
//         // 2nd pancake coil: clockwise
//         // and so on...
//         double startZ = this->getStartingZ();
//         double z = point.z();

//         bool clockwise = false;
//         for (int i = 0; i < m_numberOfPancakeCoils; i++) {
//             if (z >= startZ +
//                          i * (m_tapeHeight + m_gapBetweenPancakeCoils / 2) &&
//                 z <= startZ + (i + 1) * (m_tapeHeight +
//                                          m_gapBetweenPancakeCoils / 2)) {
//                 if (i % 2 == 1) {
//                     clockwise = true;
//                 }
//             }
//         }

//         return clockwise;
//     }

//     int getElementIndex(const vec3 &point) const {
//         // Determine which pancake coil the point is in and set the winding's
//         // rotation direction accordingly:
//         bool clockwise = this->isClockwise(point);

//         // Total gap for the spiral:
//         double totalGap = m_thickness + m_gapThickness;

//         // Find the magnitude of the point's position vector:
//         double r = point.XYplanarMagnitude();

//         // Find the approximate turn number of the point. The point must be
//         // between startTurn and finalTurn:
//         int startTurn = (r - m_innerRadius) / totalGap - 1.5;
//         if (startTurn < 0) {
//             startTurn = 0;
//         }

//         int finalTurn = (r / cos(2 * PI / m_azimuthalNumberOfElements / 2) -
//                          m_innerRadius) /
//                             totalGap +
//                         0.5;
//         if (finalTurn == 0) {
//             finalTurn = 1;
//         }

//         // Check each turn to find exact element index that containts the point:
//         // 0th turn: first turn
//         // 0th elementIndex: first element
//         int elementIndex = -1;
//         for (int turn = startTurn; turn <= finalTurn; turn++) {
//             // Outer radius of the first turn's first element:
//             double outerPolygonRadius =
//                 m_innerRadius + turn * totalGap + m_thickness;

//             int relativeElementIndex = isInsideTheMeshPolygon(
//                 point.x(), point.y(), m_azimuthalNumberOfElements,
//                 outerPolygonRadius, m_startAngle, totalGap, clockwise);

//             if (relativeElementIndex != -1) {
//                 elementIndex =
//                     turn * m_azimuthalNumberOfElements + relativeElementIndex;
//                 break;
//             }
//         }
//         return elementIndex;
//     }

//     int findTurnNumberFromElementIndex(int elementIndex) const {
//         return elementIndex / m_azimuthalNumberOfElements;
//     }

//     double getArcLength(const vec3 &point) const {
//         int elementIndex = this->getElementIndex(point);
//         int turn = this->findTurnNumberFromElementIndex(elementIndex);
//         int rotationMultiplier = this->getRotationMultiplier(point);

//         if (elementIndex == -1) {
//             return -1;
//         } else {
//             double r_1 = m_innerRadius;
//             double theta_1 = m_startAngle;

//             double totalGap = m_thickness + m_gapThickness;
//             double innerPolygonRadius = m_innerRadius + turn * totalGap;
//             double outerPolygonRadius =
//                 m_innerRadius + turn * totalGap + m_thickness;

//             double thetaRelativeToCurrentTurn =
//                 atan2(point.y(), point.x()) * rotationMultiplier;

//             if (thetaRelativeToCurrentTurn < 0) {
//                 thetaRelativeToCurrentTurn += 2 * PI;
//             }

//             double theta_2 =
//                 m_startAngle + turn * 2 * PI + thetaRelativeToCurrentTurn;
//             double r_2 = (innerPolygonRadius + outerPolygonRadius) / 2 +
//                          thetaRelativeToCurrentTurn * totalGap / (2 * PI);

//             double dt = 2 * PI / m_azimuthalNumberOfElements / 8;
//             if (dt > theta_2 - theta_1) {
//                 dt = (theta_2 - theta_1) / 2;
//             }

//             double arcLength =
//                 integrateSpiralsSpeedFunction(r_2, theta_2, r_1, theta_1, dt);

//             return arcLength;
//         }
//     }

//     double getTurnNumber(const vec3 &point) const {
//         int elementIndex = this->getElementIndex(point);
//         int turn = this->findTurnNumberFromElementIndex(elementIndex);
//         int rotationMultiplier = this->getRotationMultiplier(point);

//         if (elementIndex == -1) {
//             return -1;
//         } else {
//             double theta_1 = m_startAngle;

//             double thetaRelativeToCurrentTurn =
//                 atan2(point.y(), point.x()) * rotationMultiplier;

//             if (thetaRelativeToCurrentTurn < 0) {
//                 thetaRelativeToCurrentTurn += 2 * PI;
//             }

//             double theta_2 =
//                 m_startAngle + turn * 2 * PI + thetaRelativeToCurrentTurn;

//             double turnNumber = (theta_2 - theta_1) / (2 * PI);

//             return turnNumber;
//         }
//     }

//     vec3 getNormal(const vec3 &point) const {
//         int elementIndex = this->getElementIndex(point);
//         int turn = this->findTurnNumberFromElementIndex(elementIndex);
//         int relativeElementIndex = elementIndex % m_azimuthalNumberOfElements;
//         int rotationMultiplier = this->getRotationMultiplier(point);

//         if (elementIndex == -1) {
//             return vec3(0, 0, 0);
//         } else {
//             double theta_step =
//                 2 * PI / m_azimuthalNumberOfElements * rotationMultiplier;

//             double totalGap = m_thickness + m_gapThickness;
//             double r_step = totalGap / m_azimuthalNumberOfElements;

//             double theta = m_startAngle + relativeElementIndex * theta_step;

//             double outerPolygonRadius =
//                 m_innerRadius + turn * totalGap + m_thickness;
//             double r = outerPolygonRadius + relativeElementIndex * r_step;

//             double x1 = r * cos(theta);
//             double y1 = r * sin(theta);
//             double x2 = (r + r_step) * cos(theta + theta_step);
//             double y2 = (r + r_step) * sin(theta + theta_step);

//             vec3 normalVector = vec3(x2 - x1, y2 - y1, 0);
//             normalVector.rotateWithRespectToXYPlane(-PI / 2);
//             normalVector.normalize();

//             return normalVector;
//         }
//     }

//   private:
//     double m_innerRadius;
//     double m_thickness;
//     double m_gapThickness;
//     double m_startAngle;
//     int m_azimuthalNumberOfElements;
//     double m_tapeHeight;
//     int m_numberOfPancakeCoils;
//     double m_gapBetweenPancakeCoils;
// };

// vec3 getNormal(double x, double y, double z, double r_i, double w_t,
//                double gap_t, double theta_i, int ane, int NofPancakeCoils,
//                double h, double gapBetweenPancakeCoils) {
//     winding w = winding();
//     w.setInnerRadius(r_i);
//     w.setThickness(w_t);
//     w.setGapThickness(gap_t);
//     w.setStartAngle(theta_i);
//     w.setAzimuthalNumberOfElements(ane);
//     w.setTapeHeight(h);
//     w.setNumberOfPancakeCoils(NofPancakeCoils);
//     w.setGapBetweenPancakeCoils(gapBetweenPancakeCoils);

//     return w.getNormal(vec3(x, y, z));
// }

// double getContinuousArcLength(double x, double y, double z, double r_i,
//                               double w_t, double gap_t, double theta_i, int ane,
//                               int NofPancakeCoils, double h,
//                               double gapBetweenPancakeCoils) {
//     winding w = winding();
//     w.setInnerRadius(r_i);
//     w.setThickness(w_t);
//     w.setGapThickness(gap_t);
//     w.setStartAngle(theta_i);
//     w.setAzimuthalNumberOfElements(ane);
//     w.setTapeHeight(h);
//     w.setNumberOfPancakeCoils(NofPancakeCoils);
//     w.setGapBetweenPancakeCoils(gapBetweenPancakeCoils);

//     return w.getArcLength(vec3(x, y, z));
// }

// double getContinuousTurnNumber(double x, double y, double z, double r_i,
//                                double w_t, double gap_t, double theta_i,
//                                int ane, int NofPancakeCoils, double h,
//                                double gapBetweenPancakeCoils) {
//     winding w = winding();
//     w.setInnerRadius(r_i);
//     w.setThickness(w_t);
//     w.setGapThickness(gap_t);
//     w.setStartAngle(theta_i);
//     w.setAzimuthalNumberOfElements(ane);
//     w.setTapeHeight(h);
//     w.setNumberOfPancakeCoils(NofPancakeCoils);
//     w.setGapBetweenPancakeCoils(gapBetweenPancakeCoils);

//     return w.getTurnNumber(vec3(x, y, z));
// }

// void F_FiQuS_Pancake3D_getNormal(F_ARG) {
//     double x = (double)A->Val[0];
//     double y = (double)A->Val[1];
//     double z = (double)A->Val[2];

//     double r_i = (double)Fct->Para[0];
//     double w_t = (double)Fct->Para[1];
//     double gap_t = (double)Fct->Para[2];
//     double theta_i = (double)Fct->Para[3];
//     double ane = (double)Fct->Para[4];
//     double NofPancakeCoils = (double)Fct->Para[5];
//     double h = (double)Fct->Para[6];
//     double gapBetweenPancakeCoils = (double)Fct->Para[7];

//     vec3 normalVector = getNormal(x, y, z, r_i, w_t, gap_t, theta_i, ane,
//                                   NofPancakeCoils, h, gapBetweenPancakeCoils);

//     V->Val[0] = normalVector.x();
//     V->Val[1] = normalVector.y();
//     V->Val[2] = normalVector.z();
//     V->Type = VECTOR;
// }

// void F_FiQuS_Pancake3D_getContinuousArcLength(F_ARG) {
//     double x = (double)A->Val[0];
//     double y = (double)A->Val[1];
//     double z = (double)A->Val[2];

//     double r_i = (double)Fct->Para[0];
//     double w_t = (double)Fct->Para[1];
//     double gap_t = (double)Fct->Para[2];
//     double theta_i = (double)Fct->Para[3];
//     double ane = (double)Fct->Para[4];
//     double NofPancakeCoils = (double)Fct->Para[5];
//     double h = (double)Fct->Para[6];
//     double gapBetweenPancakeCoils = (double)Fct->Para[7];

//     double arcLength =
//         getContinuousArcLength(x, y, z, r_i, w_t, gap_t, theta_i, ane,
//                                NofPancakeCoils, h, gapBetweenPancakeCoils);

//     V->Val[0] = arcLength;
//     V->Type = SCALAR;
// }

// void F_FiQuS_Pancake3D_getContinuousTurnNumber(F_ARG) {
//     double x = (double)A->Val[0];
//     double y = (double)A->Val[1];
//     double z = (double)A->Val[2];

//     double r_i = (double)Fct->Para[0];
//     double w_t = (double)Fct->Para[1];
//     double gap_t = (double)Fct->Para[2];
//     double theta_i = (double)Fct->Para[3];
//     double ane = (double)Fct->Para[4];
//     double NofPancakeCoils = (double)Fct->Para[5];
//     double h = (double)Fct->Para[6];
//     double gapBetweenPancakeCoils = (double)Fct->Para[7];

//     double turnNumber =
//         getContinuousTurnNumber(x, y, z, r_i, w_t, gap_t, theta_i, ane,
//                                NofPancakeCoils, h, gapBetweenPancakeCoils);

//     V->Val[0] = turnNumber;
//     V->Type = SCALAR;
// }
