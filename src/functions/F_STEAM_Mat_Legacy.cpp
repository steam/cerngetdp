#include <math.h>
#include <stdlib.h>
#include "F.h"
#include "Message.h"
#include <float.h>
#include <cstring>
#include "F_CERN_Utils.h"
#include "Gauss.h"

// Workaround: GetDP defines these macros and the COMSOL material functions
// use the same identifier for parameters, hence un- and redefine the macros
#undef P1
#undef P2

// legacy, will be replaced by automatic inclusion
namespace rhoCuNIST_legacy {
    #include "COMSOL_functions_legacy/CFUN_rhoCuNIST.c"
}

namespace kCuNIST_legacy {
  #include "COMSOL_functions_legacy/CFUN_kCuNIST.c"
}

namespace CvCU_CUDI_legacy {
  #include "COMSOL_functions_legacy/CFUN_CvCu.c"
}

namespace CvG10_NIST_legacy {
  #include "COMSOL_functions_legacy/CFUN_CvG10.c"
}

namespace CvNb3SnNIST_legacy{
  #include "COMSOL_functions_legacy/CFUN_CvNb3SnNIST.c"
}

namespace rhoAG_CUDI_legacy{
  #include "COMSOL_functions_legacy/CFUN_rhoAgCUDI.c"
}

namespace kKapton_legacy{
  #include "COMSOL_functions_legacy/CFUN_kKapton.c"
}

namespace CvKapton_legacy{
  #include "COMSOL_functions_legacy/CFUN_CvKapton.c"
}

namespace CvNbTi_legacy{
  #include "COMSOL_functions_legacy/CFUN_CvNbTi.c"
}

namespace CvSteel_legacy{
  #include "COMSOL_functions_legacy/CFUN_CvSteel.c"
}

namespace kSteel_legacy{
  #include "COMSOL_functions_legacy/CFUN_kSteel.c"
}

namespace rhoSteel_legacy{
  #include "COMSOL_functions_legacy/CFUN_rhoSSCERN.c"
}

namespace kAg_legacy{
  #include "COMSOL_functions_legacy/CFUN_kAg.c"
}

namespace CvHast_legacy{
  #include "COMSOL_functions_legacy/CFUN_CvHast.c"
}

#define P1 1
#define P2 2

void F_rhoCU_NIST_legacy(F_ARG) {

  if(A->Type != SCALAR || (A+1)->Type != SCALAR)
    Message::Error("Wrong type of arg. for rhoCU_NIST!");

  int nArgs = 4;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  // fill input
  inReal[0][0] = (double)A->Val[0];
  inReal[1][0] = (double)(A+1)->Val[0];
  inReal[2][0] = Fct->Para[0];
  inReal[3][0] = Fct->Para[1];

  // compute material parameter
  int code = rhoCuNIST_legacy::eval("CFUN_rhoCuNIST", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_kCU_NIST_legacy(F_ARG){

  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR)
    Message::Error("Wrong type of arg. for kCU_NIST!");

  int nArgs = 4;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)A->Val[0];
  inReal[1][0] = Fct->Para[0];
  inReal[2][0] = (double)(A+1)->Val[0];
  inReal[3][0] = (double)(A+2)->Val[0];

  int code = kCuNIST_legacy::eval("CFUN_kCuNIST", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvCU_CUDI_legacy(F_ARG) {

  if(A->Type != SCALAR)
    Message::Error("Wrong type of arg. for CvCU_CUDI_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)A->Val[0];

  int code = CvCU_CUDI_legacy::eval("CFUN_CvCu", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvG10_NIST_legacy(F_ARG) {

  if(A->Type != SCALAR)
    Message::Error("Wrong type of arg. for CvG10_NIST_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)A->Val[0];

  int code = CvG10_NIST_legacy::eval("CFUN_CvG10", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvNb3Sn_NIST_legacy(F_ARG) {

  if(A->Type != SCALAR || (A+1)->Type != SCALAR)
    Message::Error("Wrong type of arg. for CvNb3Sn_NIST!");

  int nArgs = 2;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)A->Val[0];
  inReal[1][0] = (double)(A+1)->Val[0];

  int code = CvNb3SnNIST_legacy::eval("CFUN_CvNb3SnNIST", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_rhoAG_CUDI_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR)
    Message::Error("Wrong inputs for rhoAG_CUDI!");

  int nArgs = 4;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0];
  inReal[1][0] = (double)(A+1)->Val[0];
  inReal[2][0] = Fct->Para[0];
  inReal[3][0] = Fct->Para[1];

  int code = rhoAG_CUDI_legacy::eval("CFUN_rhoAgCUDI", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvKapton_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for CvKapton_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0];

  int code = CvKapton_legacy::eval("CFUN_CvKapton", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_kKapton_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for kKapton_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0];


  int code = kKapton_legacy::eval("CFUN_kKapton", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvNbTi_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR)
    Message::Error("Wrong inputs for CvNbTi_legacy!");

  int nArgs = 5;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T
  inReal[1][0] = (double)(A+1)->Val[0]; // B
  inReal[2][0] = (double)(A+2)->Val[0]; // I, calculated as I = J * Ic/Jc?
  inReal[3][0] = Fct->Para[0]; // C1
  inReal[4][0] = Fct->Para[1]; // C2


  int code = CvNbTi_legacy::eval("CFUN_CvNbTi", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvSteel_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for CvSteel_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T

  int code = CvSteel_legacy::eval("CFUN_CvSteel", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_kSteel_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for kSteel_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T

  int code = kSteel_legacy::eval("CFUN_kSteel", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_rhoSteel_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for rhoSteel_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T

  int code = rhoSteel_legacy::eval("CFUN_rhoSSCERN", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_kAg_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for kAg_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T

  int code = kAg_legacy::eval("CFUN_kAg", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_CvHast_legacy(F_ARG) {
  if(A->Type != SCALAR)
    Message::Error("Wrong inputs for CvHast_legacy!");

  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = (double)(A)->Val[0]; // T

  int code = CvHast_legacy::eval("CFUN_CvHast", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  V->Val[0] = outReal[0];
  V->Type   = SCALAR;

  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);
}

void F_quenchFct_legacy(F_ARG) {

  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR)
    Message::Error("Wrong type of arg. for quenchFct!");

  double B = (double)A->Val[0];
  double J = (double)(A+1)->Val[0];
  double T = (double)(A+2)->Val[0];

  double effJ_SC  = Fct->Para[0];

  /*Input consistency check */
  // if(T < 1.9)
  //   Message::Error("T is negative!");
  //
  // if(B < 0)
  //   Message::Error("NormB is negative!");
  //
  // if(J < 0)
  //   Message::Error("NormJ is negative!");

  // Critical surface
  double TC0   = 16.0;
  double BC0   = 28.11;
  double JC0   = 6190e6;

  // Parameters for quench fit
  double p     = 1.52;
  double alpha = 0.96;

  double t  = T/TC0;
  double BC = BC0 * (1.0 - pow(t,p));
  double h  = B/BC;
  double JC = JC0 * pow(1.0 - pow(t,p), alpha - 1.0) *
                pow(1.0 - pow(t,2), alpha) * pow(h, -0.5) * pow(1-h, 2);

  double output;

  if(B >= BC || T >= TC0 || J*effJ_SC >= JC)
    output = 1.0;
  else
    output = 0.0;

  V->Val[0] = output;
  V->Type   = SCALAR;
}

void F_HTS_CS_legacy(F_ARG) {
  // Global index
  int iter_i;

  // Binary search algorithm
  double alpha;
  double lambda = 0;
  double lambda_MIN;
  double lambda_MAX;
  double fun;
  double err;

  if(A->Type != SCALAR)
    Message::Error("Wrong type of arg. for current sharing!");

  // Equivalent surface current density in the tape
  double Kz = (double)A->Val[0];
	double Ec = Fct->Para[0]; // 1e-4 [V/m]
	double Kc = Fct->Para[1]; // from the Ic fit
	double n  = Fct->Para[2]; // 20
  // Equivalent surface resistivity of the normal conducting layer
	double rhoSurfC  = Fct->Para[3];

  // error = (b-a)/2^n where (b-a)=1 and n=NO_OF_MAX_ITER
  // n = 20 -> error < 1e-6
  double ABS_TOL        = Fct->Para[4];
  int    NO_OF_MAX_ITER = Fct->Para[5];

  double rhoSurfS;

  // SKIP FOR SPEED-UP
  // Consistency check: arguments
 //  if(Kc <  0)
 //      Message::Error("Kc is <= 0!");
 //
 // if(Ec <= 0)
 //    Message::Error("Ec is <= 0!");
 //
 //  if(n        <  0)
 //    Message::Error("n is <  0!");
 //
 //  if(rhoSurfC <= 0)
 //    Message::Error("rhoSurfC is <= 0!");

  // Quench
	if (Kc<=0) {
		lambda = 0;
	}
	// Current Sharing Regime
	else {
		// Binary search algorithm
		lambda_MIN = 0;
		lambda_MAX = 1;

		rhoSurfS = (Ec/Kc)*pow((fabs(Kz)/Kc),(n-1));
		alpha    = rhoSurfS/rhoSurfC;

		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {

			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*pow(lambda,n)+lambda-1;
			err    = lambda_MAX-lambda_MIN;

			if (err>ABS_TOL){     // Update boundaries
				if (fun>=0){      // too much current in HTS
					lambda_MAX = lambda;
					}
				else if (fun < 0) {  // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;
			}
		}
    // Consistency check: number of iterations
   if (iter_i == NO_OF_MAX_ITER)
     Message::Warning("Maximum number of bisection iterations reached!");
}

 double output = lambda;

 if(isnan(output))
     Message::Warning("Output is nan");

 V->Val[0] = output;
 V->Type   = SCALAR;
}

void F_HomCC_CS_legacy(F_ARG) {
  // Global index
  int iter_i;

  // Binary search algorithm
  double alpha;
  double lambda = 0;
  double lambda_MIN;
  double lambda_MAX;
  double fun;
  double err;

  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR || (A+3)->Type != SCALAR)
    Message::Error("Wrong type of arg. for current sharing!");

	double Jcc = (double)A->Val[0]; // Current density in homogenized CC
	double Jcrit = (double)(A + 1)->Val[0]; // from the Ic fit
	double rhoNC  = (double)(A + 2)->Val[0]; // resistivity of normal conductor
	double n  = (double)(A + 3)->Val[0]; // n-Value

	double Ecrit = Fct->Para[0]; // Critical electric field 1e-4 [V/m]


  // error = (b-a)/2^n where (b-a)=1 and n=NO_OF_MAX_ITER
  // n = 20 -> error < 1e-6
  double ABS_TOL        = Fct->Para[1];
  int    NO_OF_MAX_ITER = Fct->Para[2];

  // thicknesses
  double thickness_NC  = Fct->Para[3];
  double thickness_HTS = Fct->Para[4];

  double rhoHTS;

  // Quench
	if (Jcrit<=0) {
		lambda = 0;
	}
	// Current Sharing Regime
	else {
		// Binary search algorithm
		lambda_MIN = 0;
		lambda_MAX = 1;

		rhoHTS = (Ecrit/Jcrit) * pow((fabs(Jcc)/Jcrit),(n-1));
		alpha  = (rhoHTS/thickness_HTS) / (rhoNC/thickness_NC);

		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {

			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*pow(lambda,n)+lambda-1;
			err    = lambda_MAX-lambda_MIN;

			if (err>ABS_TOL){     // Update boundaries
				if (fun>=0){      // too much current in HTS
					lambda_MAX = lambda;
					}
				else if (fun < 0) {  // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;
			}
		}
    // Consistency check: number of iterations
   if (iter_i == NO_OF_MAX_ITER)
     Message::Warning("Maximum number of bisection iterations reached!");
}

 double output = lambda;

 if(isnan(output))
     Message::Warning("Output is nan");

 V->Val[0] = output;
 V->Type   = SCALAR;
}


void F_kKaptonStiffness_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A + 1)->Type != SCALAR)
    Message::Error("Wrong inputs for kKapton_legacy!");

  int Nbr_Points = (int) Fct->Para[0];

  double temp_1 = (double)A->Val[0];
  double temp_2 = (double)(A+1)->Val[0];
  double delta = (double)(A+2)->Val[0];

  double u,v,w,wght;

  double num_int = 0;

  for(int n = 0; n < Nbr_Points; n++) {

    Gauss_Line(Nbr_Points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    double arg = (temp_2 - temp_1)/2.0 * (u + 1.0) + temp_1;

    num_int += wght * kKapton_compute(arg);
  }

  if(isnan(num_int))
      Message::Error("Output is nan");

  V->Val[0] = num_int/(2.0 * delta);
  V->Type   = SCALAR;
}

void F_kKaptonMass_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR ||
    (A+3)->Type != SCALAR || (A+4)->Type != SCALAR)
    Message::Error("Wrong inputs for kKapton_legacy!");

  int Nbr_Points = (int) Fct->Para[0];

  double temp_1 = (double)A->Val[0];
  double temp_2 = (double)(A+1)->Val[0];
  int i = (int) (A+2)->Val[0];
  int j = (int) (A+3)->Val[0];
  double delta = (double) (A+4)->Val[0];


  double u,v,w,wght,arg,int_basis;

  double num_int = 0;

  for(int n = 0; n < Nbr_Points; n++) {

    Gauss_Line(Nbr_Points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    arg = (temp_2 - temp_1)/2.0 * (u + 1.0) + temp_1;

    if (i == 1 && j == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (i == 2 && j == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * kKapton_compute(arg) * int_basis;
  }

  if(isnan(num_int))
      Message::Error("Output is nan");

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

void F_CvKaptonMass_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR ||
    (A+3)->Type != SCALAR || (A+4)->Type != SCALAR)
      Message::Error("Wrong inputs for F_CvKaptonMass_legacy!");

  int Nbr_Points = (int) Fct->Para[0];

  double temp_1 = (double) A->Val[0];
  double temp_2 = (double) (A+1)->Val[0];
  int i = (int) (A+2)->Val[0];
  int j = (int) (A+3)->Val[0];
  double delta = (double) (A+4)->Val[0];

  double u,v,w,wght,arg,int_basis;

  double num_int = 0;

//   std::string weight_string = std::to_string(j);
// Message::Info(weight_string.c_str());

  for(int n = 0; n < Nbr_Points; n++) {

    Gauss_Line(Nbr_Points, n, &u, &v, &w, &wght);

    // transform integrand to integration from -1 to 1
    arg = (temp_2 - temp_1)/2.0 * (u + 1.0) + temp_1;

    if (i == 1 && j == 1)
      int_basis = pow(1.0 - (u + 1.0)/2.0, 2.0);
    else if (i == 2 && j == 2)
      int_basis = pow((u + 1.0)/2.0, 2.0);
    else
      int_basis = (u + 1.0)/2.0 * (1.0 - (u + 1.0)/2.0);

    num_int += wght * CvKapton_compute(arg) * int_basis;
  }

  if(isnan(num_int))
      Message::Error("Output is nan");

  V->Val[0] = num_int * 0.5 * delta;
  V->Type   = SCALAR;
}

// input parameter x, y, z of current coordinate
// input constant, list of 6-tupels of the form (x orig, y orig, z orig, x ref
// y ref, z ref)
void F_ShiftedCoordinate_legacy(F_ARG) {
  if(A->Type != SCALAR || (A+1)->Type != SCALAR || (A+2)->Type != SCALAR)
      Message::Error("Wrong inputs for ShiftedCoordinate!");

  int nbrCoords = Fct->NbrParameters ;

  if(nbrCoords % 6 != 0)
    Message::Error("Number of coordinates for ShiftedCoordinate not divisible \
      by six!");

  // simply loop over all coords
  // obviously more sophisticated methods are possible --> evaluate potential
  // bottleneck
  for (int i = 0; i < nbrCoords; i+=6) {
    for (int j = 0; j < 3; j++) {
      if (!isEqual(Fct->Para[i + j], (A + j)->Val[0]))
        break;
      else if (j == 2) {
        for(int k = 0; k < 3; k++)
          V->Val[k] = Fct->Para[i + 3 + k];

        V->Type = VECTOR;
        return;
      }
    }
  }

  Message::Error("Did not find corresponding reference point!");
}

bool isEqual(double firstCoord, double secondCoord) {
  double epsilon = 1E-14;

  if(firstCoord - epsilon <= secondCoord && firstCoord + epsilon >= secondCoord)
    return true;
  else
    return false;
}

double kKapton_compute(double temperature) {
  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = temperature;

  int code = kKapton_legacy::eval("CFUN_kKapton", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  double output = outReal[0];
  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);

  return output;
}

double CvKapton_compute(double temperature) {
  int nArgs = 1;

  // initialization (here instead of in initInput to omit a compiler warning)
  double **inReal = new double*[nArgs];
  double **inImag = new double*[nArgs];
  double* outReal = new double[blockSize];
  double* outImag = new double[blockSize];
  initInput(nArgs, blockSize, inReal, inImag);

  inReal[0][0] = temperature;

  int code = CvKapton_legacy::eval("CFUN_CvKapton", nArgs,
                              const_cast<const double **> (inReal),
                              const_cast<const double **> (inImag), blockSize,
                              outReal, outImag);

// if (code != 1)
    // Message::Warning("COMSOL material function exited abnormally!");

  double output = outReal[0];
  freeInputOutput(nArgs, inReal, inImag, outReal, outImag);

  return output;
}
