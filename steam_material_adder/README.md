# steam material adder
## Description

Using ```functions_adder.py```, one can add 
automatically a new function on GetDP interface. The script
first reads each function characteristics and parameters from
```included_function.csv``` and then updates
the files: ```STEAM_Mat_Lib_ProDefines.h```, ```F_CERN.cpp``` and
```F.h``` in GetDP source code. In order to add a material, the user
needs to fill out correspondingly the characteristics of the function
on ```included_function.csv```.

## Material parameters

The ```included_function.csv``` file has 6 columns in total:
1. **c_function_name**: the name of the C function (this name includes the version number)
2. **GetDP_function_name**: the name of the GetDP function that is created (it does not include the version number)
3. **input_const_params**: the number of input *constant* parameters (i.e. material parameters that do not change during the simulation)
4. **input_var_params**: the number of input *variable* parameters (e.g. temperature or magnetic field)
5. **mapping**: the line-up of the parameters on GetDP. We use 0,1,2,3... to declare the 
row of the arguments in the C functions, so if we need 
to change this structure (e.g. we want to swap the first 2 args)
we change the mapping (e.g. from 0-1 that it is on the Source_c to 1-0). See below for more explanation.
6. **in_helper**: this parameter is set to 1 only when the specific file is in the ```Source_c_helper``` folder.

## Calling a GetDP function
If ```CFUN_fctname``` has two variable parameters (T and B), and three constant parameters (C1, C2, C3), 
it has to be called by: ```CFUN_fctname[T, B]{C1, C2, C3}``` inside a .pro file (e.g. inside a formulation term).

Note that in many cases, variable parameters are also finite element *fields* (i.e. unknows of the problem),
so that curly brackets have to be used for them. For example, ```CFUN_fctname[{T}, Norm[{B}]]{C1, C2, C3}``` is the proper way of
calling the function when {T} and {B} are fields, with CFUN_fctname depending on T and the norm of B (it could be any function as well).

### Comment on the mapping
For most functions, the mapping is trivial and parameters are given to GetDP function in the same order as to C functions, with a distinction
between variable parameters and constant ones (as shown in the example above: [...]{...}).

However, for some functions, the parameters had to be reordered from the C function to the GetDP one. This is for example
the case when ```CFUN_fctname2``` takes parameters T, C1, and B, in that specific order, with C1 a constant parameter. The proper (i.e. efficient) way of calling this
function with GetDP is ```CFUN_fctname[T, B]{C1}```, while for the C function, it is called by ```CFUN_fctname(T,C1,B)```. 
The mapping describes this reordering. For this example, the mapping would be 0-2-1.

### Comment on duplicated functions
Note that some functions are duplicated, with a variation ending with ```_T```. The associated C function is the same,
but all parameters are considered constant, except for the temperature. This is for improving efficiency in GetDP, as only
the temperature will then be evaluated at runtime, whereas the other parameters are evaluated just once when reading the .pro file.

Same comment is valid for functions ending with ```_B```. This is for magnetic simulations only, with constant temperature taken as a parameter.
(Note how the mapping usually involves swapping the first two arguments in this case.)

## Updating the STEAM material library

Updates of the [STEAM material library](https://gitlab.cern.ch/steam/steam-material-library) are not pulled automatically. This is on purpose to avoid working with an unstable version of the material library. 

The library is included as a `git submodule`. Whenever an update to the STEAM material library should be propagated to `CERNGetDP` , you need to update the submodule as well as commit and push the change. This can be done calling the following in a git bash or terminal
```
git submodule update --remote
git add steam-material-library/
git commit -m "<<type commit message here>>"
git push
```

In summary, an updated submodule needs to be commited and pushed like a change to regular files.