import os
import pandas as pd

# Before running, one should change the paths: prodefines_path, f_path & f_steam_material_library_path (and possibly comsol_functions_path)

class GetDPFunctionAdder:
    # Read input parameters from Excel file

    def __init__(self, df, is_derivative=False):

        self.df = df
        self.is_derivative = is_derivative 
        # define the function name using the first column of the file
        self.c_function_names = self.df[0].tolist()
        self.function_names = self.df[1].tolist()
        # define the constant parameters
        self.num_const_params = self.df[2].tolist()
        # define the constant parameters
        self.num_var_params = self.df[3].tolist()
        # define the boolean if it's in source_c_helper or not (i.e., in source_c) 
        self.in_helper = self.df[5].tolist()
        # convert the params into integers
        self.num_const_params = [int(x) for x in self.num_const_params]
        self.num_var_params = [int(x) for x in self.num_var_params]
        # Calculate the total amount of input arguments
        self.nArgs = [self.num_const_params[i] + self.num_var_params[i] for i in range(len(self.num_var_params))]

        # define the paths
        self.prodefines_path = "src/interface/STEAM_Mat_Lib_ProDefines.h"
        self.f_path = "src/functions/STEAM_Mat_Lib.h"

        self.f_steam_material_library_path = "src/functions/F_STEAM_Mat_Lib.cpp"

    def write_function_definition(self, num_const_params, num_var_params, mapping_list):
        code = []

        # Generate code for the constant parameters
        for i in range(num_var_params):
            code.append(f"    inReal[{mapping_list[i]}][0] = (double)(A+{i})->Val[0];")

        # Generate code for the variable parameters
        for i in range(num_const_params):
            code.append(f"    inReal[{mapping_list[num_var_params + i]}][0] = (double)Fct->Para[{i}];")

        return '\n'.join(code)

    def add_function(self):

        for i, function_name in enumerate(self.function_names):
            # Append function definition to STEAM_Mat_Lib_ProDefines.h
            with open(self.prodefines_path, "a") as f:
                f.write(f'  {{ "{function_name}", (CAST)F_{function_name}, {self.num_const_params[i]}, {self.num_var_params[i]} }},\n')

            # Append function headers to F.h
            with open(self.f_path, "a") as f:
                f.write("void F_" + function_name + "(F_ARG);\n")


            # Add COMSOL file to COMSOL_functions directory
            # Add namespace for COMSOL function in F_CERN.cpp
            with open(self.f_steam_material_library_path, "a") as f:
                if self.is_derivative:
                    self.comsol_functions_path = "../../steam-material-library/models/c/derivatives/" + self.c_function_names[i]
                else:
                    if self.in_helper[i]:
                        self.comsol_functions_path = "../../steam-material-library/models/c/helper/" + self.c_function_names[i]
                    else: 
                        self.comsol_functions_path = "../../steam-material-library/models/c/general/" + self.c_function_names[i]
                f.write("namespace " + function_name + "{ \n")
                f.write('  #include "' + self.comsol_functions_path + '.c"\n')
                f.write("}\n \n")

                f.write("void F_" + function_name + "(F_ARG) {\n")
                f.write("    constexpr int nArgs = " + str(self.nArgs[i]) + ";\n\n")
                f.write("    static double inReal[nArgs][blockSize];\n")
                f.write("    static double inImag[nArgs][blockSize];\n")
                f.write("    static double outReal[blockSize];\n")
                f.write("    static double outImag[blockSize];\n")

                f.write("    // Set inputs for COMSOL function in inReal\n")

                mapping_list = self.df[4].tolist()[i]
                if isinstance(mapping_list, int):
                    mapping_list = [str(mapping_list)]
                else:
                    mapping_list = str(mapping_list).split('-')

                code = self.write_function_definition(self.num_const_params[i], self.num_var_params[i], mapping_list)
                f.write(code)

                f.write("    \n")

                f.write("    static const double* inRealPtrs[nArgs];\n")
                f.write("    static const double* inImagPtrs[nArgs];\n\n")

                f.write("    for (int i = 0; i < nArgs; ++i) {\n")
                f.write("        inRealPtrs[i] = inReal[i];\n")
                f.write("        inImagPtrs[i] = inImag[i];\n")
                f.write("    }\n\n")

                f.write("    int code = " + function_name + "::eval( \"" + self.c_function_names[i] + "\" , nArgs,\n")
                f.write("                                  inRealPtrs, inImagPtrs,\n")
                f.write("                                  blockSize, \n")
                f.write("                                  outReal, outImag);\n\n")
                f.write("     if (code != 1){\n")
                f.write(f"         Message::Debug(\"COMSOL material function {function_name} exited abnormally! Returning nan!\");\n")
                for i in range(self.num_var_params[i]):
                                    f.write(f"         Message::Debug(\"Input {i} was %g \", (double)(A+{i})->Val[0]);\n")            
                f.write( "         outReal[0] = std::numeric_limits<double>::quiet_NaN();\n")
                f.write("    }\n\n")
                f.write("    V->Val[0] = outReal[0];\n")
                f.write("    V->Type   = SCALAR;\n\n")
                f.write("}\n \n ")

    def cp_functions(self):
        # copying not needed on the remote machine
        pass

        # for function_name in self.c_function_names:
        #    #Here you define the path of the Source_LTS & COMSOL_functions folders on the project at your pc
        #    src_path = os.path.join("C:\\Users\\gzachou\\cernbox\\Technical_studentship\\Projects\\steam-material-library\\Source_LTS", f"{function_name}.c")
        #    dst_path = os.path.join("C:\\Users\\gzachou\\cernbox\\Technical_studentship\\Projects\\cerngetdp\\Functions\\COMSOL_functions", f"{function_name}.c")

        #    if not os.path.exists(dst_path):
        #        shutil.copy(src_path, dst_path)


# Correct the path on the code below to match your pc
# create an instance and make the addition
# This is how we use the function above:

# to read a .csv file just use:
df_functions = pd.read_csv('steam_material_adder/included_function.csv', header=None, skiprows=1)
# to read a .xlsx file just use:
# df_functions = pd.read_excel('included_function.xlsx', sheet_name='Sheet1', header=None, skiprows=1)
function_adder = GetDPFunctionAdder(df_functions, is_derivative=False)
function_adder.add_function()

df_derivatives = pd.read_csv('steam_material_adder/included_derivatives.csv', header=None, skiprows=1)
derivative_adder = GetDPFunctionAdder(df_derivatives, is_derivative=True)
derivative_adder.add_function()