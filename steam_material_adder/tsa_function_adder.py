from jinja2 import Environment, FileSystemLoader
import pandas as pd
import math 

tsa_cpp_file_path = "src/functions/FiQuS_TSA.cpp"
tsa_cpp_template_path = "src/functions/FiQuS_TSA_template.cpp"

tsa_h_file_path = "src/functions/FiQuS_TSA.h"
tsa_h_template_path = "src/functions/FiQuS_TSA_template.h" 

tsa_pro_defines_file_path = "src/interface/FiQuS_TSA_ProDefines.h"
tsa_pro_defines_template_path = "src/interface/FiQuS_TSA_ProDefines_template.h"

tsa_csv = "steam_material_adder/tsa_functions.csv"
included_fcts_csv = "steam_material_adder/included_function.csv"

### Read to be generated tsa functions from csv
tsa_data = pd.read_csv(tsa_csv, header=None, skiprows=1)

getdp_function_names = tsa_data[0].tolist()
rhs = tsa_data[1].tolist()
mass = tsa_data[2].tolist()
stiffness = tsa_data[3].tolist()
triple = tsa_data[4].tolist()
fct_only = tsa_data[5].tolist()

### Read included_function.csv to grab input_const_params, input_var_params, mapping
steam_mat_data = pd.read_csv(included_fcts_csv, header=None, skiprows=1)

c_function_names = steam_mat_data[0].tolist()
function_names = steam_mat_data[1].tolist()
num_const_params = steam_mat_data[2].tolist()
num_var_params = steam_mat_data[3].tolist()

tsa_const_params = []
tsa_var_params = [] 
tsa_mapping = []
tsa_function_names = []

### loop over tsa functions to grab right input_const_params, input_var_params, mapping
for name in getdp_function_names:
    function_index = function_names.index(name)

    tsa_const_params.append(num_const_params[function_index])

    # we need two parameters for each variable parameter (value for 2 linear basis functions)
    # plus one for the thickness, which is a piecewise defined function
    tsa_var_params.append(2 * num_var_params[function_index] + 1)

    tsa_mapping.append([i for i in range(num_const_params[function_index] + num_var_params[function_index])])

    tsa_function_names.append(function_names[function_index])


### Jinja2 templating #####################################################
loader = FileSystemLoader(".")
# following FiQuS, also use << and >> as variable start and end strings
env = Environment(loader=loader, variable_start_string='<<', variable_end_string='>>',
                    trim_blocks=True, lstrip_blocks=True)

## Generate FiQuS_TSA.cpp
tsa_cpp_template = env.get_template(tsa_cpp_template_path)
output_from_parsed_template = tsa_cpp_template.render(floor=math.floor, zipped_inputs=zip(getdp_function_names, tsa_const_params, tsa_var_params, rhs, mass, stiffness, triple, fct_only, tsa_function_names))
with open(tsa_cpp_file_path, "w") as tf:
    tf.write(output_from_parsed_template)

## Generate FiQuS_TSA.h
tsa_h_template = env.get_template(tsa_h_template_path)
output_from_parsed_template = tsa_h_template.render(zipped_inputs=zip(getdp_function_names, tsa_const_params, tsa_var_params, rhs, mass, stiffness, triple, fct_only))
with open(tsa_h_file_path, "w") as tf:
    tf.write(output_from_parsed_template)

## Generate FiQuS_TSA_ProDefines.h
tsa_pro_defines_template = env.get_template(tsa_pro_defines_template_path)
output_from_parsed_template = tsa_pro_defines_template.render(zipped_inputs=zip(getdp_function_names, tsa_const_params, tsa_var_params, rhs, mass, stiffness, triple, fct_only))
with open(tsa_pro_defines_file_path, "w") as tf:
    tf.write(output_from_parsed_template)
