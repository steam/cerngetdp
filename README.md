# CERNGetDP

CERNGetDP is mostly a copy of the original [GetDP source code](https://gitlab.onelab.info/getdp/getdp) with added material functions and other functionality specific to CERN.

GetDP is copyright (C) 1997-2022 P. Dular and C. Geuzaine, University of Liege, and is distributed under the terms of the GNU General Public License, Version 2 or later. See [the GetDP homepage](https://www.getdp.info) for more information.

## CERNGetDP executables

The releases can be found [under the Gitlab releases page](https://gitlab.cern.ch/steam/cerngetdp/-/releases) or [can be directly downloaded](https://cernbox.cern.ch/s/n6BIGib9k2OKA2B).

Snapshots built for each commit can be downloaded for [Windows](https://cernbox.cern.ch/remote.php/dav/public-files/r6trFEwyBQvRfgx/Windows/cerngetdp_snapshot_openblas.zip) (also available [with MPI](https://cernbox.cern.ch/remote.php/dav/public-files/r6trFEwyBQvRfgx/Windows/cerngetdp_snapshot_mpi.zip)) and [Unix](https://cernbox.cern.ch/remote.php/dav/public-files/r6trFEwyBQvRfgx/Linux/cerngetdp_snapshot.tar.gz) operating systems.

## Calling GetDP from the command line

Running GetDP `*.pro` files from the command line is the preferred way for efficient simulations. A typical GetDP call reads like this:

```
getdp modelName -solve resolutionName -pos postOperationName -v2
```

or, in two steps:

```
getdp modelName -solve resolutionName
getdp modelName -pos postOperationName -v2
```

- `getdp` is the GetDP executable. It can be the full (or relative) path leading to the executable, or a previously defined alias leading to that executable. I would recommend the last option. Information for setting it up is given below.
- `modelName` is the name of the `.pro` file to be run. By default, it will also read a mesh file with the same name (i.e., `modelName.msh`).
It will complain if it does not exist. If another mesh file has to be used, it can be specified by an additional option in the command line: `-msh meshFile.msh`.
- `-solve resolutionName` will ask GetDP to solve the problem with the Resolution `resolutionName` defined in the `.pro` file.
This solve option actually consists of two steps: a pre-processing step that will produce a `.pre` file containing information about the mesh, function spaces, groups, degrees of freedom (DOF), etc. Then, the resolution of the problem produces a
`.res` file, that contains the raw solution (i.e., the values of the DOFs).
- `-pos postOperationName` actually turns the information in the `.res` file into a human-readable set of files, defined in the PostOperation `postOperationName`. Depending of what is asked, this may produce `.pos` files to be opened and visualized with Gmsh, 
or `.txt` files with processed information (e.g., global quantities such as current intensity, voltage, AC loss, etc.).
- `-v2` asks to store the mesh information in the `.pos` files during the post-operation step. This option is optional, but convenient. With this option, opening a `.pos` file with Gmsh directly shows the associated mesh.

All these options, and much more, are listed in Chapter 3 of GetDP [documentation](https://getdp.info/dev/doc/texinfo/getdp.pdf). Scrolling through the available options is useful to have an idea of what is possible.

If your GetDP call outputs an annoying chunk of information, you can reduce the verbosity level with `-verbose 3` or `-v 3`, with a number from 0 to 9.

## Adding `getdp` as an alias for the GetDP executable

It can quickly be annoying to call GetDP (and Gmsh) by using the full path of the executable(s) everytime. For this reason, creating an alias (i.e., a handy command line shortcut) is helpful.

With the Windows PowerShell, aliases can be added with the `Set-Alias getdp C:\Users\name\getdp\path\obscure\dir` option. This alias will be valid on the current PowerShell session only.
More conveniently, they can be defined as permanent aliases, to be read at every PowerShell session by adding this `Set-Alias` option in the `profile.ps1` file, whose location on your computer can be
found with the command `echo $profile`. More info in this [stackoverflow question](https://stackoverflow.com/questions/24914589/how-to-create-permanent-powershell-aliases).

With the MacOS Terminal, a similar procedure can be followed by updating the `.bash_profile` file with a line like `alias getdp='/Users/name/getdp/less/obscure/dir'`.
Note that the name of the fine starts with a point `.` so it is hidden by default in the Finder. These files can be made visible with the `Command+Shift+.` (last is the period key) key combination.

Once these files are updated, restart the PowerShell or Terminal. You can then check that the setup is working properly by simply typing `getdp`. It will output the list of available commands to call GetDP with.

I recommend doing the same for the Gmsh executable, and launching simulations from the command lines will soon be a pleasure. 

# Adding new material functions manually

*Deprecated*: this is now done using `steam_material_adder/function_adder.py`. 

No matter if you start from a COMSOL C source template or from scratch, the following common steps have to be followed: 

1. In `Interface/ProDefines.h`, the function names have to be added in the definition of the structure `StringXFunction2Nbr`. The format is as follows `{"NameOfFunctionInGetDP", (CAST)NameOfFunctionInC++Source, NumberOfConstantParameters, NumberOfVariableParameters}`.
   
   An example for a function with two variable arguments (temperature field, time step) and one parameter (e.g. constant value of an analytical fitting law) would be `{"TestHTS" , (CAST)F_TestHTS, 1, 2 }`. Search for `CERN` in the header file to see examples. 

2. In `Functions/F.h`, add the functions headers. Again, search for `CERN` in the header file to see examples.

The next step depends if you want to add a new function from scratch or have a COMSOL C source at hand. 

## In general

The variable inputs (most commonly fields) can be accessed via the variable `A`, for example `double time = (double)A->Val[0]` and 
`double compZ_a = (double)(A+1)->Val[2]`, where the latter extracts the z-component of a field (i.e., the 3rd value).  

The parameters can be accessed via `Fct`, e.g. `double multiplier = Fct->Para[0]`. 

The output is safed in `V` by giving both the values and the types, e.g. `V->Val[0] = scalarOutput` and `V->Type = SCALAR`.

It is typically wise for debugging to include an input check. For maximum performance, this should be commented. 

## Using COMSOL-compatible C sources

3. Add the COMSOL file to `Functions/COMSOL_functions`.

4. In `Functions/F_CERN.cpp`, add a new namespace for the inclusion of the COMSOL file. This is necessary since the COMSOL functions are all called `eval`, no matter what the material name is. 

5. Add the function definition by following the examples of the other functions. Make sure that 

   - You change the input data check on `A` according to the number and type of inputs. 
   - You change the number of arguments `nArgs` to reflect the number of inputs. 
   - You set the inputs for the COMSOL function correctly in `inReal`. 
   - You change the namespace of the `eval` function and also the first argument, the number of the COMSOL function as a `string`. 

Afterwards, you should recompile (this can be done by CI/CD by simply pushing the changes). The function can then be called in GetDP by invoking `NameOfFunctionInGetDP[varArg1, ..., varArgN]{constParam1, ..., constParamM}`. 

# Setting up the CI/CD 

First of all, the build machine (might be a docker, virtual machine, physical machine, ...) needs to be setup to compile GetDP. This especially means the dependencies for GetDP (PETSc, static gmsh library, BLAS, ...) need to be installed on the machine. 

Fortunately, for `Linux` the docker images from onelab can be used (see https://hub.docker.com/u/onelab). Hence, you can just run them as they are on CERN's default Docker runner. 

For `Windows`, currently, a virtual machine is used. Therefore, an Openstack machine is setup following https://gitlab.cern.ch/eschnaub/getdecompile/-/blob/master/compileGetDP_windows.md. Afterwards, the runner needs to be registered as described at https://docs.gitlab.com/runner/install/windows.html. Here, shell needs to be used as the Docker executor. 

If the runners are registered with the right tags, they should be visible under `Settings/CI/CD` on the Gitlab site.

## In Case of Pipeline Getting Stuck

If the Windows runner gets stuck, the applicable procedure to fix it is described at https://steam-fiqus-dev.docs.cern.ch/gitlab_runner/

# Building and running the docker file locally

This section assumes that you have installed Docker and it runs.

Build the docker calling `docker build -f Dockerfile .`. This will tell you the container id at the end.

Run the docker container and open bash in it `docker run -it <<Container_id>> /bin/bash`.

We have two dockerfiles: 

- `Dockerfile`: contains a non-MPI GetDP with non-MPI PetSC. It still has OpenMPI installed to run e.g. Parareal without parallelization in space. 
- `Dockerfile_MPI`: contains MPI GetDP with MPI PetSC, based on OpenMPI.

# Versioning 

Each tag of the repository will trigger a release pipeline which will 

- create release executables/binaries for Win/UNIX and put them to `EOS`, the releases will have the name of the tag,
- create a release Dockerfile with the version equal to the commit tag. 

This allows us to repeat simulations with the exact same executable if desired. The naming of tags should be `yyyy.mm.v` with the year in 4 digits, month in 2 digits and an increasing version number per month.
